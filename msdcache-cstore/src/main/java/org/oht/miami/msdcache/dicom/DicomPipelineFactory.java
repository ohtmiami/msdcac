/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

public class DicomPipelineFactory implements ChannelPipelineFactory {
    
    private DicomServer dicomServer;
    
    public DicomPipelineFactory(DicomServer dicomServer) {
        
        this.dicomServer = dicomServer;
    }

    @Override
    public ChannelPipeline getPipeline() throws Exception {
        
        ChannelPipeline pipeline = Channels.pipeline();
        pipeline.addLast("dicom-pdu-decoder", new PDUDecoder());
        pipeline.addLast("dicom-assoc-handler", new AssociationHandler(dicomServer));
        pipeline.addLast("dicom-pdu-encoder", new PDUEncoder());
        return pipeline;
    }
}
