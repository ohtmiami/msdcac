/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelLocal;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRQ;
import org.oht.miami.msdcache.dicom.pdu.PDV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AssociationHandler extends SimpleChannelUpstreamHandler {

    static final ChannelLocal<Association> associations = new ChannelLocal<Association>(true);

    private static Logger LOG = LoggerFactory.getLogger(AssociationHandler.class);

    private DicomServer dicomServer;

    private Association association;

    public AssociationHandler(DicomServer dicomServer) {

        this.dicomServer = dicomServer;
        association = null;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

        Object message = e.getMessage();
        if (message instanceof PDUError) {
            PDUError pduError = (PDUError) message;
            association.abort(pduError.getAAbort());
            return;
        }
        try {
            if (message instanceof AAssociateRJ) {
                association.receivedAssociateRJ((AAssociateRJ) message);
            } else if (message instanceof AReleaseRQ) {
                association.receivedReleaseRQ();
            } else if (message instanceof AReleaseRP) {
                association.receivedReleaseRP();
            } else if (message instanceof AAbort) {
                association.receivedAbort((AAbort) message);
            } else if (message instanceof AAssociateRQ) {
                association.receivedAssociateRQ((AAssociateRQ) message);
            } else if (message instanceof AAssociateAC) {
                association.receivedAssociateAC((AAssociateAC) message);
            } else if (message instanceof PDV) {
                association.receivedPDV((PDV) message);
            }
        } catch (AAbort aa) {
            association.abort(aa);
        } finally {
            AssociationState state = association.getState();
            if (state == AssociationState.STA1 || state == AssociationState.STA13) {
                association.closeChannel();
            }
        }
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        Channel channel = e.getChannel();
        dicomServer.getAllChannels().add(channel);
        LOG.info("Accepted connection from {}", channel.getRemoteAddress());

        association = Association.accept(channel, dicomServer);
        associations.set(channel, association);
        super.channelOpen(ctx, e);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        Channel channel = e.getChannel();
        LOG.info("Disconnected from {}", channel.getRemoteAddress());
        super.channelClosed(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {

        LOG.warn("exception in state " + association.getState(), e.getCause());
        association.closeChannel();
        super.exceptionCaught(ctx, e);
    }
}
