/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.File;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.io.DicomOutputStream;

public class DicomObjectSaver extends DicomObjectParser {

    private final File outputDir;

    private final String cuid;

    private final String iuid;

    public DicomObjectSaver(InputStream in, TransferSyntax transferSyntax, File outputDir,
            String cuid, String iuid) {

        super(in, transferSyntax);
        this.outputDir = outputDir;
        this.cuid = cuid;
        this.iuid = iuid;
        outputDir.mkdirs();
    }

    @Override
    public DicomObject call() throws Exception {

        this.target.initFileMetaInformation(cuid, iuid, transferSyntax.uid());
        try {
            // TODO Write to file directly without parsing
            super.call();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof Exception) {
                throw (Exception) e.getCause();
            }
        }
        File file = new File(outputDir, iuid + ".dcm");
        DicomOutputStream dicomOut = new DicomOutputStream(file);
        try {
            dicomOut.writeDicomFile(this.target);
        } finally {
            dicomOut.close();
        }
        return this.target;
    }
}
