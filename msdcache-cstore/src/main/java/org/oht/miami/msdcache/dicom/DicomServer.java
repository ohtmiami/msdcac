/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class DicomServer {

    private final ChannelGroup allChannels = new DefaultChannelGroup("dicom-server");

    private int port;

    private ChannelFactory channelFactory;

    private ChannelPipelineFactory pipelineFactory = new DicomPipelineFactory(this);

    private NetworkApplicationEntity[] networkAEs = {};

    public ChannelGroup getAllChannels() {

        return allChannels;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {

        this.port = port;
    }

    public NetworkApplicationEntity[] getNetworkApplicationEntities() {

        return networkAEs;
    }

    public void setNetworkApplicationEntities(NetworkApplicationEntity[] networkAEs) {

        for (NetworkApplicationEntity ae : networkAEs) {
            ae.setDicomServer(this);
        }
        this.networkAEs = networkAEs;
    }

    public void setNetworkApplicationEntity(NetworkApplicationEntity networkAE) {

        setNetworkApplicationEntities(new NetworkApplicationEntity[] { networkAE });
    }

    public void start() {

        channelFactory = new NioServerSocketChannelFactory(Executors.newCachedThreadPool(),
                Executors.newCachedThreadPool());
        ServerBootstrap bootstrap = new ServerBootstrap(channelFactory);
        bootstrap.setPipelineFactory(pipelineFactory);
        Channel serverChannel = bootstrap.bind(new InetSocketAddress(port));
        allChannels.add(serverChannel);
    }

    public void stop() {

        ChannelGroupFuture closeFuture = allChannels.close();
        closeFuture.awaitUninterruptibly();
        channelFactory.releaseExternalResources();
    }

    public NetworkApplicationEntity getNetworkApplicationEntity(String aet) {

        for (NetworkApplicationEntity ae : networkAEs) {
            String aeti = ae.getAETitle();
            if (aeti == null || aeti.equals(aet)) {
                return ae;
            }
        }
        return null;
    }
}
