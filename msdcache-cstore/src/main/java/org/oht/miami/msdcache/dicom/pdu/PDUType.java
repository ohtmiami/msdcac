/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom.pdu;

public class PDUType {

    public static final int A_ASSOCIATE_RQ = 0x01;
    public static final int A_ASSOCIATE_AC = 0x02;
    public static final int A_ASSOCIATE_RJ = 0x03;
    public static final int P_DATA_TF      = 0x04;
    public static final int A_RELEASE_RQ   = 0x05;
    public static final int A_RELEASE_RP   = 0x06;
    public static final int A_ABORT        = 0x07;
}
