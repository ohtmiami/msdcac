/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache;

import org.dcm4che2.data.UID;
import org.oht.miami.msdcache.dicom.DicomServer;
import org.oht.miami.msdcache.dicom.NetworkApplicationEntity;
import org.oht.miami.msdcache.dicom.TransferCapability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomReceiver {

    private static Logger LOG = LoggerFactory.getLogger(DicomReceiver.class);

    private static final String[] CUIDS = { UID.BasicStudyContentNotificationSOPClassRetired,
                    UID.StoredPrintStorageSOPClassRetired,
                    UID.HardcopyGrayscaleImageStorageSOPClassRetired,
                    UID.HardcopyColorImageStorageSOPClassRetired,
                    UID.ComputedRadiographyImageStorage,
                    UID.DigitalXRayImageStorageForPresentation,
                    UID.DigitalXRayImageStorageForProcessing,
                    UID.DigitalMammographyXRayImageStorageForPresentation,
                    UID.DigitalMammographyXRayImageStorageForProcessing,
                    UID.DigitalIntraOralXRayImageStorageForPresentation,
                    UID.DigitalIntraOralXRayImageStorageForProcessing,
                    UID.StandaloneModalityLUTStorageRetired, UID.EncapsulatedPDFStorage,
                    UID.StandaloneVOILUTStorageRetired,
                    UID.GrayscaleSoftcopyPresentationStateStorageSOPClass,
                    UID.ColorSoftcopyPresentationStateStorageSOPClass,
                    UID.PseudoColorSoftcopyPresentationStateStorageSOPClass,
                    UID.BlendingSoftcopyPresentationStateStorageSOPClass,
                    UID.XRayAngiographicImageStorage, UID.EnhancedXAImageStorage,
                    UID.XRayRadiofluoroscopicImageStorage, UID.EnhancedXRFImageStorage,
                    UID.XRayAngiographicBiPlaneImageStorageRetired,
                    UID.PositronEmissionTomographyImageStorage,
                    UID.StandalonePETCurveStorageRetired, UID.CTImageStorage,
                    UID.EnhancedCTImageStorage, UID.NuclearMedicineImageStorage,
                    UID.UltrasoundMultiFrameImageStorageRetired,
                    UID.UltrasoundMultiFrameImageStorage, UID.MRImageStorage,
                    UID.EnhancedMRImageStorage, UID.MRSpectroscopyStorage, UID.RTImageStorage,
                    UID.RTDoseStorage, UID.RTStructureSetStorage,
                    UID.RTBeamsTreatmentRecordStorage, UID.RTPlanStorage,
                    UID.RTBrachyTreatmentRecordStorage, UID.RTTreatmentSummaryRecordStorage,
                    UID.NuclearMedicineImageStorageRetired, UID.UltrasoundImageStorageRetired,
                    UID.UltrasoundImageStorage, UID.RawDataStorage, UID.SpatialRegistrationStorage,
                    UID.SpatialFiducialsStorage, UID.RealWorldValueMappingStorage,
                    UID.SecondaryCaptureImageStorage,
                    UID.MultiFrameSingleBitSecondaryCaptureImageStorage,
                    UID.MultiFrameGrayscaleByteSecondaryCaptureImageStorage,
                    UID.MultiFrameGrayscaleWordSecondaryCaptureImageStorage,
                    UID.MultiFrameTrueColorSecondaryCaptureImageStorage,
                    UID.VLImageStorageTrialRetired, UID.VLEndoscopicImageStorage,
                    UID.VideoEndoscopicImageStorage, UID.VLMicroscopicImageStorage,
                    UID.VideoMicroscopicImageStorage,
                    UID.VLSlideCoordinatesMicroscopicImageStorage, UID.VLPhotographicImageStorage,
                    UID.VideoPhotographicImageStorage, UID.OphthalmicPhotography8BitImageStorage,
                    UID.OphthalmicPhotography16BitImageStorage,
                    UID.StereometricRelationshipStorage, UID.VLMultiFrameImageStorageTrialRetired,
                    UID.StandaloneOverlayStorageRetired, UID.BasicTextSRStorage,
                    UID.EnhancedSRStorage, UID.ComprehensiveSRStorage, UID.ProcedureLogStorage,
                    UID.MammographyCADSRStorage, UID.KeyObjectSelectionDocumentStorage,
                    UID.ChestCADSRStorage, UID.XRayRadiationDoseSRStorage,
                    UID.EncapsulatedPDFStorage, UID.EncapsulatedCDAStorage,
                    UID.StandaloneCurveStorageRetired, UID.TwelveLeadECGWaveformStorage,
                    UID.GeneralECGWaveformStorage, UID.AmbulatoryECGWaveformStorage,
                    UID.HemodynamicWaveformStorage, UID.CardiacElectrophysiologyWaveformStorage,
                    UID.BasicVoiceAudioWaveformStorage, UID.HangingProtocolStorage,
                    UID.SiemensCSANonImageStorage,
                    UID.Dcm4cheAttributesModificationNotificationSOPClass,
                    UID.MultiSeriesStudyStorage };

    private static final String[] TSUIDS = { UID.JPEGLSLossless, UID.JPEGLossless,
                    UID.JPEGLosslessNonHierarchical14, UID.JPEG2000LosslessOnly,
                    UID.DeflatedExplicitVRLittleEndian, UID.RLELossless,
                    UID.ExplicitVRLittleEndian, UID.ExplicitVRBigEndian,
                    UID.ImplicitVRLittleEndian, UID.JPEGBaseline1, UID.JPEGExtended24,
                    UID.JPEGLSLossyNearLossless, UID.JPEG2000, UID.MPEG2, };

    public static void main(String[] args) {

        NetworkApplicationEntity serverAE = new NetworkApplicationEntity();
        serverAE.setAETitle("DCMRCV_NETTY");
        serverAE.setAssociationAcceptor(true);
        initTransferCapabilities(serverAE);

        final DicomServer server = new DicomServer();
        server.setPort(11112);
        server.setNetworkApplicationEntity(serverAE);

        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {

                LOG.info("Shutting down DICOM server...");
                server.stop();
            }
        });

        LOG.info("Starting DICOM server...");
        server.start();
    }

    private static void initTransferCapabilities(NetworkApplicationEntity ae) {

        TransferCapability[] tcs = new TransferCapability[CUIDS.length];
        for (int i = 0; i < tcs.length; i++) {
            tcs[i] = new TransferCapability(CUIDS[i], TSUIDS, TransferCapability.SCP);
        }
        ae.setTransferCapabilities(tcs);
    }
}
