/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

public class ExtQueryTransferCapability extends TransferCapability {

    public static final int RELATIONAL_QUERIES = 0;
    public static final int DATE_TIME_MATCHING = 1;
    public static final int FUZZY_SEMANTIC_PN_MATCHING = 2;

    public ExtQueryTransferCapability(String sopClass, String[] transferSyntax, String role) {

        super(sopClass, transferSyntax, role);
        super.setExtInfo(new byte[3]);
    }
}
