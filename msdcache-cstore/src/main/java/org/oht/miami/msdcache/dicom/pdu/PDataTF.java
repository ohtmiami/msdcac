/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom.pdu;

import org.dcm4che2.data.DicomObject;

public class PDataTF {

    private int pcid;

    private DicomObject command;

    private String tsuid;

    public PDataTF(int pcid, DicomObject command, String tsuid) {

        this.pcid = pcid;
        this.command = command;
        this.tsuid = tsuid;
    }

    public int getPCID() {

        return pcid;
    }

    public DicomObject getCommand() {

        return command;
    }

    public String getTransferSyntax() {

        return tsuid;
    }
}
