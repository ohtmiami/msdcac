/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.oht.miami.msdcache.dicom.pdu.PDV;

public class AssociationState {

    /**
     * State 1 = Idle.
     */
    public static final AssociationState STA1 = new Sta1();

    /**
     * State 2 = Transport connection open (Awaiting A-ASSOCIATE-RQ PDU).
     */
    public static final AssociationState STA2 = new Sta2();

    /**
     * State 3 = Awaiting local A-ASSOCIATE response primitive (from local
     * user).
     */
    public static final AssociationState STA3 = new Sta3();

    /**
     * State 4 = Awaiting transport connection opening to complete (from local
     * transport service).
     */
    public static final AssociationState STA4 = new Sta4();

    /**
     * State 5 = Awaiting A-ASSOCIATE-AC or A-ASSOCIATE-RJ PDU.
     */
    public static final AssociationState STA5 = new Sta5();

    /**
     * State 6 = Association established and ready for data transfer.
     */
    public static final AssociationState STA6 = new Sta6();

    /**
     * State 7 = Awaiting A-RELEASE-RP PDU.
     */
    public static final AssociationState STA7 = new Sta7();

    /**
     * State 8 = Awaiting local A-RELEASE response primitive (from local user).
     */
    public static final AssociationState STA8 = new Sta8();

    /**
     * State 10 = Release collision acceptor side; awaiting A-RELEASE-RP PDU.
     */
    public static final AssociationState STA10 = new Sta10();

    /**
     * State 11 = Release collision requestor side; awaiting A-RELEASE-RP PDU.
     */
    public static final AssociationState STA11 = new Sta11();

    /**
     * State 13 = Awaiting transport connection close indication (association no
     * longer exists).
     */
    public static final AssociationState STA13 = new Sta13();

    protected final String name;

    AssociationState(String name) {

        this.name = name;
    }

    @Override
    public String toString() {

        return name;
    }

    private static class Sta1 extends AssociationState {

        public Sta1() {

            super("Sta1");
        }

        @Override
        void abort(Association association, AAbort aa) {

            // NOOP
        }
    }

    private static class Sta2 extends AssociationState {

        public Sta2() {

            super("Sta2");
        }

        @Override
        void receivedAssociateRQ(Association association, AAssociateRQ rq) throws IOException {

            association.onAssociateRQ(rq);
        }
    }

    private static class Sta3 extends AssociationState {

        public Sta3() {

            super("Sta3");
        }
    }

    private static class Sta4 extends AssociationState {

        public Sta4() {

            super("Sta4");
        }
    }

    private static class Sta5 extends AssociationState {

        public Sta5() {

            super("Sta5");
        }

        @Override
        void receivedAssociateAC(Association association, AAssociateAC ac) throws IOException {

            association.onAssociateAC(ac);
        }

        @Override
        void receivedAssociateRJ(Association association, AAssociateRJ rj) throws IOException {

            association.onAssociateRJ(rj);
        }
    }

    private static class Sta6 extends AssociationState {

        public Sta6() {

            super("Sta6");
        }

        @Override
        void receivedPDV(Association association, PDV pdv) throws IOException {

            association.onPDV(pdv);
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            association.onReleaseRQ();
        }

        @Override
        boolean isReadyForDataTransfer() {

            return true;
        }

        @Override
        boolean isReadyForDataSend() {

            return true;
        }

        @Override
        boolean isReadyForDataReceive() {

            return true;
        }
    }

    private static class Sta7 extends AssociationState {

        public Sta7() {

            super("Sta7");
        }

        @Override
        void receivedPDV(Association association, PDV pdv) throws IOException {

            association.onPDV(pdv);
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            association.onCollisionReleaseRQ();
        }

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            association.onReleaseRP();
        }

        @Override
        boolean isReadyForDataReceive() {

            return true;
        }
    }

    private static class Sta8 extends AssociationState {

        public Sta8() {

            super("Sta8");
        }

        @Override
        boolean isReadyForDataSend() {

            return true;
        }
    }

    private static class Sta10 extends AssociationState {

        public Sta10() {

            super("Sta10");
        }

        @Override
        void receivedReleaseRQ(Association association) throws IOException {

            association.onCollisionReleaseRP();
        }
    }

    private static class Sta11 extends AssociationState {

        public Sta11() {

            super("Sta11");
        }

        @Override
        void receivedReleaseRP(Association association) throws IOException {

            association.onReleaseRP();
        }
    }

    private static class Sta13 extends AssociationState {

        public Sta13() {

            super("Sta13");
        }

        @Override
        void abort(Association association, AAbort aa) {

            // NOOP
        }
    }

    void receivedAssociateRQ(Association association, AAssociateRQ rq) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-RQ");
    }

    void receivedAssociateAC(Association association, AAssociateAC ac) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-AC");
    }

    void receivedAssociateRJ(Association association, AAssociateRJ rj) throws IOException {

        association.unexpectedPDU("A-ASSOCIATE-RJ");
    }

    void receivedPDV(Association association, PDV pdv) throws IOException {

        association.unexpectedPDU("P-DATA-TF");
    }

    void receivedReleaseRQ(Association association) throws IOException {

        association.unexpectedPDU("A-RELEASE-RQ");
    }

    void receivedReleaseRP(Association association) throws IOException {

        association.unexpectedPDU("A-RELEASE-RP");
    }

    void abort(Association association, AAbort aa) {

        association.writeAbort(aa);
    }

    boolean isReadyForDataTransfer() {

        return false;
    }

    boolean isReadyForDataSend() {

        return false;
    }

    boolean isReadyForDataReceive() {

        return false;
    }
}
