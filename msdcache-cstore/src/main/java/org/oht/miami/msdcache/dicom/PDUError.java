/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;

import org.dcm4che2.net.pdu.AAbort;

class PDUError extends IOException {

    private static final long serialVersionUID = -3211487336363534990L;

    PDUError(String message, AAbort cause) {

        super(message, cause);
    }

    PDUError(IOException cause) {

        super(cause);
    }
    
    AAbort getAAbort() {
        
        return (AAbort) this.getCause();
    }
}
