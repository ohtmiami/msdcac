/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.VR;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.UserIdentity;
import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.util.CloseUtils;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.PDV;
import org.oht.miami.msdcache.dicom.pdu.PDVType;
import org.oht.miami.msdcache.dicom.pdu.PDataTF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Association {

    private static Logger LOG = LoggerFactory.getLogger(Association.class);

    private static int nextSerialNo = 0;

    private final int serialNo;

    private final DicomServer dicomServer;

    private final Channel channel;

    private final boolean requestor;

    private NetworkApplicationEntity ae;

    private UserIdentity userIdentity;

    private AssociationState state;

    private String name;

    private AAssociateRQ associateRQ;

    private AAssociateAC associateAC;

    private IOException exception;

    private int maxOpsInvoked;

    private int maxPDULength;

    private boolean closed;

    private Map<String, Map<String, PresentationContext>> acceptedPCs = new HashMap<String, Map<String, PresentationContext>>();

    private Map<String, TransferCapability> scuTCs = new HashMap<String, TransferCapability>();

    private Map<String, TransferCapability> scpTCs = new HashMap<String, TransferCapability>();

    private int expectedPDVType = PDVType.COMMAND;

    private PresentationContext chosenPC = null;

    private PipedOutputStream pdvOut = null;

    private PipedInputStream pdvIn = null;

    private DicomObject command = null;

    private ExecutorService executor = Executors.newCachedThreadPool();

    private Future<DicomObject> commandParserFuture = null;

    private Future<DicomObject> dataSetParserFuture = null;

    protected Association(Channel channel, DicomServer dicomServer, boolean requestor) {

        if (channel == null) {
            throw new NullPointerException("channel");
        }
        if (dicomServer == null) {
            throw new NullPointerException("dicom server");
        }
        this.serialNo = ++nextSerialNo;
        this.name = "Association(" + serialNo + ")";
        this.dicomServer = dicomServer;
        this.channel = channel;
        this.requestor = requestor;
        closed = false;
        // Make sure the current thread is not used by the thread pool
        // TODO Verify that this does work
        ((ThreadPoolExecutor) executor).setCorePoolSize(1);
    }

    public final boolean isRequestor() {

        return requestor;
    }

    AssociationState getState() {

        return state;
    }

    void setState(AssociationState state) {

        synchronized (this) {
            if (this.state == state) {
                return;
            }
            this.state = state;
            this.notifyAll();
        }
    }

    @Override
    public String toString() {

        return name;
    }

    public static Association request(Channel channel, DicomServer dicomServer,
            NetworkApplicationEntity ae, UserIdentity userIdentity) {

        Association association = new Association(channel, dicomServer, true);
        association.setApplicationEntity(ae);
        association.setUserIdentity(userIdentity);
        association.setState(AssociationState.STA4);
        return association;
    }

    public static Association accept(Channel channel, DicomServer dicomServer) {

        Association association = new Association(channel, dicomServer, false);
        association.setState(AssociationState.STA2);
        return association;
    }

    final void setApplicationEntity(NetworkApplicationEntity ae) {

        this.ae = ae;
    }

    final void setUserIdentity(UserIdentity userIdentity) {

        this.userIdentity = userIdentity;
    }

    public final AAssociateAC getAssociateAC() {

        return associateAC;
    }

    public final AAssociateRQ getAssociateRQ() {

        return associateRQ;
    }

    public final boolean isReadyForDataTransfer() {

        return state.isReadyForDataTransfer();
    }

    private boolean isReadyForDataReceive() {

        return state.isReadyForDataReceive();
    }

    private void processAC() {

        Collection<PresentationContext> c = associateAC.getPresentationContexts();
        for (PresentationContext pc : c) {
            if (!pc.isAccepted()) {
                continue;
            }
            PresentationContext pcrq = associateRQ.getPresentationContext(pc.getPCID());
            if (pcrq == null) {
                LOG.warn("{}: Missing Presentation Context(id={}) in received AA-AC", name,
                        pc.getPCID());
                continue;
            }
            String as = pcrq.getAbstractSyntax();
            Map<String, PresentationContext> ts2pc = acceptedPCs.get(as);
            if (ts2pc == null) {
                ts2pc = new HashMap<String, PresentationContext>();
                acceptedPCs.put(as, ts2pc);
            }
            ts2pc.put(pc.getTransferSyntax(), pc);
        }
        for (Map.Entry<String, Map<String, PresentationContext>> entry : acceptedPCs.entrySet()) {
            String asuid = entry.getKey();
            Map<String, PresentationContext> ts2pc = entry.getValue();
            String[] tsuids = ts2pc.keySet().toArray(new String[ts2pc.size()]);
            String cuid = asuid;
            ExtendedNegotiation extneg = associateAC.getExtendedNegotiationFor(cuid);
            byte[] extinfo = extneg != null ? extneg.getInformation() : null;
            if (isSCUFor(cuid)) {
                TransferCapability tc = new TransferCapability(cuid, tsuids, TransferCapability.SCU);
                tc.setExtInfo(extinfo);
                scuTCs.put(cuid, tc);
            }
            if (isSCPFor(cuid)) {
                TransferCapability tc = new TransferCapability(cuid, tsuids, TransferCapability.SCP);
                tc.setExtInfo(extinfo);
                scpTCs.put(cuid, tc);
            }
        }
    }

    private boolean isSCPFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return !requestor;
        }
        return requestor ? rs.isSCP() : rs.isSCU();
    }

    private boolean isSCUFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return requestor;
        }
        return requestor ? rs.isSCU() : rs.isSCP();
    }

    void writeDimseRSP(int pcid, DicomObject command) {

        writeDimseRSP(pcid, command, null);
    }

    void writeDimseRSP(int pcid, DicomObject command, DicomObject dataSet) {

        if (!CommandUtils.isResponse(command)) {
            throw new IllegalArgumentException("command:\n" + command);
        }
        PresentationContext pc = associateAC.getPresentationContext(pcid);
        if (pc == null) {
            throw new IllegalStateException("No Presentation Context with id - " + pcid);
        }
        if (!pc.isAccepted()) {
            throw new IllegalStateException("Presentation Context not accepted - " + pc);
        }

        int dataSetType = CommandUtils.NO_DATASET;
        // TODO What if dataSet != null
        command.putInt(Tag.CommandDataSetType, VR.US, dataSetType);
        // TODO Run this in a separate thread?
        channel.write(new PDataTF(pcid, command, pc.getTransferSyntax()));
    }

    void onDimseRSP(DicomObject command, DicomObject dataSet) throws IOException {

        // TODO
        LOG.info("In onDimseRSP");
    }

    void closeChannel() {

        if (state == AssociationState.STA13) {
            // TODO Sleep
        }
        setState(AssociationState.STA1);
        if (!closed) {
            LOG.info("{}: close {}", name, channel);
            channel.close();
            closed = true;
            onClosed();
        }
    }

    private void onClosed() {

        if (ae != null) {
            ae.removeFromPool(this);
        }
        // TODO rspHandlerForMsgId
        if (ae != null) {
            ae.associationClosed(this);
        }
    }

    void receivedAssociateRQ(AAssociateRQ rq) throws IOException {

        LOG.info("{}: A-ASSOCIATE-RQ {} >> {}",
                new String[] { name, rq.getCallingAET(), rq.getCalledAET() });
        state.receivedAssociateRQ(this, rq);
    }

    void receivedAssociateAC(AAssociateAC ac) throws IOException {

        LOG.info("{}: A-ASSOCIATE-AC {} >> {}",
                new String[] { name, ac.getCallingAET(), ac.getCalledAET() });
        state.receivedAssociateAC(this, ac);
    }

    void receivedAssociateRJ(AAssociateRJ rj) throws IOException {

        LOG.info("{} >> {}", name, rj);
        state.receivedAssociateRJ(this, rj);
    }

    void receivedPDV(PDV pdv) throws IOException {

        state.receivedPDV(this, pdv);
    }

    void onPDV(PDV pdv) throws IOException {

        if (pdv.getType() != expectedPDVType) {
            LOG.warn(
                    "{}: "
                            + (expectedPDVType == PDVType.COMMAND ? "Expected Command but received Data Set PDV"
                                    : "Expected Data Set but received Command PDV"), this);
            throw new AAbort();
        }

        int pcid = pdv.getPCID();
        if (chosenPC == null) {
            PresentationContext pc = getAssociateAC().getPresentationContext(pcid);
            if (pc == null) {
                LOG.warn("{}: No Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            if (!pc.isAccepted()) {
                LOG.warn("{}: No accepted Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            chosenPC = pc;
        } else {
            int chosenPCID = chosenPC.getPCID();
            if (pcid != chosenPCID) {
                LOG.warn("{}: Expected PDV with pcid: {} but received with pcid: {}", new Object[] {
                                this, chosenPCID, pcid });
                throw new AAbort();
            }
        }

        if (pdvOut == null) {
            pdvOut = new PipedOutputStream();
            // TODO Tune pipe size
            pdvIn = new PipedInputStream(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                DicomObjectParser commandParser = new DicomObjectParser(pdvIn,
                        TransferSyntax.ImplicitVRLittleEndian);
                commandParserFuture = executor.submit(commandParser);
            } else {
                DicomObjectParser dataSetParser = new DicomObjectSaver(pdvIn,
                        TransferSyntax.valueOf(chosenPC.getTransferSyntax()), new File(
                                "target/test-out/received"),
                        command.getString(Tag.AffectedSOPClassUID),
                        command.getString(Tag.AffectedSOPInstanceUID));
                dataSetParserFuture = executor.submit(dataSetParser);
            }
        }
        ChannelBuffer pdvData = pdv.getData();
        pdvData.readBytes(pdvOut, pdvData.readableBytes());

        if (pdv.isLast()) {
            CloseUtils.safeClose(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                onLastCommandPDV();
            } else {
                onLastDataSetPDV();
            }
        }
    }

    private void onLastCommandPDV() throws IOException {

        try {
            command = commandParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn("{}: Interrupted while waiting for Command parser: {}", this, e.getMessage());
            throw new AAbort();
        } catch (ExecutionException e) {
            LOG.warn("{}: Failed to parse Command: {}", this, e.getCause().getMessage());
            throw new AAbort();
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;
        if (CommandUtils.hasDataset(command)) {
            expectedPDVType = PDVType.DATA;
        } else {
            if (CommandUtils.isResponse(command)) {
                onDimseRSP(command, null);
            } else {
                onDimseRQ(chosenPC, command, null);
            }
            chosenPC = null;
        }
    }

    private void onLastDataSetPDV() throws IOException {

        DicomObject dataSet = null;
        try {
            dataSet = dataSetParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn("{}: Interrupted while waiting for Data Set parser: {}", this, e.getMessage());
            throw new AAbort();
        } catch (ExecutionException e) {
            LOG.warn("{}: Failed to parse Data Set: {}", this, e.getCause().getMessage());
            throw new AAbort();
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;
        expectedPDVType = PDVType.COMMAND;
        if (CommandUtils.isResponse(command)) {
            onDimseRSP(command, dataSet);
        } else {
            onDimseRQ(chosenPC, command, dataSet);
        }
        chosenPC = null;
    }

    void receivedReleaseRQ() throws IOException {

        LOG.info("{} >> A-RELEASE-RQ", name);
        state.receivedReleaseRQ(this);
    }

    void receivedReleaseRP() throws IOException {

        LOG.info("{} >> A-RELEASE-RP", name);
        state.receivedReleaseRP(this);
    }

    void receivedAbort(AAbort aa) {

        LOG.info("{}: >> {}", name, aa);
        exception = aa;
        setState(AssociationState.STA1);
        ae.removeFromPool(this);
    }

    void onDimseRQ(PresentationContext pc, DicomObject command, DicomObject dataSet) {

        int commandField = command.getInt(Tag.CommandField);
        if (commandField == CommandUtils.C_STORE_RQ) {
            DicomObject rsp = CommandUtils.mkRSP(command, CommandUtils.SUCCESS);
            writeDimseRSP(pc.getPCID(), rsp);
        } else {
            // TODO Add support for Command Fields other than C-STORE-RQ
            LOG.info("{}: Got Command:\n{}", this, command);
        }
    }

    void abort(AAbort aa) {

        if (ae != null) {
            ae.removeFromPool(this);
        }
        state.abort(this, aa);
    }

    void writeAbort(AAbort aa) {

        exception = aa;
        setState(AssociationState.STA13);
        ChannelFuture writeFuture = channel.write(aa);
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                closeChannel();
            }
        });
    }

    void unexpectedPDU(String name) throws AAbort {
        LOG.warn("received unexpected {} in state: {}", name, state);
        throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.UNEXPECTED_PDU);
    }

    void onAssociateRQ(AAssociateRQ rq) {

        associateRQ = rq;
        name = rq.getCallingAET() + "(" + serialNo + ")";
        setState(AssociationState.STA3);
        try {
            if ((rq.getProtocolVersion() & 1) == 0) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_PROVIDER_ACSE,
                        AAssociateRJ.REASON_PROTOCOL_VERSION_NOT_SUPPORTED);
            }
            if (!rq.getApplicationContext().equals(UID.DICOMApplicationContextName)) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_USER,
                        AAssociateRJ.REASON_APP_CTX_NAME_NOT_SUPPORTED);
            }
            NetworkApplicationEntity ae = dicomServer
                    .getNetworkApplicationEntity(rq.getCalledAET());
            if (ae == null) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_USER,
                        AAssociateRJ.REASON_CALLED_AET_NOT_RECOGNIZED);
            }
            // TODO checkConnectionCountWithinLimit()
            setApplicationEntity(ae);
            associateAC = ae.negotiate(this, rq);
            processAC();
            maxOpsInvoked = associateAC.getMaxOpsInvoked();
            maxPDULength = minZeroAsMax(rq.getMaxPDULength(), ae.getMaxPDULengthSend());
            setState(AssociationState.STA6);
            channel.write(associateAC);
            ae.addToPool(this);
            ae.associationAccepted(this);
        } catch (AAssociateRJ rj) {
            setState(AssociationState.STA13);
            channel.write(rj);
        }
    }

    void onAssociateAC(AAssociateAC ac) {

        associateAC = ac;
        processAC();
        maxOpsInvoked = associateAC.getMaxOpsInvoked();
        maxPDULength = minZeroAsMax(associateAC.getMaxPDULength(), ae.getMaxPDULengthSend());
        setState(AssociationState.STA6);
    }

    private int minZeroAsMax(int i1, int i2) {

        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }

    void onAssociateRJ(AAssociateRJ rj) {

        exception = rj;
        setState(AssociationState.STA1);
    }

    void onReleaseRP() {

        setState(AssociationState.STA1);
    }

    void onCollisionReleaseRP() {

        LOG.info("{} << A-RELEASE-RP", name);
        setState(AssociationState.STA13);
        channel.write(new AReleaseRP());
    }

    void onReleaseRQ() {

        setState(AssociationState.STA8);
        if (ae != null) {
            ae.removeFromPool(this);
        }
        // TODO waitForPerformingOps();
        setState(AssociationState.STA13);
        channel.write(new AReleaseRP());
    }

    void onCollisionReleaseRQ() {

        if (requestor) {
            setState(AssociationState.STA1);
            channel.write(new AReleaseRP());
        } else {
            setState(AssociationState.STA10);
        }
    }
}
