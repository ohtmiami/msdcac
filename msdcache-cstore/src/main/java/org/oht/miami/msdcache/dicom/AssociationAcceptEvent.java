/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.util.EventObject;

public class AssociationAcceptEvent extends EventObject {

    private static final long serialVersionUID = 4066828952860720425L;

    private final Association association;

    public AssociationAcceptEvent(NetworkApplicationEntity ae, Association association) {

        super(ae);
        this.association = association;
    }

    public final Association getAssociation() {

        return association;
    }
}
