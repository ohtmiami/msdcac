/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.InputStream;
import java.util.concurrent.Callable;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.io.DicomInputStream;

public class DicomObjectParser implements Callable<DicomObject> {

    protected final InputStream in;

    protected final TransferSyntax transferSyntax;

    protected final DicomObject target;

    public DicomObjectParser(InputStream in, TransferSyntax transferSyntax) {

        this(in, transferSyntax, null);
    }

    public DicomObjectParser(InputStream in, TransferSyntax transferSyntax, DicomObject target) {

        this.in = in;
        this.transferSyntax = transferSyntax;
        this.target = target == null ? new BasicDicomObject() : target;
    }

    @Override
    public DicomObject call() throws Exception {

        DicomInputStream dicomIn = new DicomInputStream(in, transferSyntax);
        try {
            dicomIn.readDicomObject(target, -1);
        } finally {
            dicomIn.close();
        }
        return target;
    }
}
