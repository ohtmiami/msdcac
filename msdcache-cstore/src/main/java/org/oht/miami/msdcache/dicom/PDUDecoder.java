/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.io.UnsupportedEncodingException;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.AAssociateRQAC;
import org.dcm4che2.net.pdu.CommonExtendedNegotiation;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.net.pdu.UserIdentityAC;
import org.dcm4che2.net.pdu.UserIdentityRQ;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.replay.ReplayingDecoder;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRQ;
import org.oht.miami.msdcache.dicom.pdu.ItemType;
import org.oht.miami.msdcache.dicom.pdu.PDUType;
import org.oht.miami.msdcache.dicom.pdu.PDV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDUDecoder extends ReplayingDecoder<PDUDecoderState> {

    private static Logger LOG = LoggerFactory.getLogger(PDUDecoder.class);

    /**
     * Default PDU Length = 16KB
     */
    private static final int DEF_PDU_LEN = 0x4000;

    /**
     * Maximum PDU Length = 16MB
     */
    private static final int MAX_PDU_LEN = 0x1000000;

    private Association association = null;

    private int pduType = 0;

    private int pduLength = 0;

    private int remainingPDVLength = 0;

    private int pdvLength = 0;

    private int pcid = -1;

    private int messageControlHeader = 0;

    public PDUDecoder() {

        super(PDUDecoderState.READ_HEADER);
    }

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer,
            PDUDecoderState state) throws Exception {

        association = AssociationHandler.associations.get(channel);
        switch (this.getState()) {
        case READ_HEADER:
            ChannelBuffer header = buffer.readSlice(6);
            // PDU Type (1B)
            pduType = header.readUnsignedByte();
            // Reserved (1B)
            header.readUnsignedByte();
            // PDU Length (4B)
            pduLength = header.readInt();
            if (pduType < PDUType.A_ASSOCIATE_RQ || pduType > PDUType.A_ABORT) {
                LOG.warn("{} >> unrecognized PDU[type={}, len={}]", new Object[] { association,
                                pduType, pduLength & 0xFFFFFFFFL });
                return new PDUError(new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.UNRECOGNIZED_PDU));
            }
            this.checkpoint(PDUDecoderState.READ_FIXED_LENGTH_CONTENT);

        case READ_FIXED_LENGTH_CONTENT:
            if (pduType == PDUType.A_ASSOCIATE_RJ || pduType == PDUType.A_RELEASE_RQ
                    || pduType == PDUType.A_RELEASE_RP || pduType == PDUType.A_ABORT) {
                ChannelBuffer fixedLengthContent = buffer.readSlice(4);
                this.checkpoint(PDUDecoderState.READ_HEADER);
                return decodeFixedLengthPDU(fixedLengthContent);
            }
            if (pduLength < 0 || pduLength > MAX_PDU_LEN) {
                LOG.warn("{}: Length of PDU[type={}, len={}] exceeds {} limit", new Object[] {
                                association, pduType, pduLength & 0xFFFFFFFFL, MAX_PDU_LEN });
                this.checkpoint(PDUDecoderState.READ_HEADER);
                return new PDUError(new AAbort(AAbort.UL_SERIVE_PROVIDER,
                        AAbort.INVALID_PDU_PARAMETER_VALUE));
            }
            this.checkpoint(PDUDecoderState.READ_RQAC_CONTENT);

        case READ_RQAC_CONTENT:
            if (pduType != PDUType.P_DATA_TF) {
                ChannelBuffer rqacContent = buffer.readSlice(pduLength);
                this.checkpoint(PDUDecoderState.READ_HEADER);
                switch (pduType) {
                case PDUType.A_ASSOCIATE_RQ:
                    return decodeAAssociateRQAC(rqacContent, new AAssociateRQ());

                case PDUType.A_ASSOCIATE_AC:
                    return decodeAAssociateRQAC(rqacContent, new AAssociateAC());

                default:
                    throw new RuntimeException("Unexpected PDU type: " + pduType);
                }
            }
            this.checkpoint(PDUDecoderState.READ_PDV_HEADER);

        case READ_PDV_HEADER:
            ChannelBuffer pdvHeader = buffer.readSlice(6);
            if (remainingPDVLength == 0) {
                remainingPDVLength = pduLength;
            }
            pdvLength = pdvHeader.readInt();
            if (pdvLength < 2 || 4 + pdvLength > remainingPDVLength) {
                LOG.warn("{}: Invalid PDV item length: {}", association, pdvLength);
                this.checkpoint(PDUDecoderState.READ_HEADER);
                return new PDUError(new AAbort());
            }
            pcid = pdvHeader.readUnsignedByte();
            messageControlHeader = pdvHeader.readUnsignedByte();
            this.checkpoint(PDUDecoderState.READ_PDV_DATA);

        case READ_PDV_DATA:
            ChannelBuffer pdvData = buffer.readSlice(pdvLength - 2);
            remainingPDVLength -= 4 + pdvLength;
            if (remainingPDVLength == 0) {
                this.checkpoint(PDUDecoderState.READ_HEADER);
            } else if (remainingPDVLength < 6) {
                LOG.warn("{}: PDV does not fit in remaining {} bytes of P-DATA-TF[len={}]",
                        new Object[] { association, remainingPDVLength, pduLength });
                remainingPDVLength = 0;
                this.checkpoint(PDUDecoderState.READ_HEADER);
                return new PDUError(new AAbort());
            } else {
                this.checkpoint(PDUDecoderState.READ_PDV_HEADER);
            }
            return new PDV(pcid, messageControlHeader, pdvData);

        default:
            throw new RuntimeException("Unexpected PDU decoder state");
        }
    }

    private Object decodeFixedLengthPDU(ChannelBuffer buffer) {

        if (pduLength != 4) {
            LOG.warn("{}: Invalid length of PDU[type={}, len={}]", new Object[] { association,
                            pduType, pduLength & 0xFFFFFFFFL });
            return new PDUError(new AAbort(AAbort.UL_SERIVE_PROVIDER,
                    AAbort.INVALID_PDU_PARAMETER_VALUE));
        }

        switch (pduType) {
        case PDUType.A_ASSOCIATE_RJ:
            // Reserved (1B)
            buffer.readUnsignedByte();
            return new AAssociateRJ(buffer.readUnsignedByte(), buffer.readUnsignedByte(),
                    buffer.readUnsignedByte());

        case PDUType.A_RELEASE_RQ:
            // Reserved (4B)
            buffer.readUnsignedInt();
            return new AReleaseRQ();

        case PDUType.A_RELEASE_RP:
            // Reserved (4B)
            buffer.readUnsignedInt();
            return new AReleaseRP();

        case PDUType.A_ABORT:
            // Reserved (2B)
            buffer.readUnsignedShort();
            return new AAbort(buffer.readUnsignedByte(), buffer.readUnsignedByte());

        default:
            throw new RuntimeException("Unexpected PDU type: " + pduType);
        }
    }

    private Object decodeAAssociateRQAC(ChannelBuffer buffer, AAssociateRQAC rqac) {

        try {
            // Protocol Version (2B)
            rqac.setProtocolVersion(buffer.readUnsignedShort());
            // Reserved (2B)
            buffer.readUnsignedShort();
            // Called Entity Title (16B)
            rqac.setCalledAET(decodeASCIIString(buffer, 16).trim());
            // Calling Entity Title (16B)
            rqac.setCallingAET(decodeASCIIString(buffer, 16).trim());
            // Reserved (32B)
            rqac.setReservedBytes(decodeBytes(buffer, 32));
            while (buffer.readable()) {
                decodeItem(buffer, rqac);
            }
        } catch (AAbort aa) {
            return new PDUError(aa);
        } catch (IndexOutOfBoundsException e) {
            LOG.warn("{}: Invalid length of PDU[type={}, len={}]", new Object[] { association,
                            pduType, pduLength & 0xFFFFFFFFL });
            return new PDUError(new AAbort(AAbort.UL_SERIVE_PROVIDER,
                    AAbort.INVALID_PDU_PARAMETER_VALUE));
        }
        return rqac;
    }

    private String decodeASCIIString(ChannelBuffer buffer) {

        return decodeASCIIString(buffer, buffer.readUnsignedShort());
    }

    private String decodeASCIIString(ChannelBuffer buffer, int length) {

        byte[] asciiBytes = new byte[length];
        buffer.readBytes(asciiBytes, 0, length);
        int l = length;
        while (l > 0 && asciiBytes[l - 1] == 0) {
            l--;
        }
        try {
            return new String(asciiBytes, 0, l, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("US-ASCII charset is not available", e);
        }
    }

    private byte[] decodeBytes(ChannelBuffer buffer) {

        return decodeBytes(buffer, buffer.readUnsignedShort());
    }

    private byte[] decodeBytes(ChannelBuffer buffer, int length) {

        byte[] bytes = new byte[length];
        buffer.readBytes(bytes);
        return bytes;
    }

    private void decodeItem(ChannelBuffer buffer, AAssociateRQAC rqac) throws AAbort {

        // Item Type (1B)
        int itemType = buffer.readUnsignedByte();
        // Reserved (1B)
        buffer.readUnsignedByte();
        // Item Length (2B)
        int itemLength = buffer.readUnsignedShort();
        switch (itemType) {
        case ItemType.APP_CONTEXT:
            // Application Context (<= 64B)
            rqac.setApplicationContext(decodeASCIIString(buffer, itemLength));
            break;

        case ItemType.RQ_PRES_CONTEXT:
        case ItemType.RQ_USER_IDENTITY:
            rqac.addPresentationContext(decodePC(buffer.readSlice(itemLength)));
            break;

        case ItemType.USER_INFO:
            decodeUserInfo(buffer.readSlice(itemLength), rqac);
            break;

        default:
            buffer.skipBytes(itemLength);
        }
    }

    private PresentationContext decodePC(ChannelBuffer buffer) {

        PresentationContext pc = new PresentationContext();
        // Presentation Context ID (1B)
        pc.setPCID(buffer.readUnsignedByte());
        // Reserved (1B)
        buffer.readUnsignedByte();
        // Result/Reason (1B)
        pc.setResult(buffer.readUnsignedByte());
        // Reserved (1B)
        buffer.readUnsignedByte();
        while (buffer.readable()) {
            decodePCSubItem(buffer, pc);
        }
        return pc;
    }

    private void decodePCSubItem(ChannelBuffer buffer, PresentationContext pc) {

        // Item Type (1B)
        int itemType = buffer.readUnsignedByte();
        // Reserved (1B)
        buffer.readUnsignedByte();
        // Item Length (2B)
        int itemLength = buffer.readUnsignedShort();
        switch (itemType) {
        case ItemType.ABSTRACT_SYNTAX:
            // Abstract Syntax (<= 64B)
            pc.setAbstractSyntax(decodeASCIIString(buffer, itemLength));
            break;

        case ItemType.TRANSFER_SYNTAX:
            // Transfer Syntax (<= 64B)
            pc.addTransferSyntax(decodeASCIIString(buffer, itemLength));
            break;

        default:
            buffer.skipBytes(itemLength);
        }
    }

    private void decodeUserInfo(ChannelBuffer buffer, AAssociateRQAC rqac) throws AAbort {

        while (buffer.readable()) {
            decodeUserInfoSubItem(buffer, rqac);
        }
    }

    private void decodeUserInfoSubItem(ChannelBuffer buffer, AAssociateRQAC rqac) throws AAbort {

        // Item Type (1B)
        int itemType = buffer.readUnsignedByte();
        // Reserved (1B)
        buffer.readUnsignedByte();
        // Item Length (2B)
        int itemLength = buffer.readUnsignedShort();
        switch (itemType) {
        case ItemType.MAX_PDU_LENGTH:
            // Maximum Length Received (4B)
            rqac.setMaxPDULength(buffer.readInt());
            break;

        case ItemType.IMPL_CLASS_UID:
            rqac.setImplClassUID(decodeASCIIString(buffer, itemLength));
            break;

        case ItemType.ASYNC_OPS_WINDOW:
            rqac.setMaxOpsInvoked(buffer.readUnsignedShort());
            rqac.setMaxOpsPerformed(buffer.readUnsignedShort());
            break;

        case ItemType.ROLE_SELECTION:
            rqac.addRoleSelection(decodeRoleSelection(buffer.readSlice(itemLength)));
            break;

        case ItemType.IMPL_VERSION_NAME:
            rqac.setImplVersionName(decodeASCIIString(buffer, itemLength));
            break;

        case ItemType.EXT_NEG:
            rqac.addExtendedNegotiation(decodeExtendedNegotiation(buffer.readSlice(itemLength)));
            break;

        case ItemType.COMMON_EXT_NEG:
            rqac.addCommonExtendedNegotiation(decodeCommonExtendedNegotiation(buffer
                    .readSlice(itemLength)));
            break;

        case ItemType.RQ_USER_IDENTITY:
        case ItemType.AC_USER_IDENTITY:
            if (rqac instanceof AAssociateRQ) {
                ((AAssociateRQ) rqac).setUserIdentity(decodeUserIdentityRQ(buffer
                        .readSlice(itemLength)));
            } else {
                ((AAssociateAC) rqac).setUserIdentity(decodeUserIdentityAC(buffer
                        .readSlice(itemLength)));
            }
            break;

        default:
            buffer.skipBytes(itemLength);
        }
    }

    private RoleSelection decodeRoleSelection(ChannelBuffer buffer) {

        RoleSelection rs = new RoleSelection();
        rs.setSOPClassUID(decodeASCIIString(buffer));
        rs.setSCU(buffer.readUnsignedByte() != 0);
        rs.setSCP(buffer.readUnsignedByte() != 0);
        return rs;
    }

    private ExtendedNegotiation decodeExtendedNegotiation(ChannelBuffer buffer) {

        ExtendedNegotiation extNeg = new ExtendedNegotiation();
        extNeg.setSOPClassUID(decodeASCIIString(buffer));
        extNeg.setInformation(decodeBytes(buffer, buffer.readableBytes()));
        return extNeg;
    }

    private CommonExtendedNegotiation decodeCommonExtendedNegotiation(ChannelBuffer buffer)
            throws AAbort {

        CommonExtendedNegotiation extNeg = new CommonExtendedNegotiation();
        extNeg.setSOPClassUID(decodeASCIIString(buffer));
        extNeg.setServiceClassUID(decodeASCIIString(buffer));
        decodeRelatedGeneralSOPClassUIDs(buffer.readSlice(buffer.readUnsignedShort()), extNeg);
        if (buffer.readable()) {
            LOG.warn(
                    "{}: Mismatch of encoded ({}) with actual ({}) Common Extended Negotiation item length",
                    new Object[] { association, buffer.capacity(),
                                    buffer.capacity() - buffer.readableBytes() });
            throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.INVALID_PDU_PARAMETER_VALUE);
        }
        return extNeg;
    }

    private void decodeRelatedGeneralSOPClassUIDs(ChannelBuffer buffer,
            CommonExtendedNegotiation extNeg) {

        while (buffer.readable()) {
            extNeg.addRelatedGeneralSOPClassUID(decodeASCIIString(buffer));
        }
    }

    private UserIdentityRQ decodeUserIdentityRQ(ChannelBuffer buffer) throws AAbort {

        UserIdentityRQ user = new UserIdentityRQ();
        user.setUserIdentityType(buffer.readUnsignedByte());
        user.setPositiveResponseRequested(buffer.readUnsignedByte() != 0);
        user.setPrimaryField(decodeBytes(buffer));
        user.setSecondaryField(decodeBytes(buffer));
        if (buffer.readable()) {
            LOG.warn(
                    "{}: Mismatch of encoded ({}) with actual ({}) User Identity item length",
                    new Object[] { association, buffer.capacity(),
                                    buffer.capacity() - buffer.readableBytes() });
            throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.INVALID_PDU_PARAMETER_VALUE);
        }
        return user;
    }

    private UserIdentityAC decodeUserIdentityAC(ChannelBuffer buffer) throws AAbort {

        UserIdentityAC user = new UserIdentityAC();
        user.setServerResponse(decodeBytes(buffer));
        if (buffer.readable()) {
            LOG.warn(
                    "{}: Mismatch of encoded ({}) with actual ({}) User Identity item length",
                    new Object[] { association, buffer.capacity(),
                                    buffer.capacity() - buffer.readableBytes() });
            throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.INVALID_PDU_PARAMETER_VALUE);
        }
        return user;
    }
}
