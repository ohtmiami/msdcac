/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import org.dcm4che2.net.pdu.ExtendedNegotiation;

public class ExtStorageTransferCapability extends TransferCapability {

    public static final int LEVEL_OF_SUPPORT = 0;
    public static final int LEVEL_OF_DIGITAL_SIGNATURE_SUPPORT = 2;
    public static final int ELEMENT_COERCION = 4;

    public ExtStorageTransferCapability(String sopClass, String[] transferSyntax, String role) {

        super(sopClass, transferSyntax, role);
        super.setExtInfo(new byte[6]);
    }

    @Override
    public ExtendedNegotiation negotiate(ExtendedNegotiation offered) {

        return extInfo != null ? new ExtendedNegotiation(sopClass, extInfo) : null;
    }
}
