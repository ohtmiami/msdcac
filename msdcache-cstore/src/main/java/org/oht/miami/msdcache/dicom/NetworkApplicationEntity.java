/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetworkApplicationEntity {

    private static Logger LOG = LoggerFactory.getLogger(NetworkApplicationEntity.class);

    private boolean associationAcceptor;

    private boolean associationInitiator;

    private String aeTitle;

    private String[] preferredCallingAETitles = {};

    private String[] preferredCalledAETitles = {};

    private int maxOpsInvoked;

    private int maxOpsPerformed;

    private int maxPDULengthReceive = 0x4000;

    private int maxPDULengthSend = 0x4000;

    private TransferCapability[] transferCapabilities = {};

    private DicomServer dicomServer;

    private AssociationListener[] associationListeners = {};

    private final List<Association> pool = new ArrayList<Association>();

    private AtomicInteger messageID = new AtomicInteger();

    public DicomServer getDicomServer() {

        return dicomServer;
    }

    void setDicomServer(DicomServer dicomServer) {

        this.dicomServer = dicomServer;
    }

    public String getAETitle() {

        return aeTitle;
    }

    public void setAETitle(String aeTitle) {

        this.aeTitle = aeTitle;
    }

    public boolean isAssociationAcceptor() {

        return associationAcceptor;
    }

    public void setAssociationAcceptor(boolean acceptor) {

        associationAcceptor = acceptor;
    }

    public boolean isAssociationInitiator() {

        return associationInitiator;
    }

    public void setAssociationInitiator(boolean initiator) {

        associationInitiator = initiator;
    }

    public String[] getPreferredCalledAETitles() {

        return preferredCalledAETitles;
    }

    public void setPreferredCalledAETitles(String[] aets) {

        preferredCalledAETitles = aets;
    }

    public boolean hasPreferredCalledAETitle() {

        return preferredCalledAETitles != null && preferredCalledAETitles.length > 0;
    }

    public boolean isPreferredCalledAETitle(String aet) {

        return contains(preferredCalledAETitles, aet);
    }

    public String[] getPreferredCallingAETitles() {

        return preferredCallingAETitles;
    }

    public void setPreferredCallingAETitles(String[] aets) {

        preferredCallingAETitles = aets;
    }

    public boolean hasPreferredCallingAETitle() {

        return preferredCallingAETitles != null && preferredCallingAETitles.length > 0;
    }

    public boolean isPreferredCallingAETitle(String aet) {

        return contains(preferredCallingAETitles, aet);
    }

    private static boolean contains(String[] a, String s) {

        for (String si : a) {
            if (si.equals(a)) {
                return true;
            }
        }
        return false;
    }

    public TransferCapability[] getTransferCapabilities() {

        return transferCapabilities;
    }

    public void setTransferCapabilities(TransferCapability[] transferCapabilities) {

        this.transferCapabilities = transferCapabilities;
    }

    public int getMaxPDULengthReceive() {

        return maxPDULengthReceive;
    }

    public void setMaxPDULengthReceive(int maxPDULengthReceive) {

        this.maxPDULengthReceive = maxPDULengthReceive;
    }

    public int getMaxPDULengthSend() {

        return maxPDULengthSend;
    }

    public void setMaxPDULengthSend(int maxPDULengthSend) {

        this.maxPDULengthSend = maxPDULengthSend;
    }

    private TransferCapability findTC(TransferCapability[] tcs, String cuid, boolean scp) {

        for (TransferCapability tc : tcs) {
            if (tc.isSCP() == scp && tc.getSopClass().equals(cuid))
                return tc;
        }
        return null;
    }

    void addToPool(Association association) {

        synchronized (pool) {
            pool.add(association);
        }
    }

    void removeFromPool(Association association) {

        synchronized (pool) {
            pool.remove(association);
        }
    }

    AAssociateAC negotiate(Association association, AAssociateRQ rq) throws AAssociateRJ {

        if (!isAssociationAcceptor()) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER, AAssociateRJ.REASON_NO_REASON_GIVEN);
        }
        String[] calling = getPreferredCallingAETitles();
        if (calling.length != 0 && !isPreferredCallingAETitle(rq.getCallingAET())) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER,
                    AAssociateRJ.REASON_CALLING_AET_NOT_RECOGNIZED);
        }
        // TODO installed
        AAssociateAC ac = new AAssociateAC();
        ac.setCalledAET(rq.getCalledAET());
        ac.setCallingAET(rq.getCallingAET());
        ac.setMaxPDULength(maxPDULengthReceive);
        ac.setMaxOpsInvoked(minZeroAsMax(rq.getMaxOpsInvoked(), maxOpsPerformed));
        ac.setMaxOpsPerformed(minZeroAsMax(rq.getMaxOpsPerformed(), maxOpsInvoked));
        Collection<PresentationContext> pcs = rq.getPresentationContexts();
        for (PresentationContext rqpc : pcs) {
            PresentationContext acpc = negPresCtx(rq, ac, rqpc);
            ac.addPresentationContext(acpc);
        }
        return ac;
    }

    private PresentationContext negPresCtx(AAssociateRQ rq, AAssociateAC ac,
            PresentationContext rqpc) {

        String asuid = rqpc.getAbstractSyntax();
        RoleSelection rqrs = rq.getRoleSelectionFor(asuid);
        TransferCapability tcscp = findTC(transferCapabilities, asuid, true);
        TransferCapability tcscu = findTC(transferCapabilities, asuid, false);
        RoleSelection acrs = ac.getRoleSelectionFor(asuid);
        if (rqrs != null && acrs == null) {
            boolean scp = rqrs.isSCP() && tcscu != null;
            boolean scu = rqrs.isSCU() && tcscp != null;
            acrs = new RoleSelection(asuid, scu, scp);
            ac.addRoleSelection(acrs);
        }
        TransferCapability tc = rqrs == null || acrs.isSCU() ? tcscp : tcscu;

        PresentationContext acpc = new PresentationContext();
        acpc.setPCID(rqpc.getPCID());
        if (tc != null) {
            Set<String> rqts = rqpc.getTransferSyntaxes();
            String[] acts = tc.getTransferSyntax();
            for (int i = 0; i < acts.length; i++) {
                if (rqts.contains(acts[i])) {
                    acpc.addTransferSyntax(acts[i]);
                    if (ac.getExtendedNegotiationFor(asuid) == null) {
                        ExtendedNegotiation extNeg = tc.negotiate(rq
                                .getExtendedNegotiationFor(asuid));
                        if (extNeg != null)
                            ac.addExtendedNegotiation(extNeg);
                    }
                    return acpc;
                }
            }
            acpc.setResult(PresentationContext.TRANSFER_SYNTAX_NOT_SUPPORTED);
        } else {
            acpc.setResult(PresentationContext.ABSTRACT_SYNTAX_NOT_SUPPORTED);
        }
        acpc.addTransferSyntax(rqpc.getTransferSyntax());
        return acpc;
    }

    private int minZeroAsMax(int i1, int i2) {
        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }

    public void addAssociationListener(AssociationListener l) {

        if (l == null) {
            throw new NullPointerException();
        }
        synchronized (associationListeners) {
            AssociationListener[] tmp = new AssociationListener[associationListeners.length + 1];
            System.arraycopy(associationListeners, 0, tmp, 0, associationListeners.length);
            tmp[associationListeners.length] = l;
            associationListeners = tmp;
        }
    }

    public void removeAssociationListener(AssociationListener l) {

        if (l == null) {
            throw new NullPointerException();
        }
        synchronized (associationListeners) {
            for (int i = 0; i < associationListeners.length; i++) {
                if (associationListeners[i].equals(l)) {
                    AssociationListener[] tmp = new AssociationListener[associationListeners.length - 1];
                    System.arraycopy(associationListeners, 0, tmp, 0, i);
                    System.arraycopy(associationListeners, i + 1, tmp, i, tmp.length - i);
                    associationListeners = tmp;
                    return;
                }
            }
        }
    }

    void associationAccepted(Association association) {

        synchronized (associationListeners) {
            for (AssociationListener l : associationListeners) {
                l.associationAccepted(new AssociationAcceptEvent(this, association));
            }
        }
    }

    void associationClosed(Association association) {

        synchronized (associationListeners) {
            for (AssociationListener l : associationListeners) {
                l.associationClosed(new AssociationCloseEvent(this, association));
            }
        }
    }

    int nextMessageID() {

        return messageID.incrementAndGet() & 0xFFFF;
    }
}
