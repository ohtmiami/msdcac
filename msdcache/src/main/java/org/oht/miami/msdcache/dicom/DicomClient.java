/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.dcm4che2.net.UserIdentity;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

public class DicomClient extends DicomConnector {

    private long connectTimeoutMillis = 5000;

    public DicomClient(ExecutorService executorService) {

        super(executorService);
    }

    @Override
    public void start() {

        if (this.bootstrap != null) {
            throw new IllegalStateException("Already started");
        }
        this.bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        this.bootstrap.setPipelineFactory(new DicomClientPipelineFactory(this));
        this.bootstrap.setOption("tcpNoDelay", true);
    }

    public Association connect(String remoteAET, String localAET, TransferCapability[] localTCs)
            throws IOException, InterruptedException {

        if (this.bootstrap == null) {
            throw new IllegalStateException("Not started");
        }
        final NetworkApplicationEntity localAE = this.getLocalApplicationEntity(localAET);
        if (localAE == null) {
            throw new IllegalArgumentException("Unknown local AE " + localAET);
        }
        final UserIdentity userIdentity = localAE.getUserIdentity();

        Association association = null;
        NetworkApplicationEntity remoteAE = this.getRemoteApplicationEntity(remoteAET);
        if (remoteAE == null) {
            throw new IllegalArgumentException("Unknown remote AE " + remoteAET);
        }

        final Object associationLock = new Object();
        InetSocketAddress endPoint = remoteAE.getAddress();
        InetSocketAddress bindPoint = localAE.getAddress();
        ClientBootstrap clientBootstrap = (ClientBootstrap) this.bootstrap;
        ChannelFuture connectFuture = clientBootstrap.connect(endPoint, bindPoint);
        connectFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                if (future.isSuccess()) {
                    Channel channel = future.getChannel();
                    Association association = Association.request(channel, DicomClient.this,
                            localAE, userIdentity);
                    synchronized (associationLock) {
                        // associations.get() is called in
                        // channelConnected() (ChannelFutureListeners are
                        // invoked before channelConnected() is fired)
                        AssociationHandler.associations.set(channel, association);
                        associationLock.notifyAll();
                    }
                }
            }
        });
        // TODO Make connection timeout configurable
        connectFuture.await(connectTimeoutMillis);
        if (!connectFuture.isSuccess()) {
            throw new IOException("Failed to connect to " + endPoint + " from " + bindPoint,
                    connectFuture.getCause());
        }
        synchronized (associationLock) {
            while (true) {
                association = AssociationHandler.associations.get(connectFuture.getChannel());
                if (association == null) {
                    associationLock.wait();
                } else {
                    break;
                }
            }
        }

        association.negotiate(localAE.makeAssociateRQ(localTCs, remoteAE, userIdentity));
        localAE.addToPool(association);
        localAE.associationAccepted(association);
        return association;
    }
}
