/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.oht.miami.msdcache.dicom.pdu.PDUType;
import org.oht.miami.msdcache.dicom.pdu.PDVType;

public class PDataTFOutputStream extends OutputStream {

    private final Association association;

    private final int pcid;

    private final int pdvType;

    private ChannelBuffer pduBuffer;

    public PDataTFOutputStream(Association association, int pcid, int pdvType) {

        this.association = association;
        this.pcid = pcid;
        this.pdvType = pdvType;
        initPDUBuffer();
    }

    private void initPDUBuffer() {

        pduBuffer = ChannelBuffers.buffer(6 + association.getMaxPDULengthSend());
        pduBuffer.writerIndex(12);
    }

    @Override
    public void write(int b) throws IOException {

        flushPDataTF();
        pduBuffer.writeByte(b);
    }

    @Override
    public void write(byte b[], int off, int len) throws IOException {

        int pos = off;
        int remaining = len;
        while (remaining > 0) {
            flushPDataTF();
            int bytesToWrite = Math.min(remaining, pduBuffer.writableBytes());
            pduBuffer.writeBytes(b, pos, bytesToWrite);
            pos += bytesToWrite;
            remaining -= bytesToWrite;
        }
    }

    @Override
    public void close() {

        encodeHeader(PDVType.LAST);
        association.sendPDataTF(pduBuffer);
    }

    private void encodeHeader(int last) {

        int pduLength = pduBuffer.readableBytes() - 6;
        int pdvLength = pduLength - 4;
        // PDU Type (1B)
        pduBuffer.setByte(0, PDUType.P_DATA_TF);
        // Reserved (1B)
        pduBuffer.setByte(1, 0);
        // PDU Length (4B)
        pduBuffer.setInt(2, pduLength);
        // Item Length (4B)
        pduBuffer.setInt(6, pdvLength);
        // Presentation Context ID (1B)
        pduBuffer.setByte(10, pcid);
        // Message Control Header (1B)
        pduBuffer.setByte(11, pdvType | last);
    }

    private void flushPDataTF() {

        if (pduBuffer.writableBytes() > 0) {
            return;
        }
        encodeHeader(PDVType.PENDING);
        association.sendPDataTF(pduBuffer);
        initPDUBuffer();
    }

    public void copyFrom(InputStream in, int len) throws IOException {

        int remaining = len;
        while (remaining > 0) {
            flushPDataTF();
            int bytesToCopy = Math.min(remaining, pduBuffer.writableBytes());
            int bytesCopied = pduBuffer.writeBytes(in, bytesToCopy);
            if (bytesCopied == -1) {
                throw new EOFException();
            }
            remaining -= bytesCopied;
        }
    }

    public void copyFrom(InputStream in) throws IOException {

        int bytesCopied = -1;
        do {
            flushPDataTF();
            bytesCopied = pduBuffer.writeBytes(in, pduBuffer.writableBytes());
        } while (bytesCopied >= 0);
    }
}
