/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.util;

/**
 * An enum which represents the different types of messages used by the system.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 * 
 */
public enum MessageType {
	STUDY_ENTRY, VERSION, BULK_DATA;

	/**
	 * Provides the string representation of the enum.
	 * 
	 * @return the String representation of the enum
	 */
	public String getValue() {
		return name();
	}
}
