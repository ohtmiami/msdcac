/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.net.UserIdentity;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.oht.miami.msdcache.dicom.service.DicomService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetworkApplicationEntity {

    private static Logger LOG = LoggerFactory.getLogger(NetworkApplicationEntity.class);

    private boolean associationAcceptor = false;

    private boolean associationInitiator = false;

    private String aeTitle = null;

    private String hostname = null;

    private int port = 0;

    private InetAddress address = null;

    private String[] preferredCallingAETitles = {};

    private String[] preferredCalledAETitles = {};

    private int maxOpsInvoked;

    private int maxOpsPerformed;

    private int maxPDULengthReceive = 0x4000;

    private int maxPDULengthSend = 0x4000;

    private int dimseRSPTimeout = 10000;

    private TransferCapability[] transferCapabilities = {};

    private UserIdentity userIdentity = null;

    private DicomConnector connector;

    private AssociationListener[] associationListeners = {};

    private final DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();

    private final List<Association> pool = new ArrayList<Association>();

    private AtomicInteger messageID = new AtomicInteger();

    public NetworkApplicationEntity(String aeTitle, String hostname, int port) {

        if (aeTitle == null) {
            throw new NullPointerException("aeTitle");
        }
        this.aeTitle = aeTitle;
        this.hostname = hostname;
        this.port = port;
    }

    public DicomConnector getConnector() {

        return connector;
    }

    void setConnector(DicomConnector connector) {

        this.connector = connector;
    }

    public String getAETitle() {

        return aeTitle;
    }

    public void setAETitle(String aeTitle) {

        this.aeTitle = aeTitle;
    }

    public String getHostname() {

        return hostname;
    }

    public void setHostname(String hostname) {

        this.hostname = hostname;
    }

    public int getPort() {

        return port;
    }

    public void setPort(int port) {

        this.port = port;
    }

    public InetSocketAddress getAddress() throws UnknownHostException {

        return new InetSocketAddress(resolveAddress(), port);
    }

    private InetAddress resolveAddress() throws UnknownHostException {

        if (address == null && hostname != null) {
            address = InetAddress.getByName(hostname);
        }
        return address;
    }

    public boolean isAssociationAcceptor() {

        return associationAcceptor;
    }

    public void setAssociationAcceptor(boolean acceptor) {

        associationAcceptor = acceptor;
    }

    public boolean isAssociationInitiator() {

        return associationInitiator;
    }

    public void setAssociationInitiator(boolean initiator) {

        associationInitiator = initiator;
    }

    public String[] getPreferredCalledAETitles() {

        return preferredCalledAETitles;
    }

    public void setPreferredCalledAETitles(String[] aets) {

        preferredCalledAETitles = aets;
    }

    public boolean hasPreferredCalledAETitle() {

        return preferredCalledAETitles != null && preferredCalledAETitles.length > 0;
    }

    public boolean isPreferredCalledAETitle(String aet) {

        return contains(preferredCalledAETitles, aet);
    }

    public String[] getPreferredCallingAETitles() {

        return preferredCallingAETitles;
    }

    public void setPreferredCallingAETitles(String[] aets) {

        preferredCallingAETitles = aets;
    }

    public boolean hasPreferredCallingAETitle() {

        return preferredCallingAETitles != null && preferredCallingAETitles.length > 0;
    }

    public boolean isPreferredCallingAETitle(String aet) {

        return contains(preferredCallingAETitles, aet);
    }

    private static boolean contains(String[] a, String s) {

        for (String si : a) {
            if (si.equals(a)) {
                return true;
            }
        }
        return false;
    }

    public TransferCapability[] getTransferCapabilities() {

        return transferCapabilities;
    }

    public void setTransferCapabilities(TransferCapability[] transferCapabilities) {

        this.transferCapabilities = transferCapabilities;
    }

    public int getMaxPDULengthReceive() {

        return maxPDULengthReceive;
    }

    public void setMaxPDULengthReceive(int maxPDULengthReceive) {

        this.maxPDULengthReceive = maxPDULengthReceive;
    }

    public int getMaxPDULengthSend() {

        return maxPDULengthSend;
    }

    public void setMaxPDULengthSend(int maxPDULengthSend) {

        this.maxPDULengthSend = maxPDULengthSend;
    }

    public int getDimseRSPTimeout() {

        return dimseRSPTimeout;
    }

    public void setDimseRSPTimeout(int dimseRSPTimeout) {

        this.dimseRSPTimeout = dimseRSPTimeout;
    }

    public UserIdentity getUserIdentity() {

        return userIdentity;
    }

    public void setUserIdentity(UserIdentity userIdentity) {

        this.userIdentity = userIdentity;
    }

    Association findReusableAssociation(String remoteAET, UserIdentity userIdentity) {

        if (pool.isEmpty()) {
            return null;
        }
        synchronized (pool) {
            for (Association association : pool) {
                if (!remoteAET.equals(association.getRemoteAETitle())) {
                    continue;
                }
                if (userIdentity != association.getUserIdentity()) {
                    continue;
                }
                if (association.isReadyForDataTransfer() && association.hasAvailableOps()) {
                    return association;
                }
            }
        }
        return null;
    }

    protected AAssociateRQ makeAssociateRQ(TransferCapability[] localTCs,
            NetworkApplicationEntity remoteAE, UserIdentity userIdentity) {

        AAssociateRQ rq = new AAssociateRQ();
        rq.setCallingAET(aeTitle);
        rq.setCalledAET(remoteAE.getAETitle());
        rq.setMaxPDULength(maxPDULengthReceive);
        rq.setMaxOpsInvoked(minZeroAsMax(maxOpsInvoked, remoteAE.maxOpsPerformed));
        rq.setMaxOpsPerformed(minZeroAsMax(maxOpsPerformed, remoteAE.maxOpsInvoked));

        Map<String, Collection<String>> cuid2ts = new LinkedHashMap<String, Collection<String>>();
        Collection<String> scu = new HashSet<String>();
        Collection<String> scp = new HashSet<String>();
        evaluateTC(rq, cuid2ts, scu, scp, localTCs, remoteAE.transferCapabilities);
        if (cuid2ts.isEmpty()) {
            LOG.info("No common Transfer Capability between local AE {} and remote AE {}", aeTitle,
                    remoteAE.getAETitle());
            PresentationContext pc = new PresentationContext();
            pc.setPCID(rq.nextPCID());
            pc.setAbstractSyntax(UID.VerificationSOPClass);
            pc.addTransferSyntax(UID.ImplicitVRLittleEndian);
            rq.addPresentationContext(pc);
            return rq;
        }
        initPCs(rq, cuid2ts);
        if (!cuid2ts.isEmpty()) {
            LOG.info("Maximum number (128) of offered Presentation Context reached"
                    + " -  cannot offer all Transfer Capabilities in A-ASSOCIATE-RQ");
        }
        for (String cuid : scp) {
            rq.addRoleSelection(new RoleSelection(cuid, scu.contains(cuid), true));
        }
        if (userIdentity != null) {
            rq.setUserIdentity(userIdentity.getUserIdentityRQ());
        }
        return rq;
    }

    private void evaluateTC(AAssociateRQ rq, Map<String, Collection<String>> cuid2ts,
            Collection<String> scu, Collection<String> scp, TransferCapability[] localTCs,
            TransferCapability[] remoteTCs) {

        for (TransferCapability tc : localTCs) {
            String cuid = tc.getSopClass();
            LinkedHashSet<String> ts1 = new LinkedHashSet<String>(Arrays.asList(tc
                    .getTransferSyntax()));
            if (remoteTCs.length != 0) {
                TransferCapability remoteTC = findTC(remoteTCs, cuid, tc.isSCU());
                if (remoteTC == null) {
                    continue;
                }
                ts1.retainAll(Arrays.asList(remoteTC.getTransferSyntax()));
                if (ts1.isEmpty()) {
                    continue;
                }
            }
            Collection<String> ts = cuid2ts.get(cuid);
            if (ts == null) {
                cuid2ts.put(cuid, ts1);
            } else {
                ts.addAll(ts1);
            }
            (tc.isSCP() ? scp : scu).add(cuid);
            byte[] extInfo = tc.getExtInfo();
            if (extInfo.length != 0) {
                ExtendedNegotiation extNeg = new ExtendedNegotiation(cuid, extInfo);
                rq.addExtendedNegotiation(extNeg);
            }
        }
    }

    private void initPCs(AAssociateRQ rq, Map<String, Collection<String>> cuid2ts) {

        String[] tsdefs = { UID.ImplicitVRLittleEndian, UID.JPEGBaseline1, UID.JPEGExtended24,
                        UID.JPEGLossless, UID.JPEGLSLossless };
        while (!cuid2ts.isEmpty() && rq.getNumberOfPresentationContexts() < 128) {
            for (Iterator<Map.Entry<String, Collection<String>>> iter = cuid2ts.entrySet()
                    .iterator(); iter.hasNext() && rq.getNumberOfPresentationContexts() < 128;) {
                Map.Entry<String, Collection<String>> e = iter.next();
                String cuid = e.getKey();
                Collection<String> ts = e.getValue();
                PresentationContext pc = new PresentationContext();
                pc.setPCID(rq.nextPCID());
                pc.setAbstractSyntax(cuid);
                pc.addTransferSyntax(selectTS(ts, tsdefs));
                rq.addPresentationContext(pc);
                if (ts.isEmpty()) {
                    iter.remove();
                }
            }
        }
    }

    private String selectTS(Collection<String> ts, String[] tsdefs) {
        for (String tsdef : tsdefs) {
            if (ts.remove(tsdef)) {
                return tsdef;
            }
        }
        Iterator<String> tsiter = ts.iterator();
        String uid = tsiter.next();
        tsiter.remove();
        return uid;
    }

    private TransferCapability findTC(TransferCapability[] tcs, String cuid, boolean scp) {

        for (TransferCapability tc : tcs) {
            if (tc.isSCP() == scp && tc.getSopClass().equals(cuid)) {
                return tc;
            }
        }
        return null;
    }

    public void register(DicomService service) {

        serviceRegistry.register(service);
    }

    public void deregister(DicomService service) {

        serviceRegistry.deregister(service);
    }

    void addToPool(Association association) {

        synchronized (pool) {
            pool.add(association);
        }
    }

    void removeFromPool(Association association) {

        synchronized (pool) {
            pool.remove(association);
        }
    }

    void perform(Association association, int pcid, DicomObject command, InputStream dataStream,
            String tsuid) throws IOException {

        serviceRegistry.process(association, pcid, command, dataStream, tsuid);
    }

    AAssociateAC negotiate(Association association, AAssociateRQ rq) throws AAssociateRJ {

        if (!isAssociationAcceptor()) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER, AAssociateRJ.REASON_NO_REASON_GIVEN);
        }
        String[] calling = getPreferredCallingAETitles();
        if (calling.length != 0 && !isPreferredCallingAETitle(rq.getCallingAET())) {
            throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_USER,
                    AAssociateRJ.REASON_CALLING_AET_NOT_RECOGNIZED);
        }
        // TODO installed
        AAssociateAC ac = new AAssociateAC();
        ac.setCalledAET(rq.getCalledAET());
        ac.setCallingAET(rq.getCallingAET());
        ac.setMaxPDULength(maxPDULengthReceive);
        ac.setMaxOpsInvoked(minZeroAsMax(rq.getMaxOpsInvoked(), maxOpsPerformed));
        ac.setMaxOpsPerformed(minZeroAsMax(rq.getMaxOpsPerformed(), maxOpsInvoked));
        Collection<PresentationContext> pcs = rq.getPresentationContexts();
        for (PresentationContext rqpc : pcs) {
            PresentationContext acpc = negPresCtx(rq, ac, rqpc);
            ac.addPresentationContext(acpc);
        }
        return ac;
    }

    private PresentationContext negPresCtx(AAssociateRQ rq, AAssociateAC ac,
            PresentationContext rqpc) {

        String asuid = rqpc.getAbstractSyntax();
        RoleSelection rqrs = rq.getRoleSelectionFor(asuid);
        TransferCapability tcscp = findTC(transferCapabilities, asuid, true);
        TransferCapability tcscu = findTC(transferCapabilities, asuid, false);
        RoleSelection acrs = ac.getRoleSelectionFor(asuid);
        if (rqrs != null && acrs == null) {
            boolean scp = rqrs.isSCP() && tcscu != null;
            boolean scu = rqrs.isSCU() && tcscp != null;
            acrs = new RoleSelection(asuid, scu, scp);
            ac.addRoleSelection(acrs);
        }
        TransferCapability tc = rqrs == null || acrs.isSCU() ? tcscp : tcscu;

        PresentationContext acpc = new PresentationContext();
        acpc.setPCID(rqpc.getPCID());
        if (tc != null) {
            Set<String> rqts = rqpc.getTransferSyntaxes();
            String[] acts = tc.getTransferSyntax();
            for (int i = 0; i < acts.length; i++) {
                if (rqts.contains(acts[i])) {
                    acpc.addTransferSyntax(acts[i]);
                    if (ac.getExtendedNegotiationFor(asuid) == null) {
                        ExtendedNegotiation extNeg = tc.negotiate(rq
                                .getExtendedNegotiationFor(asuid));
                        if (extNeg != null)
                            ac.addExtendedNegotiation(extNeg);
                    }
                    return acpc;
                }
            }
            acpc.setResult(PresentationContext.TRANSFER_SYNTAX_NOT_SUPPORTED);
        } else {
            acpc.setResult(PresentationContext.ABSTRACT_SYNTAX_NOT_SUPPORTED);
        }
        acpc.addTransferSyntax(rqpc.getTransferSyntax());
        return acpc;
    }

    private int minZeroAsMax(int i1, int i2) {
        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }

    public void addAssociationListener(AssociationListener l) {

        if (l == null) {
            throw new NullPointerException();
        }
        synchronized (associationListeners) {
            AssociationListener[] tmp = new AssociationListener[associationListeners.length + 1];
            System.arraycopy(associationListeners, 0, tmp, 0, associationListeners.length);
            tmp[associationListeners.length] = l;
            associationListeners = tmp;
        }
    }

    public void removeAssociationListener(AssociationListener l) {

        if (l == null) {
            throw new NullPointerException();
        }
        synchronized (associationListeners) {
            for (int i = 0; i < associationListeners.length; i++) {
                if (associationListeners[i].equals(l)) {
                    AssociationListener[] tmp = new AssociationListener[associationListeners.length - 1];
                    System.arraycopy(associationListeners, 0, tmp, 0, i);
                    System.arraycopy(associationListeners, i + 1, tmp, i, tmp.length - i);
                    associationListeners = tmp;
                    return;
                }
            }
        }
    }

    void associationAccepted(Association association) {

        synchronized (associationListeners) {
            for (AssociationListener l : associationListeners) {
                l.associationAccepted(new AssociationAcceptEvent(this, association));
            }
        }
    }

    void associationClosed(Association association) {

        synchronized (associationListeners) {
            for (AssociationListener l : associationListeners) {
                l.associationClosed(new AssociationCloseEvent(this, association));
            }
        }
    }

    int nextMessageID() {

        return messageID.incrementAndGet() & 0xFFFF;
    }
}
