/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue.monitor.msdsender;

import java.util.concurrent.ExecutorService;

import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.queue.StudySendQueue;
import org.oht.miami.msdcache.transport.queue.monitor.QueueMonitor;
import org.oht.miami.msdcache.transport.queue.sender.StudySender;
import org.oht.miami.msdcache.transport.study.StudySendTask;
import org.oht.miami.msdtk.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class defines the job performed by the StudySendQueue monitor thread.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class MonitorStudySendQueue extends QueueMonitor {

    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(MonitorStudySendQueue.class);

    /**
     * Constructor
     * 
     * @param context
     *            The parent component (MSDSender or MSDReceiver).
     * @param queue
     *            The queue which this QueueMonitor will monitor.
     * @param store
     *            The Store used by the application.
     * @param executorService
     *            The thread pool used by the application.
     */
    public MonitorStudySendQueue(MSDTransport context, StudySendQueue queue, Store store,
            ExecutorService executorService) {
        super(context, queue, store, executorService);
    }

    @Override
    public Void call() {
        while (isRunning()) {
            StudySendQueue studySendQueue = (StudySendQueue) queue;
            final StudySendTask task = studySendQueue.receiveTask(messageReceiveTimeout);

            // If a StudySendTask was found in the queue, process it
            if (task != null) {
                StudySender.sendStudy(task, context, store);
            }
        }

        LOG.info("This QueueMonitor is no longer running.");
        return null;
    }
}
