/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.oht.miami.msdcache.forward.DicomSender;
import org.oht.miami.msdcache.ingest.DicomReceiver;
import org.oht.miami.msdcache.transport.MSDReceiver;
import org.oht.miami.msdcache.transport.MSDSender;
import org.oht.miami.msdcache.util.PropertiesFileUtils;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StudyRouterApp {

    private static Logger LOG = LoggerFactory.getLogger(StudyRouterApp.class);

    public static void main(String[] args) {

        Store store = StoreFactory.createStore("msd-store.properties");
        final ExecutorService executorService = Executors.newCachedThreadPool();

        final MSDSender studySender = new MSDSender(store, executorService);
        final DicomReceiver dicomReceiver = new DicomReceiver(executorService, store, 30,
                TimeUnit.SECONDS);
        final MSDReceiver studyReceiver = new MSDReceiver(store, executorService);
        final DicomSender dicomSender = new DicomSender(executorService, store);

        // Hook the MSDSender into the DicomReceiver
        dicomReceiver.setStudySender(studySender);

        // Hook the DicomSender into the MSDReceiver
        studyReceiver.setDicomSender(dicomSender);

        PropertiesFileUtils.addLocalApplicationEntitiesFromFile(dicomSender,
                "config/dicomsender/local-ae.properties");
        PropertiesFileUtils.addRemoteApplicationEntitiesFromFile(dicomSender,
                "config/dicomsender/remote-ae.properties");
        PropertiesFileUtils.addLocalApplicationEntitiesFromFile(dicomReceiver,
                "config/dicomreceiver/local-ae.properties");
        PropertiesFileUtils.addRemoteNodesFromFile(studySender,
                "config/msdsender/remote-node.properties");

        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {

                LOG.info("Shutting down study router...");
                dicomReceiver.stop();
                studyReceiver.stop();
                dicomSender.stop();
                studySender.stop();

                executorService.shutdown();
                try {
                    executorService.awaitTermination(1, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    LOG.warn("Worker thread pool did not terminate", e);
                }
                LOG.info("Study router terminated");
            }
        });

        LOG.info("Starting study router...");
        studySender.start();
        dicomSender.start();
        studyReceiver.start();
        dicomReceiver.start();
        LOG.info("Study router ready");
    }
}
