/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientAssociationHandler extends AssociationHandler {

    private static Logger LOG = LoggerFactory.getLogger(ClientAssociationHandler.class);

    public ClientAssociationHandler(DicomClient client) {

        super(client);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        Channel channel = e.getChannel();
        LOG.info("Connected to {}", channel.getRemoteAddress());
        this.association = AssociationHandler.associations.get(channel);
        super.channelConnected(ctx, e);
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {

        Channel channel = e.getChannel();
        LOG.info("Disconnected from {}", channel.getRemoteAddress());
        super.channelDisconnected(ctx, e);
    }
}
