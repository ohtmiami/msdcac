/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import org.dcm4che2.data.DicomObject;

public class DimseRSPHandler {

    private int messageID;

    private int pcid;

    private long timeout = Long.MAX_VALUE;

    final void setPCID(int pcid) {

        this.pcid = pcid;
    }

    public final int getMessageID() {

        return messageID;
    }

    final void setMessageID(int messageID) {

        this.messageID = messageID;
    }

    final long getTimeout() {

        return timeout;
    }

    final void setTimeout(long timeout) {

        this.timeout = timeout;
    }

    public void cancel(Association association) {

        association.cancel(pcid, messageID);
    }

    public void onDimseRSP(Association association, DicomObject command, DicomObject dataSet) {
    }

    public void onClosed(Association association) {
    }
}
