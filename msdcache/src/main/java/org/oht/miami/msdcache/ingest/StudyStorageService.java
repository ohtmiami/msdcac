/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.ingest;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.net.DicomServiceException;
import org.dcm4che2.net.Status;
import org.dcm4che2.util.CloseUtils;
import org.oht.miami.msdcache.dicom.Association;
import org.oht.miami.msdcache.dicom.service.StorageService;
import org.oht.miami.msdcache.transport.MSDSender;
import org.oht.miami.msdcache.transport.study.StudySendTask;
import org.oht.miami.msdcache.util.AETitlePair;
import org.oht.miami.msdtk.io.ActiveStudyTable;
import org.oht.miami.msdtk.io.StudyParser;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StudyStorageService extends StorageService implements
        ActiveStudyTable.StudyTimeoutHandler {

    private static Logger LOG = LoggerFactory.getLogger(StudyStorageService.class);

    public static final String[] CUIDS = { UID.BasicStudyContentNotificationSOPClassRetired,
                    UID.StoredPrintStorageSOPClassRetired,
                    UID.HardcopyGrayscaleImageStorageSOPClassRetired,
                    UID.HardcopyColorImageStorageSOPClassRetired,
                    UID.ComputedRadiographyImageStorage,
                    UID.DigitalXRayImageStorageForPresentation,
                    UID.DigitalXRayImageStorageForProcessing,
                    UID.DigitalMammographyXRayImageStorageForPresentation,
                    UID.DigitalMammographyXRayImageStorageForProcessing,
                    UID.DigitalIntraOralXRayImageStorageForPresentation,
                    UID.DigitalIntraOralXRayImageStorageForProcessing,
                    UID.StandaloneModalityLUTStorageRetired, UID.EncapsulatedPDFStorage,
                    UID.StandaloneVOILUTStorageRetired,
                    UID.GrayscaleSoftcopyPresentationStateStorageSOPClass,
                    UID.ColorSoftcopyPresentationStateStorageSOPClass,
                    UID.PseudoColorSoftcopyPresentationStateStorageSOPClass,
                    UID.BlendingSoftcopyPresentationStateStorageSOPClass,
                    UID.XRayAngiographicImageStorage, UID.EnhancedXAImageStorage,
                    UID.XRayRadiofluoroscopicImageStorage, UID.EnhancedXRFImageStorage,
                    UID.XRayAngiographicBiPlaneImageStorageRetired,
                    UID.PositronEmissionTomographyImageStorage,
                    UID.StandalonePETCurveStorageRetired, UID.CTImageStorage,
                    UID.EnhancedCTImageStorage, UID.NuclearMedicineImageStorage,
                    UID.UltrasoundMultiFrameImageStorageRetired,
                    UID.UltrasoundMultiFrameImageStorage, UID.MRImageStorage,
                    UID.EnhancedMRImageStorage, UID.MRSpectroscopyStorage, UID.RTImageStorage,
                    UID.RTDoseStorage, UID.RTStructureSetStorage,
                    UID.RTBeamsTreatmentRecordStorage, UID.RTPlanStorage,
                    UID.RTBrachyTreatmentRecordStorage, UID.RTTreatmentSummaryRecordStorage,
                    UID.NuclearMedicineImageStorageRetired, UID.UltrasoundImageStorageRetired,
                    UID.UltrasoundImageStorage, UID.RawDataStorage, UID.SpatialRegistrationStorage,
                    UID.SpatialFiducialsStorage, UID.RealWorldValueMappingStorage,
                    UID.SecondaryCaptureImageStorage,
                    UID.MultiFrameSingleBitSecondaryCaptureImageStorage,
                    UID.MultiFrameGrayscaleByteSecondaryCaptureImageStorage,
                    UID.MultiFrameGrayscaleWordSecondaryCaptureImageStorage,
                    UID.MultiFrameTrueColorSecondaryCaptureImageStorage,
                    UID.VLImageStorageTrialRetired, UID.VLEndoscopicImageStorage,
                    UID.VideoEndoscopicImageStorage, UID.VLMicroscopicImageStorage,
                    UID.VideoMicroscopicImageStorage,
                    UID.VLSlideCoordinatesMicroscopicImageStorage, UID.VLPhotographicImageStorage,
                    UID.VideoPhotographicImageStorage, UID.OphthalmicPhotography8BitImageStorage,
                    UID.OphthalmicPhotography16BitImageStorage,
                    UID.StereometricRelationshipStorage, UID.VLMultiFrameImageStorageTrialRetired,
                    UID.StandaloneOverlayStorageRetired, UID.BasicTextSRStorage,
                    UID.EnhancedSRStorage, UID.ComprehensiveSRStorage, UID.ProcedureLogStorage,
                    UID.MammographyCADSRStorage, UID.KeyObjectSelectionDocumentStorage,
                    UID.ChestCADSRStorage, UID.XRayRadiationDoseSRStorage,
                    UID.EncapsulatedPDFStorage, UID.EncapsulatedCDAStorage,
                    UID.StandaloneCurveStorageRetired, UID.TwelveLeadECGWaveformStorage,
                    UID.GeneralECGWaveformStorage, UID.AmbulatoryECGWaveformStorage,
                    UID.HemodynamicWaveformStorage, UID.CardiacElectrophysiologyWaveformStorage,
                    UID.BasicVoiceAudioWaveformStorage, UID.HangingProtocolStorage,
                    UID.SiemensCSANonImageStorage,
                    UID.Dcm4cheAttributesModificationNotificationSOPClass,
                    UID.MultiSeriesStudyStorage };

    private final ActiveStudyTable activeStudyTable;

    private final ConcurrentMap<DicomUID, AETitlePair> studyAETitlePairs;

    private MSDSender studySender;

    public StudyStorageService(Store store, long studyTimeoutDuration, TimeUnit studyTimeoutUnit,
            Executor executor) {

        super(CUIDS);
        activeStudyTable = new ActiveStudyTable(store, studyTimeoutDuration, studyTimeoutUnit,
                this, executor);
        studyAETitlePairs = new ConcurrentHashMap<DicomUID, AETitlePair>();
        studySender = null;
    }

    ActiveStudyTable getActiveStudyTable() {

        return activeStudyTable;
    }

    void setStudySender(MSDSender studySender) {

        this.studySender = studySender;
    }

    @Override
    public void onStudyTimeout(Study study) throws Exception {

        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        LOG.info("Saving study {} to store...", studyInstanceUID);
        activeStudyTable.getStore().writeVersion(study);
        AETitlePair studyAETPair = studyAETitlePairs.remove(studyInstanceUID);
        if (studySender != null) {
            StudySendTask studySendTask = new StudySendTask(studyInstanceUID, studyAETPair);
            studySender.sendStudy(studySendTask);
        }
    }

    @Override
    protected void onCStoreRQ(Association association, int pcid, DicomObject rq,
            InputStream dataStream, String tsuid, DicomObject rsp) throws DicomServiceException,
            IOException {

        DicomObject attributeSet = new BasicDicomObject();
        String cuid = rq.getString(Tag.AffectedSOPClassUID);
        String iuid = rq.getString(Tag.AffectedSOPInstanceUID);
        attributeSet.initFileMetaInformation(cuid, iuid, tsuid);

        StudyParser parser = new StudyParser(activeStudyTable);
        DicomInputStream dicomIn = new DicomInputStream(dataStream, TransferSyntax.valueOf(tsuid));
        try {
            parser.parse(dicomIn, attributeSet);
        } catch (IOException e) {
            // TODO Delete bulk data?
            throw new DicomServiceException(rq, Status.ProcessingFailure, e.getMessage());
        } finally {
            CloseUtils.safeClose(dicomIn);
        }

        DicomUID studyInstanceUID = new DicomUID(attributeSet.getString(Tag.StudyInstanceUID));
        String sourceAET = association.getCallingAETitle();
        String destinationAET = association.getCalledAETitle();
        AETitlePair aetPair = new AETitlePair(sourceAET, destinationAET);
        AETitlePair existingAETPair = studyAETitlePairs.putIfAbsent(studyInstanceUID, aetPair);
        if (existingAETPair != null) {
            String existingSourceAET = existingAETPair.getSource();
            String existingDestinationAET = existingAETPair.getDestination();
            if (!sourceAET.equals(existingSourceAET)) {
                // TODO How to handle?
                LOG.warn("Unexpected source AE Title {} for incoming SOP instance {}"
                        + " of study {}; expected {}", new Object[] { sourceAET, iuid,
                                studyInstanceUID, existingSourceAET });
            }
            if (!destinationAET.equals(existingDestinationAET)) {
                // TODO How to handle?
                LOG.warn("Unexpected destination AE Title {} for incoming SOP instance {}"
                        + " of study {}; expected {}", new Object[] { destinationAET, iuid,
                                studyInstanceUID, existingDestinationAET });
            }
        }
    }
}
