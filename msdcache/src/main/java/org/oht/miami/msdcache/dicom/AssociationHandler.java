/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelLocal;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRQ;
import org.oht.miami.msdcache.dicom.pdu.PDV;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AssociationHandler extends SimpleChannelUpstreamHandler {

    private static Logger LOG = LoggerFactory.getLogger(AssociationHandler.class);

    static final ChannelLocal<Association> associations = new ChannelLocal<Association>(true);

    protected final DicomConnector connector;

    protected Association association;

    protected AssociationHandler(DicomConnector connector) {

        this.connector = connector;
        association = null;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {

        Object message = e.getMessage();
        if (message instanceof PDUError) {
            PDUError pduError = (PDUError) message;
            association.abort(pduError.getAAbort());
            return;
        }
        try {
            if (message instanceof AAssociateRJ) {
                association.receivedAssociateRJ((AAssociateRJ) message);
            } else if (message instanceof AReleaseRQ) {
                association.receivedReleaseRQ();
            } else if (message instanceof AReleaseRP) {
                association.receivedReleaseRP();
            } else if (message instanceof AAbort) {
                association.receivedAbort((AAbort) message);
            } else if (message instanceof AAssociateRQ) {
                association.receivedAssociateRQ((AAssociateRQ) message);
            } else if (message instanceof AAssociateAC) {
                association.receivedAssociateAC((AAssociateAC) message);
            } else if (message instanceof PDV) {
                association.receivedPDV((PDV) message);
            }
        } catch (AAbort aa) {
            association.abort(aa);
        } catch (IOException ex) {
            association.setException(ex);
            LOG.warn("i/o exception in state " + association.getState(), ex);
        } finally {
            AssociationState state = association.getState();
            if (association.getException() != null || state == AssociationState.STA1
                    || state == AssociationState.STA13) {
                association.closeChannel();
            }
        }
    }

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {

        Channel channel = e.getChannel();
        connector.getAllChannels().add(channel);
        super.channelOpen(ctx, e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {

        LOG.warn("unexpected exception in state " + association.getState(), e.getCause());
        association.closeChannel();
        super.exceptionCaught(ctx, e);
    }
}
