/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.jboss.netty.bootstrap.Bootstrap;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.ChannelGroupFuture;
import org.jboss.netty.channel.group.DefaultChannelGroup;

public abstract class DicomConnector {

    private final ExecutorService executorService;

    private final ChannelGroup allChannels = new DefaultChannelGroup();

    protected Bootstrap bootstrap = null;

    private Map<String, NetworkApplicationEntity> localAEs = new HashMap<String, NetworkApplicationEntity>();

    private Map<String, NetworkApplicationEntity> remoteAEs = new HashMap<String, NetworkApplicationEntity>();

    protected DicomConnector(ExecutorService executorService) {

        this.executorService = executorService;
    }

    public ExecutorService getExecutorService() {

        return executorService;
    }

    protected ChannelGroup getAllChannels() {

        return allChannels;
    }

    public Collection<NetworkApplicationEntity> getLocalApplicationEntities() {

        return localAEs.values();
    }

    protected NetworkApplicationEntity addLocalApplicationEntity(NetworkApplicationEntity localAE) {

        localAE.setConnector(this);
        return localAEs.put(localAE.getAETitle(), localAE);
    }

    protected NetworkApplicationEntity removeLocalApplicationEntity(String aet) {

        NetworkApplicationEntity localAE = localAEs.remove(aet);
        if (localAE != null) {
            localAE.setConnector(null);
        }
        return localAE;
    }

    protected NetworkApplicationEntity getLocalApplicationEntity(String aet) {

        return localAEs.get(aet);
    }

    public Collection<NetworkApplicationEntity> getRemoteApplicationEntities() {

        return remoteAEs.values();
    }

    protected NetworkApplicationEntity addRemoteApplicationEntity(NetworkApplicationEntity remoteAE) {

        return remoteAEs.put(remoteAE.getAETitle(), remoteAE);
    }

    protected NetworkApplicationEntity removeRemoteApplicationEntity(String aet) {

        return remoteAEs.remove(aet);
    }

    protected NetworkApplicationEntity getRemoteApplicationEntity(String aet) {

        return remoteAEs.get(aet);
    }

    public abstract void start();

    public void stop() {

        if (bootstrap == null) {
            throw new IllegalStateException("Not started");
        }
        ChannelGroupFuture closeFuture = allChannels.close();
        closeFuture.awaitUninterruptibly();
        bootstrap.releaseExternalResources();
        bootstrap = null;
    }
}
