/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.study;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.hornetq.api.core.client.ClientMessage;
import org.oht.miami.msdcache.transport.MSDReceiver;
import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.queue.StudySendQueue;
import org.oht.miami.msdcache.transport.util.FileStorageStatus;
import org.oht.miami.msdcache.transport.util.FileStorageStatusType;
import org.oht.miami.msdcache.transport.util.MessagePropertyType;
import org.oht.miami.msdcache.transport.util.MessageType;
import org.oht.miami.msdcache.util.AETitlePair;
import org.oht.miami.msdtk.store.BulkDataInfo;
import org.oht.miami.msdtk.store.BulkDataSet;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.TransportableStudyEntry;
import org.oht.miami.msdtk.store.VersionInfo;
import org.oht.miami.msdtk.util.DicomUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class which is responsible for determining when all of the associated
 * messages and files related to a particular Study have been received,
 * regardless of the file transportation implementation used (HornetQ, Aspera,
 * etc). It is also responsible for sending an acknowledgment back to the
 * source, and for adding a StudySendTask to the outgoing work queue.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class StudyReceiveManager {

    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(StudyReceiveManager.class);

    /**
     * A static ConcurrentHashMap which maps a group ID to a
     * StudyReceiveManager. This map is used to ensure that only one
     * StudyReceiveManager is created per message group.
     */
    private static final Map<String, StudyReceiveManager> studiesInProcess = new ConcurrentHashMap<String, StudyReceiveManager>();

    /**
     * The number of times to try sending a message before sending a NACK to the
     * source cache.
     */
    private static final int MAX_RETRIES = 0;

    /**
     * Reference to the Store, where StudyEntry objects and files will be saved.
     */
    private Store store;

    /**
     * The unique group ID used to group a set of messages.
     */
    private String groupID;

    /**
     * The StudyInstanceUID of the study.
     */
    private DicomUID studyInstanceUID;

    /**
     * The AE Title pair.
     */
    private AETitlePair aeTitlePair;

    /**
     * The TransportableStudyEntry that was sent with this group of messages.
     */
    private TransportableStudyEntry transportableStudyEntry;

    /**
     * A map which keeps track of the different Version UUIDs received from
     * HornetQ messages, and the storage status for that file.
     */
    private Map<UUID, FileStorageStatus> versionUUIDs = new ConcurrentHashMap<UUID, FileStorageStatus>();

    /**
     * A map which keeps track of the different BulkData UUIDs received from
     * HornetQ messages, and the storage status for that file.
     */
    private Map<UUID, FileStorageStatus> bulkDataUUIDs = new ConcurrentHashMap<UUID, FileStorageStatus>();

    /**
     * A reference to the StudySendQueue. This is required so that the
     * StudyReceiveManager can add a StudySendTask to the StudySendQueue once a
     * study has been fully received and stored.
     */
    private StudySendQueue studySendQueue;

    /**
     * Constructor
     * 
     * @param context
     *            The MSDReceiver context, used to lookup the StudySendQueue.
     * @param groupID
     *            The unique ID assigned to each message in a group.
     * @param store
     *            The store to which newly received files will be saved.
     */
    private StudyReceiveManager(MSDTransport context, String groupID, Store store) {
        this.groupID = groupID;
        this.store = store;
        this.studySendQueue = ((MSDReceiver) context).getStudySendQueue();
    }

    /**
     * Get an instance of StudyReceiveManager from the map.
     * 
     * @param context
     *            The MSDReceiver context.
     * @param groupID
     *            the group ID that is retrieved from the message header
     * @param store
     *            The store to which newly received files will be saved.
     * @return the StudyReceiveManager associated with the provided group ID, or
     *         a new StudyReceiveManager
     */
    public synchronized static StudyReceiveManager getInstance(MSDTransport context,
            String groupID, Store store) {
        if (groupID == null || groupID.trim().isEmpty()) {
            throw new IllegalArgumentException("A valid group ID cannot be null or empty.");
        } else if (store == null) {
            throw new IllegalArgumentException(
                    "A valid StudyReceiveManager cannot have a null reference to the Store.");
        }

        // If a StudyReceiveManager has already been created for this message
        // group, retrieve it from the map. Otherwise, create a new
        // StudyReceiveManager, add it to the map, and return it.
        if (studiesInProcess.containsKey(groupID)) {
            return studiesInProcess.get(groupID);
        } else {
            StudyReceiveManager manager = new StudyReceiveManager(context, groupID, store);
            studiesInProcess.put(groupID, manager);

            manager.groupID = groupID;
            return manager;
        }
    }

    /**
     * Receives a message, examines the message type, and forwards the message
     * to the appropriate method for further processing.
     * 
     * @param message
     *            the message that has been pulled from the JMS queue
     */
    public void processMessage(ClientMessage message) {
        MessageType messageType = null;

        try {
            // Populate the studyInstanceUID field (first message only)
            if (studyInstanceUID == null) {
                String studyInstanceUIDString = message
                        .getStringProperty(MessagePropertyType.STUDY_INSTANCE_UID.getValue());

                studyInstanceUID = new DicomUID(studyInstanceUIDString);
            }

            // Determine the message type
            String messageTypeString = message.getStringProperty(MessagePropertyType.MESSAGE_TYPE
                    .getValue());

            messageType = MessageType.valueOf(messageTypeString);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Process the message, based on the message type.
        switch (messageType) {
        case STUDY_ENTRY:
            LOG.info("Processing StudyEntry...");
            processStudyEntryMessage(message);
            break;

        case VERSION:
            LOG.info("Processing Version...");
            processVersionMessage(message);
            break;

        case BULK_DATA:
            LOG.info("Processing BulkData...");
            processBulkDataMessage(message);
            break;

        default:
            // TODO handle exception cases, call GWL
            break;
        }
    }

    /**
     * Attempts to store a version file to the Store and updates the
     * corresponding FileStorageStatus.
     * 
     * @param message
     *            the message that has been pulled from the JMS queue
     */
    private void processVersionMessage(ClientMessage message) {
        UUID uuid = null;
        long expectedFileSize = 0;
        long actualFileSize = 0;
        FileStorageStatus storageStatus;
        int retryCount = 0;

        try {
            // Get the UUID from the message header
            String uuidString = message.getStringProperty(MessagePropertyType.UUID.getValue());
            uuid = UUID.fromString(uuidString);

            // Get the expected file size
            expectedFileSize = message.getLongProperty(MessagePropertyType.FILE_SIZE.getValue());
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        storageStatus = versionUUIDs.get(uuid);

        if (storageStatus != null) {
            retryCount = storageStatus.getRetryCount();
        } else {
            FileStorageStatus fileStorageStatus = new FileStorageStatus(
                    FileStorageStatusType.PENDING);
            versionUUIDs.put(uuid, fileStorageStatus);
        }

        LOG.info("Version file has the UUID: " + uuid);

        // Write the file to the store
        try (OutputStream os = store.getVersionOutputStream(uuid, studyInstanceUID);) {
            message.saveToOutputStream(os);
            LOG.info("Done storing Version");
            actualFileSize = store.getVersionFileSize(uuid, studyInstanceUID);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Integrity check
        if (actualFileSize == expectedFileSize) {
            storageStatus.setCurrentStatus(FileStorageStatusType.SUCCESS);
            commitStudyIfFullyReceived();
        } else {
            LOG.error("File size doesn't match for file with StudyInstanceUID: " + studyInstanceUID
                    + ", and UUID: " + uuid);
            LOG.error("Expected size: " + expectedFileSize, ", Actual size: " + actualFileSize);

            if (retryCount < MAX_RETRIES) {
                storageStatus.setCurrentStatus(FileStorageStatusType.FAILURE_AWAITING_RETRY);
                // TODO send NACK message to trigger retry for this file
                storageStatus.incrementRetryCount();
            } else {
                // TODO send NACK message saying that this entire study has
                // failed? Something else?
                storageStatus.setCurrentStatus(FileStorageStatusType.FAILURE);
            }
        }
    }

    /**
     * Attempts to store a bulk data file to the Store and updates the
     * corresponding FileStorageStatus.
     * 
     * @param message
     *            the message that has been pulled from the JMS queue
     */
    private void processBulkDataMessage(ClientMessage message) {
        UUID uuid = null;
        long expectedFileSize = 0;
        long actualFileSize = 0;
        FileStorageStatus storageStatus;
        int retryCount = 0;

        try {
            // Get the UUID from the message header
            String uuidString = message.getStringProperty(MessagePropertyType.UUID.getValue());
            uuid = UUID.fromString(uuidString);

            // Get the expected file size
            expectedFileSize = message.getLongProperty(MessagePropertyType.FILE_SIZE.getValue());
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        storageStatus = bulkDataUUIDs.get(uuid);

        if (storageStatus != null) {
            retryCount = storageStatus.getRetryCount();
        } else {
            FileStorageStatus fileStorageStatus = new FileStorageStatus(
                    FileStorageStatusType.PENDING);
            bulkDataUUIDs.put(uuid, fileStorageStatus);
        }

        LOG.info("BulkData file has the UUID: " + uuid);

        // Write the file to the store
        try (OutputStream os = store.getBulkDataOutputStream(uuid, studyInstanceUID);) {
            message.saveToOutputStream(os);
            LOG.info("Done storing BulkData");
            actualFileSize = store.getBulkDataFileSize(uuid, studyInstanceUID);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Integrity check
        if (actualFileSize == expectedFileSize) {
            storageStatus.setCurrentStatus(FileStorageStatusType.SUCCESS);
            commitStudyIfFullyReceived();
        } else {
            LOG.error("File size doesn't match for file with StudyInstanceUID: " + studyInstanceUID
                    + ", and UUID: " + uuid);
            LOG.error("Expected size: " + expectedFileSize, ", Actual size: " + actualFileSize);

            if (retryCount < MAX_RETRIES) {
                storageStatus.setCurrentStatus(FileStorageStatusType.FAILURE_AWAITING_RETRY);
                // TODO send NACK message to trigger retry for this file
                storageStatus.incrementRetryCount();
            } else {
                // TODO send NACK message saying that this entire study has
                // failed? Something else?
                storageStatus.setCurrentStatus(FileStorageStatusType.FAILURE);
            }
        }
    }

    /**
     * Attempts to insert a StudyEntry into the Store.
     * 
     * @param message
     *            the message that has been pulled from the JMS queue
     */
    private void processStudyEntryMessage(ClientMessage message) {
        // TODO Ryan to validate that the new study entry is consistent with the
        // previous study entry in insertStudyEntry

        try {
            String sourceAET = message.getStringProperty(MessagePropertyType.SOURCE_AETITLE
                    .getValue());
            String destinationAET = message
                    .getStringProperty(MessagePropertyType.DESTINATION_AETITLE.getValue());

            aeTitlePair = new AETitlePair(sourceAET, destinationAET);

            // Deserialize the TransportableStudyEntry
            transportableStudyEntry = deserializeStudyEntryMessage(message);

            // Extract the list of BulkData UUIDs to manage
            for (BulkDataSet bulkDataSet : transportableStudyEntry.getBulkDataSetHistory()) {
                for (BulkDataInfo bulkDataInfo : bulkDataSet) {
                    UUID uuid = bulkDataInfo.getUUID();

                    // In case this BulkData file has already been processed, do
                    // not overwrite!
                    if (!bulkDataUUIDs.containsKey(uuid)) {
                        FileStorageStatus fileStorageStatus = new FileStorageStatus(
                                FileStorageStatusType.PENDING);
                        bulkDataUUIDs.put(uuid, fileStorageStatus);
                    }
                }
            }

            // Extract the list of Version UUIDs to manage
            for (VersionInfo versionInfo : transportableStudyEntry.getVersionHistory()) {
                UUID uuid = versionInfo.getUUID();

                // In case this Version file has already been processed, do not
                // overwrite!
                if (!versionUUIDs.containsKey(uuid)) {
                    FileStorageStatus fileStorageStatus = new FileStorageStatus(
                            FileStorageStatusType.PENDING);
                    versionUUIDs.put(uuid, fileStorageStatus);
                }
            }

            commitStudyIfFullyReceived();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }
    }

    /**
     * Commits the study to the store, and queues the study for forwarding by
     * the DicomSender, if it has been fully received.
     */
    private void commitStudyIfFullyReceived() {
        if (isStudyFullyReceived()) {
            // Insert the StudyEntry into the store
            store.insertStudyEntry(transportableStudyEntry);
            LOG.info("Done storing StudyEntry");

            // Send the study to the StudySendQueue
            if (studySendQueue != null) {
                StudySendTask studySendTask = new StudySendTask(studyInstanceUID, aeTitlePair);
                studySendQueue.addTask(studySendTask);
            } else {
                LOG.error("Cannot send study: StudySendQueue is not set.");
            }

            // Remove the StudyReceiveManager from the map
            studiesInProcess.remove(groupID);
        }
    }

    /**
     * Checks to see if each expected file in a particular group has been
     * received and correctly stored.
     * 
     * @return true if every expected file has been received and stored
     *         successfully; otherwise, returns false
     */
    private boolean isStudyFullyReceived() {
        // TODO how to validate the checksum of the object
        if (transportableStudyEntry != null) {
            for (FileStorageStatus status : bulkDataUUIDs.values()) {
                if (status.getCurrentStatus() != FileStorageStatusType.SUCCESS) {
                    return false;
                }
            }

            for (FileStorageStatus status : versionUUIDs.values()) {
                if (status.getCurrentStatus() != FileStorageStatusType.SUCCESS) {
                    return false;
                }
            }

            // All files have been received and stored, and the StudyEntry is
            // ready to store
            return true;
        }

        return false;
    }

    /**
     * De-serializes the body of the provided ClientMessage into a
     * TransportableStudyEntry object.
     * 
     * @param message
     *            The message which contains the serialized
     *            TransportableStudyEntry object.
     * @return The de-serialized TransportableStudyEntry.
     */
    public static TransportableStudyEntry deserializeStudyEntryMessage(ClientMessage message) {
        TransportableStudyEntry studyEntry = null;

        byte[] messageBytes = new byte[message.getBodySize()];
        message.getBodyBuffer().readBytes(messageBytes);

        try (ByteArrayInputStream bis = new ByteArrayInputStream(messageBytes);
                ObjectInputStream ois = new ObjectInputStream(bis);) {
            studyEntry = (TransportableStudyEntry) ois.readObject();
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        return studyEntry;
    }
}
