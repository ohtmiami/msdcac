/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport;

import static org.oht.miami.msdcache.transport.util.QueueUtils.MSD_RECEIVER_CONFIG;
import static org.oht.miami.msdcache.transport.util.QueueUtils.MSD_RECEIVER_HORNETQ_SERVER_ID;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_RECEIVE_QUEUE_ADDR;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_RECEIVE_QUEUE_NAME;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_SEND_QUEUE_ADDR;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_SEND_QUEUE_NAME;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.core.config.impl.FileConfiguration;
import org.hornetq.core.remoting.impl.invm.InVMAcceptorFactory;
import org.hornetq.core.remoting.impl.netty.NettyAcceptorFactory;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.forward.DicomSender;
import org.oht.miami.msdcache.transport.queue.StudyReceiveQueue;
import org.oht.miami.msdcache.transport.queue.StudySendQueue;
import org.oht.miami.msdcache.transport.queue.monitor.msdreceiver.MonitorStudyReceiveQueue;
import org.oht.miami.msdcache.transport.queue.monitor.msdreceiver.MonitorStudySendQueue;
import org.oht.miami.msdcache.transport.util.QueueUtils;
import org.oht.miami.msdtk.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class which provides the public interface to the transport receiving
 * module. Although this class does not enforce the Singleton pattern, it should
 * never be instantiated more than once per server.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class MSDReceiver extends MSDTransport {

    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(MSDReceiver.class);

    /**
     * The StudySendQueue, a persistent queue containing (the textual
     * representation) of StudySendTask objects. The messages that are placed in
     * this queue represent the studies that have been fully received by the
     * MSDReceiver which then need to be forwarded to their final destination.
     */
    private StudySendQueue studySendQueue;

    /**
     * The StudyReceiveQueue, a persistent queue that receives StudyEntry
     * messages, BulkData messages, and Version messages from a remote
     * MSDSender.
     */
    private StudyReceiveQueue studyReceiveQueue;

    /**
     * The monitor responsible for taking action when a new message is received
     * by the StudyReceiveQueue.
     */
    private MonitorStudyReceiveQueue srqMonitor;

    /**
     * The Future<Void> object that is returned by submitting the
     * MonitorStudyReceiveQueue object to the ExecutorService.
     */
    private Future<Void> srqMonitorThread;

    /**
     * The monitor responsible for taking action when a new message is received
     * by the StudySendQueue.
     */
    private MonitorStudySendQueue ssqMonitor;

    /**
     * The Future<Void> object that is returned by submitting the
     * MonitorStudySendQueue object to the ExecutorService.
     */
    private Future<Void> ssqMonitorThread;

    /**
     * A reference to the DicomSender object that is provided to the MSDReceiver
     * by the parent application. A reference to the DicomSender is required to
     * be able to forward studies to their final destination once they have been
     * fully received and stored by the MSDReceiver.
     */
    private DicomSender dicomSender;

    /**
     * Constructor
     * 
     * @param store
     *            A reference to the cache's Store.
     * @param executorService
     *            The thread pool provided by the cache application.
     */
    public MSDReceiver(Store store, ExecutorService executorService) {
        if (store == null) {
            throw new IllegalArgumentException(
                    "The MSDReceiver cannot have a null reference to the Store.");
        } else if (executorService == null) {
            throw new IllegalArgumentException(
                    "The MSDReceiver cannot have a null reference to the ExecutorService (thread pool).");
        }

        this.store = store;
        this.executorService = executorService;
    }

    @Override
    public synchronized void start() {
        if (started) {
            LOG.info("The MSDReceiver is already started.");
            return;
        }

        try {
            // Initialize the FileConfiguration object so that it can be
            // populated with the necessary Acceptors and Connectors
            FileConfiguration config = new FileConfiguration();
            config.setConfigurationUrl(MSD_RECEIVER_CONFIG);

            // Setup Acceptors
            Set<TransportConfiguration> acceptorTransports = new HashSet<TransportConfiguration>();

            Map<String, Object> nettyAcceptorParams = new HashMap<String, Object>();
            nettyAcceptorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.PORT_PROP_NAME,
                    QueueUtils.MSD_RECEIVER_HORNETQ_PORT);
            nettyAcceptorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.HOST_PROP_NAME,
                    "0.0.0.0");
            acceptorTransports.add(new TransportConfiguration(NettyAcceptorFactory.class.getName(),
                    nettyAcceptorParams));

            Map<String, Object> invmAcceptorParams = new HashMap<String, Object>();
            invmAcceptorParams.put(
                    org.hornetq.core.remoting.impl.invm.TransportConstants.SERVER_ID_PROP_NAME,
                    QueueUtils.MSD_RECEIVER_HORNETQ_SERVER_ID);
            acceptorTransports.add(new TransportConfiguration(InVMAcceptorFactory.class.getName(),
                    invmAcceptorParams));

            config.setAcceptorConfigurations(acceptorTransports);

            // Setup Connectors
            Map<String, TransportConfiguration> connectorTransports = new HashMap<String, TransportConfiguration>();

            Map<String, Object> nettyConnectorParams = new HashMap<String, Object>();
            nettyConnectorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.PORT_PROP_NAME,
                    QueueUtils.MSD_RECEIVER_HORNETQ_PORT);
            nettyConnectorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.HOST_PROP_NAME,
                    "localhost");

            TransportConfiguration connectorConfig = new TransportConfiguration(
                    NettyConnectorFactory.class.getName(), nettyConnectorParams);
            connectorTransports.put(connectorConfig.getName(), connectorConfig);

            config.setConnectorConfigurations(connectorTransports);

            // Start the configuration
            config.start();

            // Instantiate the server, apply the configuration, and start it
            messageServer = new EmbeddedHornetQ();
            messageServer.setConfiguration(config);
            messageServer.start();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Setup the necessary queues
        studyReceiveQueue = new StudyReceiveQueue(this, messageServer,
                MSD_RECEIVER_HORNETQ_SERVER_ID, STUDY_RECEIVE_QUEUE_NAME, STUDY_RECEIVE_QUEUE_ADDR);
        studySendQueue = new StudySendQueue(this, messageServer, MSD_RECEIVER_HORNETQ_SERVER_ID,
                STUDY_SEND_QUEUE_NAME, STUDY_SEND_QUEUE_ADDR);

        // Start the StudyReceiveQueue monitor thread
        srqMonitor = new MonitorStudyReceiveQueue(this, studyReceiveQueue, store, executorService);
        srqMonitorThread = executorService.submit(srqMonitor);

        // Start the StudySendQueue monitor thread
        ssqMonitor = new MonitorStudySendQueue(this, studySendQueue, store, executorService);
        ssqMonitorThread = executorService.submit(ssqMonitor);

        // Pass the DicomSender to the queue monitor
        ssqMonitor.setDicomSender(dicomSender);

        started = true;
    }

    @Override
    public synchronized void stop() {
        if (!started) {
            LOG.info("The MSDReceiver cannot be stopped because it has not been started yet.");
            return;
        }

        try {
            srqMonitor.stop();
            srqMonitorThread.cancel(false);

            ssqMonitor.stop();
            ssqMonitorThread.cancel(false);

            studyReceiveQueue.stop();
            studySendQueue.stop();
            messageServer.stop();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        started = false;
    }

    /**
     * This method allows the parent application to hook the DicomSender into
     * the MSDReceiver.
     * 
     * @param dicomSender
     *            The reference to the DicomSender object, used by the parent
     *            application to convert MSD studies into standard Dicom and to
     *            forward them to their final destination.
     */
    public void setDicomSender(DicomSender dicomSender) {
        this.dicomSender = dicomSender;

        // If the MSDReceiver is already started, update the reference to the
        // DicomSender object in the StudySendQueueMonitor.
        if (started) {
            ssqMonitor.setDicomSender(dicomSender);
        }
    }

    /**
     * This method exposes the StudySendQueue object.
     * 
     * @return The StudySendQueue.
     */
    public StudySendQueue getStudySendQueue() {
        return studySendQueue;
    }
}
