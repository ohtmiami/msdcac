/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.study;

import org.oht.miami.msdcache.util.AETitlePair;
import org.oht.miami.msdtk.util.DicomUID;

/**
 * A class which encapsulates the information required to forward a Study, such
 * as the StudyInstanceUID and AE Titles (represented as an AETitlePair).
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class StudySendTask {
    /**
     * The StudyInstanceUID of the Study that will be transported.
     */
    private final DicomUID studyInstanceUID;

    /**
     * The AETitlePair which specifies the source and destination AE Titles.
     */
    private final AETitlePair aeTitlePair;

    /**
     * Constructor
     * 
     * @param studyInstanceUID
     *            The StudyInstanceUID of the study being sent.
     * @param aeTitlePair
     *            The source and destination AE Titles.
     */
    public StudySendTask(DicomUID studyInstanceUID, AETitlePair aeTitlePair) {
        if (studyInstanceUID == null) {
            throw new IllegalArgumentException(
                    "A valid StudySendTask cannot have a null StudyInstanceUID.");
        } else if (aeTitlePair == null) {
            throw new IllegalArgumentException(
                    "A valid StudySendTask cannot have a null AETitlePair.");
        }

        this.studyInstanceUID = studyInstanceUID;
        this.aeTitlePair = aeTitlePair;
    }

    /**
     * Gets the StudyInstanceUID.
     * 
     * @return the StudyInstanceUID as a DicomUID object.
     */
    public DicomUID getStudyInstanceUID() {
        return studyInstanceUID;
    }

    /**
     * Gets the AETitlePair.
     * 
     * @return the AETitlePair.
     */
    public AETitlePair getAETitlePair() {
        return aeTitlePair;
    }

    /**
     * Gets the source AE Title.
     * 
     * @return The source AE Title.
     */
    public String getSourceAETitle() {
        return aeTitlePair.getSource();
    }

    /**
     * Gets the destination AE Title.
     * 
     * @return The destination AE Title.
     */
    public String getDesintationAETitle() {
        return aeTitlePair.getDestination();
    }

    /**
     * Serializes this StudySendTask into a string.
     * 
     * @return The string representation of this StudySendTask.
     */
    public String serialize() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(studyInstanceUID + ",");
        buffer.append(aeTitlePair.getSource().toString() + ",");
        buffer.append(aeTitlePair.getDestination().toString());

        return buffer.toString();
    }

    /**
     * De-serializes a StudySendTask from a string.
     * 
     * @param studySendTaskString
     *            The string containing the serialized form of a StudySendTask.
     * @return The StudySendTask that has been de-serialized from the provided
     *         string.
     */
    public static StudySendTask deserialize(String studySendTaskString) {
        String[] parts = studySendTaskString.split(",");

        DicomUID studyInstanceUID = new DicomUID(parts[0]);
        AETitlePair aeTitlePair = new AETitlePair(parts[1], parts[2]);

        return new StudySendTask(studyInstanceUID, aeTitlePair);
    }

    @Override
    public String toString() {
        return "(StudyInstanceUID: " + studyInstanceUID + ", AETitlePair: " + aeTitlePair + ")";
    }

    @Override
    public int hashCode() {
        String hashString = studyInstanceUID.toString() + aeTitlePair.toString();
        return hashString.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof StudySendTask)) {
            return false;
        }

        StudySendTask other = (StudySendTask) obj;
        return studyInstanceUID.equals(other.studyInstanceUID)
                && aeTitlePair.equals(other.aeTitlePair);
    }
}
