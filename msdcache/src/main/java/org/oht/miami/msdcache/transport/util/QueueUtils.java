/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Queue utilities.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class QueueUtils {
    private static Logger LOG = LoggerFactory.getLogger(QueueUtils.class);

    public final static int MSD_SENDER_HORNETQ_SERVER_ID = 0;
    public final static int MSD_RECEIVER_HORNETQ_SERVER_ID = 1;

    public final static String MSD_SENDER_CONFIG = "config/msdsender/hornetq-configuration.xml";
    public final static String MSD_RECEIVER_CONFIG = "config/msdreceiver/hornetq-configuration.xml";
    public final static String MSD_PORT_CONFIG = "config/msdports.properties";

    public final static int MSD_SENDER_HORNETQ_PORT = getMSDSenderHornetQPort();
    public final static int MSD_RECEIVER_HORNETQ_PORT = getMSDReceiverHornetQPort();

    public final static String ACK_RECEIVE_QUEUE_NAME = "ackReceiveQueue";
    public final static String ACK_RECEIVE_QUEUE_ADDR = "ackReceiveQueue";

    public final static String STUDY_RECEIVE_QUEUE_NAME = "studyReceiveQueue";
    public final static String STUDY_RECEIVE_QUEUE_ADDR = "studyReceiveQueue";

    public final static String STUDY_SEND_QUEUE_NAME = "studySendQueue";
    public final static String STUDY_SEND_QUEUE_ADDR = "studySendQueue";

    public final static int ACK_BATCH_SIZE = 0;

    /**
     * Gets the MSDSender HornetQ port from the config file.
     * 
     * @return The MSDSender HornetQ port.
     */
    private static int getMSDSenderHornetQPort() {
        final int DEFAULT = 5445;
        final String key = "msdsender.port";

        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(MSD_PORT_CONFIG));

            String port = properties.getProperty(key);

            if (port != null) {
                LOG.info("Using port " + port + " for the MSDSender.");
                return Integer.parseInt(port);
            } else {
                LOG.info("Property '" + key + "' not found. Using default port " + DEFAULT
                        + " for the MSDSender.");
                return DEFAULT;
            }
        } catch (IOException e) {
            LOG.info("Error opening properties file (" + MSD_PORT_CONFIG + "). Using default port "
                    + DEFAULT + " for the MSDSender.");
            return DEFAULT;
        }
    }

    /**
     * Gets the MSDReceiver HornetQ port from the config file.
     * 
     * @return The MSDReceiver HornetQ port.
     */
    private static int getMSDReceiverHornetQPort() {
        final int DEFAULT = 5446;
        final String key = "msdreceiver.port";

        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(MSD_PORT_CONFIG));

            String port = properties.getProperty(key);

            if (port != null) {
                LOG.info("Using port " + port + " for the MSDReceiver.");
                return Integer.parseInt(port);
            } else {
                LOG.info("Property '" + key + "' not found. Using default port " + DEFAULT
                        + " for the MSDReceiver.");
                return DEFAULT;
            }
        } catch (IOException e) {
            LOG.info("Error opening properties file (" + MSD_PORT_CONFIG + "). Using default port "
                    + DEFAULT + " for the MSDReceiver.");
            return DEFAULT;
        }
    }
}
