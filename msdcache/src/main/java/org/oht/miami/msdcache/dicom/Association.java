/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.UIDDictionary;
import org.dcm4che2.data.VR;
import org.dcm4che2.io.DicomCodingException;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.NoPresentationContextException;
import org.dcm4che2.net.UserIdentity;
import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.util.CloseUtils;
import org.dcm4che2.util.IntHashtable;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRQ;
import org.oht.miami.msdcache.dicom.pdu.PDV;
import org.oht.miami.msdcache.dicom.pdu.PDVType;
import org.oht.miami.msdcache.dicom.pdu.PDataTF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Association {

    private static Logger LOG = LoggerFactory.getLogger(Association.class);

    private static int nextSerialNo = 0;

    private final int serialNo;

    private final DicomConnector connector;

    private final Channel channel;

    private final boolean requestor;

    private NetworkApplicationEntity ae;

    private UserIdentity userIdentity;

    private AssociationState state;

    private String name;

    private AAssociateRQ associateRQ;

    private AAssociateAC associateAC;

    private IOException exception;

    private int maxOpsInvoked;

    private int maxPDULength;

    private boolean closed;

    private IntHashtable<DimseRSPHandler> rspHandlers = new IntHashtable<DimseRSPHandler>();

    private Map<String, Map<String, PresentationContext>> acceptedPCs = new HashMap<String, Map<String, PresentationContext>>();

    private Map<String, TransferCapability> scuTCs = new HashMap<String, TransferCapability>();

    private Map<String, TransferCapability> scpTCs = new HashMap<String, TransferCapability>();

    private int expectedPDVType = PDVType.COMMAND;

    private PresentationContext chosenPC = null;

    private PipedOutputStream pdvOut = null;

    private PipedInputStream pdvIn = null;

    private DicomObject command = null;

    private Future<DicomObject> commandParserFuture = null;

    private Future<Void> dataSetParserFuture = null;

    protected Association(Channel channel, DicomConnector connector, boolean requestor) {

        if (channel == null) {
            throw new NullPointerException("channel");
        }
        if (connector == null) {
            throw new NullPointerException("connector");
        }
        this.serialNo = ++nextSerialNo;
        this.name = "Association(" + serialNo + ")";
        this.connector = connector;
        this.channel = channel;
        this.requestor = requestor;
        closed = false;
    }

    public final boolean isRequestor() {

        return requestor;
    }

    AssociationState getState() {

        return state;
    }

    void setState(AssociationState state) {

        synchronized (this) {
            if (this.state == state) {
                return;
            }
            this.state = state;
            this.notifyAll();
        }
    }

    @Override
    public String toString() {

        return name;
    }

    public static Association request(Channel channel, DicomConnector connector,
            NetworkApplicationEntity ae, UserIdentity userIdentity) {

        Association association = new Association(channel, connector, true);
        association.setApplicationEntity(ae);
        association.setUserIdentity(userIdentity);
        association.setState(AssociationState.STA4);
        return association;
    }

    public static Association accept(Channel channel, DicomConnector connector) {

        Association association = new Association(channel, connector, false);
        association.setState(AssociationState.STA2);
        return association;
    }

    public final NetworkApplicationEntity getApplicationEntity() {

        return ae;
    }

    final void setApplicationEntity(NetworkApplicationEntity ae) {

        this.ae = ae;
    }

    final void setUserIdentity(UserIdentity userIdentity) {

        this.userIdentity = userIdentity;
    }

    public final AAssociateAC getAssociateAC() {

        return associateAC;
    }

    public final AAssociateRQ getAssociateRQ() {

        return associateRQ;
    }

    final IOException getException() {

        return exception;
    }

    final void setException(IOException exception) {

        this.exception = exception;
    }

    void checkException() throws IOException {

        if (exception != null) {
            throw exception;
        }
    }

    public final boolean isReadyForDataTransfer() {

        return state.isReadyForDataTransfer();
    }

    public boolean hasAvailableOps() {

        if (maxOpsInvoked == 0) {
            return true;
        }
        synchronized (rspHandlers) {
            return rspHandlers.size() < maxOpsInvoked;
        }
    }

    private boolean isReadyForDataReceive() {

        return state.isReadyForDataReceive();
    }

    private void processAC() {

        Collection<PresentationContext> c = associateAC.getPresentationContexts();
        for (PresentationContext pc : c) {
            if (!pc.isAccepted()) {
                continue;
            }
            PresentationContext pcrq = associateRQ.getPresentationContext(pc.getPCID());
            if (pcrq == null) {
                LOG.warn("{}: Missing Presentation Context(id={}) in received AA-AC", name,
                        pc.getPCID());
                continue;
            }
            String as = pcrq.getAbstractSyntax();
            Map<String, PresentationContext> ts2pc = acceptedPCs.get(as);
            if (ts2pc == null) {
                ts2pc = new HashMap<String, PresentationContext>();
                acceptedPCs.put(as, ts2pc);
            }
            ts2pc.put(pc.getTransferSyntax(), pc);
        }
        for (Map.Entry<String, Map<String, PresentationContext>> entry : acceptedPCs.entrySet()) {
            String asuid = entry.getKey();
            Map<String, PresentationContext> ts2pc = entry.getValue();
            String[] tsuids = ts2pc.keySet().toArray(new String[ts2pc.size()]);
            String cuid = asuid;
            ExtendedNegotiation extneg = associateAC.getExtendedNegotiationFor(cuid);
            byte[] extinfo = extneg != null ? extneg.getInformation() : null;
            if (isSCUFor(cuid)) {
                TransferCapability tc = new TransferCapability(cuid, tsuids, TransferCapability.SCU);
                tc.setExtInfo(extinfo);
                scuTCs.put(cuid, tc);
            }
            if (isSCPFor(cuid)) {
                TransferCapability tc = new TransferCapability(cuid, tsuids, TransferCapability.SCP);
                tc.setExtInfo(extinfo);
                scpTCs.put(cuid, tc);
            }
        }
    }

    private boolean isSCPFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return !requestor;
        }
        return requestor ? rs.isSCP() : rs.isSCU();
    }

    private boolean isSCUFor(String cuid) {

        RoleSelection rs = associateAC.getRoleSelectionFor(cuid);
        if (rs == null) {
            return requestor;
        }
        return requestor ? rs.isSCU() : rs.isSCP();
    }

    public String getCallingAETitle() {

        return associateRQ != null ? associateRQ.getCallingAET() : null;
    }

    public String getCalledAETitle() {

        return associateRQ != null ? associateRQ.getCalledAET() : null;
    }

    public String getRemoteAETitle() {

        return requestor ? getCalledAETitle() : getCallingAETitle();
    }

    public String getLocalAETitle() {

        return requestor ? getCallingAETitle() : getCalledAETitle();
    }

    public UserIdentity getUserIdentity() {

        return userIdentity;
    }

    public TransferCapability getTransferCapabilityAsSCP(String cuid) {

        return scpTCs.get(cuid);
    }

    public TransferCapability getTransferCapabilityAsSCU(String cuid) {

        return scuTCs.get(cuid);
    }

    public AAssociateAC negotiate(AAssociateRQ rq) throws IOException, InterruptedException {

        sendAssociateRQ(rq);
        synchronized (this) {
            while (state == AssociationState.STA5) {
                // If successful, state will be changed to STA6 by
                // onAssociateAC()
                // Otherwise, state will be changed to STA1 by closeChannel() or
                // onAssociateRJ()
                this.wait();
            }
        }
        checkException();
        if (state != AssociationState.STA6) {
            throw new RuntimeException("unexpected state: " + state);
        }
        return associateAC;
    }

    public void release() throws InterruptedException {

        if (ae != null) {
            ae.removeFromPool(this);
        }
        // TODO waitForRSP
        sendReleaseRQ();
        synchronized (this) {
            while (state != AssociationState.STA1) {
                // state will be changed to STA1 by onReleaseRP() or
                // closeChannel()
                this.wait();
            }
        }
    }

    public void waitForDimseRSP() throws InterruptedException {

        synchronized (rspHandlers) {
            while (!rspHandlers.isEmpty() && isReadyForDataReceive()) {
                rspHandlers.wait();
            }
        }
    }

    private PresentationContext pcFor(String cuid, String tsuid)
            throws NoPresentationContextException {

        Map<String, PresentationContext> ts2pc = acceptedPCs.get(cuid);
        if (ts2pc == null) {
            throw new NoPresentationContextException("Abstract Syntax "
                    + UIDDictionary.getDictionary().prompt(cuid) + " not supported");
        }
        if (tsuid == null) {
            return ts2pc.values().iterator().next();
        }
        PresentationContext pc = ts2pc.get(tsuid);
        if (pc == null) {
            throw new NoPresentationContextException("Abstract Syntax "
                    + UIDDictionary.getDictionary().prompt(cuid) + " with Transfer Syntax "
                    + UIDDictionary.getDictionary().prompt(tsuid) + " not supported");
        }
        return pc;
    }

    public void cstore(String cuid, String iuid, int priority, DataSetWriter dataSetWriter,
            String tsuid, DimseRSPHandler rspHandler) throws IOException, InterruptedException {

        cstore(cuid, cuid, iuid, priority, dataSetWriter, tsuid, rspHandler);
    }

    public void cstore(String asuid, String cuid, String iuid, int priority,
            DataSetWriter dataSetWriter, String tsuid, DimseRSPHandler rspHandler)
            throws IOException, InterruptedException {

        PresentationContext pc = pcFor(asuid, tsuid);
        DicomObject cstoreRQ = CommandUtils.mkCStoreRQ(ae.nextMessageID(), cuid, iuid, priority);
        invoke(pc.getPCID(), cstoreRQ, dataSetWriter, rspHandler, ae.getDimseRSPTimeout());
    }

    public DimseRSP cstore(String asuid, String cuid, String iuid, int priority,
            DataSetWriter dataSetWriter, String tsuid) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP();
        cstore(asuid, cuid, iuid, priority, dataSetWriter, tsuid, rsp);
        return rsp;
    }

    public DimseRSP cecho() throws IOException, InterruptedException {

        return cecho(UID.VerificationSOPClass);
    }

    public DimseRSP cecho(String cuid) throws IOException, InterruptedException {

        FutureDimseRSP rsp = new FutureDimseRSP();
        PresentationContext pc = pcFor(cuid, null);
        DicomObject cechoRQ = CommandUtils.mkCEchoRQ(ae.nextMessageID(), cuid);
        invoke(pc.getPCID(), cechoRQ, null, rsp, ae.getDimseRSPTimeout());
        return rsp;
    }

    void invoke(int pcid, DicomObject command, DataSetWriter dataSetWriter,
            DimseRSPHandler rspHandler, int rspTimeout) throws IOException, InterruptedException {

        if (CommandUtils.isResponse(command)) {
            throw new IllegalArgumentException("command:\n" + command);
        }
        checkException();
        if (!isReadyForDataTransfer()) {
            throw new IllegalStateException(state.toString());
        }
        PresentationContext pc = associateAC.getPresentationContext(pcid);
        if (pc == null) {
            throw new IllegalStateException("No Presentation Context with id - " + pcid);
        }
        if (!pc.isAccepted()) {
            throw new IllegalStateException("Presentation Context not accepted - " + pc);
        }
        rspHandler.setPCID(pcid);
        int messageID = command.getInt(Tag.MessageID);
        rspHandler.setMessageID(messageID);
        addDimseRSPHandler(messageID, rspHandler);
        channel.write(new PDataTF(pcid, command, dataSetWriter, pc.getTransferSyntax()));
        rspHandler.setTimeout(System.currentTimeMillis() + rspTimeout);
    }

    void cancel(int pcid, int messageID) {

        DicomObject command = CommandUtils.mkCCancelRQ(messageID);
        channel.write(new PDataTF(pcid, command, null, null));
    }

    public void writeDimseRSP(int pcid, DicomObject command) {

        writeDimseRSP(pcid, command, null);
    }

    public void writeDimseRSP(int pcid, DicomObject command, DicomObject dataSet) {

        if (!CommandUtils.isResponse(command)) {
            throw new IllegalArgumentException("command:\n" + command);
        }
        PresentationContext pc = associateAC.getPresentationContext(pcid);
        if (pc == null) {
            throw new IllegalStateException("No Presentation Context with id - " + pcid);
        }
        if (!pc.isAccepted()) {
            throw new IllegalStateException("Presentation Context not accepted - " + pc);
        }

        DataSetWriter dataSetWriter = null;
        int dataSetType = CommandUtils.NO_DATASET;
        if (dataSet != null) {
            dataSetWriter = new DataSetWriterAdapter(dataSet);
            dataSetType = CommandUtils.getWithDatasetType();
        }
        command.putInt(Tag.CommandDataSetType, VR.US, dataSetType);
        // TODO Run this in a separate thread?
        channel.write(new PDataTF(pcid, command, dataSetWriter, pc.getTransferSyntax()));
        // TODO updateIdleTimeout(), decPerforming()
    }

    void onDimseRSP(DicomObject command, DicomObject dataSet) throws IOException {

        int messageID = command.getInt(Tag.MessageIDBeingRespondedTo);
        DimseRSPHandler rspHandler = getDimseRSPHandler(messageID);
        if (rspHandler == null) {
            LOG.warn("unexpected message ID in DIMSE RSP:\n{}", command);
            throw new AAbort();
        }
        try {
            rspHandler.onDimseRSP(this, command, dataSet);
        } finally {
            if (!CommandUtils.isPending(command)) {
                // TODO updateIdleTimeout
                removeDimseRSPHandler(messageID);
            } else {
                // TODO rspHandler.setTimeout
            }
        }
    }

    private void addDimseRSPHandler(int messageID, DimseRSPHandler rspHandler)
            throws InterruptedException {

        synchronized (rspHandlers) {
            while (maxOpsInvoked > 0 && rspHandlers.size() > maxOpsInvoked) {
                rspHandlers.wait();
            }
            if (isReadyForDataReceive()) {
                rspHandlers.put(messageID, rspHandler);
            }
        }
    }

    private DimseRSPHandler removeDimseRSPHandler(int messageID) {

        synchronized (rspHandlers) {
            DimseRSPHandler rspHandler = (DimseRSPHandler) rspHandlers.remove(messageID);
            rspHandlers.notifyAll();
            return rspHandler;
        }
    }

    private DimseRSPHandler getDimseRSPHandler(int messageID) {

        synchronized (rspHandlers) {
            return rspHandlers.get(messageID);
        }
    }

    void closeChannel() {

        if (state == AssociationState.STA13) {
            // TODO Sleep
        }
        setState(AssociationState.STA1);
        if (!closed) {
            LOG.info("{}: close {}", name, channel);
            channel.close();
            closed = true;
            onClosed();
        }
    }

    private void onClosed() {

        if (ae != null) {
            ae.removeFromPool(this);
        }
        synchronized (rspHandlers) {
            rspHandlers.accept(new IntHashtable.Visitor() {

                @Override
                public boolean visit(int key, Object value) {

                    ((DimseRSPHandler) value).onClosed(Association.this);
                    return true;
                }
            });
            rspHandlers.clear();
            rspHandlers.notifyAll();
        }
        if (ae != null) {
            ae.associationClosed(this);
        }
    }

    int getMaxPDULengthSend() {

        return maxPDULength;
    }

    void receivedAssociateRQ(AAssociateRQ rq) throws IOException {

        LOG.info("{}: A-ASSOCIATE-RQ {} >> {}",
                new String[] { name, rq.getCallingAET(), rq.getCalledAET() });
        state.receivedAssociateRQ(this, rq);
    }

    void receivedAssociateAC(AAssociateAC ac) throws IOException {

        LOG.info("{}: A-ASSOCIATE-AC {} >> {}",
                new String[] { name, ac.getCallingAET(), ac.getCalledAET() });
        state.receivedAssociateAC(this, ac);
    }

    void receivedAssociateRJ(AAssociateRJ rj) throws IOException {

        LOG.info("{} >> {}", name, rj);
        state.receivedAssociateRJ(this, rj);
    }

    void receivedPDV(PDV pdv) throws IOException {

        state.receivedPDV(this, pdv);
    }

    void onPDV(PDV pdv) throws IOException {

        if (pdv.getType() != expectedPDVType) {
            LOG.warn(
                    "{}: "
                            + (expectedPDVType == PDVType.COMMAND ? "Expected Command but received Data Set PDV"
                                    : "Expected Data Set but received Command PDV"), this);
            throw new AAbort();
        }

        int pcid = pdv.getPCID();
        if (chosenPC == null) {
            PresentationContext pc = getAssociateAC().getPresentationContext(pcid);
            if (pc == null) {
                LOG.warn("{}: No Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            if (!pc.isAccepted()) {
                LOG.warn("{}: No accepted Presentation Context with given ID - {}", this, pcid);
                throw new AAbort();
            }
            chosenPC = pc;
        } else {
            int chosenPCID = chosenPC.getPCID();
            if (pcid != chosenPCID) {
                LOG.warn("{}: Expected PDV with pcid: {} but received with pcid: {}", new Object[] {
                                this, chosenPCID, pcid });
                throw new AAbort();
            }
        }

        if (pdvOut == null) {
            pdvOut = new PipedOutputStream();
            // TODO Tune pipe size
            pdvIn = new PipedInputStream(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                commandParserFuture = connector.getExecutorService().submit(
                        new Callable<DicomObject>() {

                            @Override
                            public DicomObject call() throws Exception {

                                return readDicomObject(TransferSyntax.ImplicitVRLittleEndian);
                            }
                        });
            } else {
                dataSetParserFuture = connector.getExecutorService().submit(new Callable<Void>() {

                    @Override
                    public Void call() throws Exception {

                        if (CommandUtils.isResponse(command)) {
                            DicomObject dataSet = readDicomObject(TransferSyntax.valueOf(chosenPC
                                    .getTransferSyntax()));
                            onDimseRSP(command, dataSet);
                        } else {
                            onDimseRQ(chosenPC, command, pdvIn);
                        }
                        return null;
                    }
                });
            }
        }
        ChannelBuffer pdvData = pdv.getData();
        pdvData.readBytes(pdvOut, pdvData.readableBytes());

        if (pdv.isLast()) {
            CloseUtils.safeClose(pdvOut);
            if (expectedPDVType == PDVType.COMMAND) {
                onLastCommandPDV();
            } else {
                onLastDataSetPDV();
            }
        }
    }

    private DicomObject readDicomObject(TransferSyntax transferSyntax) throws IOException {

        DicomInputStream dicomIn = new DicomInputStream(pdvIn, transferSyntax);
        try {
            return dicomIn.readDicomObject();
        } catch (DicomCodingException e) {
            LOG.warn("{}: Failed to decode dicom object: ", this, e.getMessage());
            throw new AAbort();
        } finally {
            CloseUtils.safeClose(dicomIn);
        }
    }

    private void onLastCommandPDV() throws IOException {

        try {
            command = commandParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn("{}: Interrupted while parsing DIMSE Command: {}", this, e.getMessage());
            throw new AAbort();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                LOG.warn("{}: Unexpected error while parsing DIMSE Command: {}", this,
                        e.getMessage());
                throw new AAbort();
            }
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;
        if (CommandUtils.hasDataset(command)) {
            expectedPDVType = PDVType.DATA;
        } else {
            if (CommandUtils.isResponse(command)) {
                onDimseRSP(command, null);
            } else {
                onDimseRQ(chosenPC, command, null);
            }
            chosenPC = null;
        }
    }

    private void onLastDataSetPDV() throws IOException {

        try {
            dataSetParserFuture.get();
        } catch (InterruptedException e) {
            LOG.warn("{}: Interrupted while processing DIMSE: {}", this, e.getMessage());
            throw new AAbort();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                LOG.warn("{}: Unexpected error while processing DIMSE: {}", this, e.getMessage());
                throw new AAbort();
            }
        } finally {
            CloseUtils.safeClose(pdvIn);
        }

        pdvOut = null;
        pdvIn = null;
        expectedPDVType = PDVType.COMMAND;
        chosenPC = null;
    }

    void receivedReleaseRQ() throws IOException {

        LOG.info("{} >> A-RELEASE-RQ", name);
        state.receivedReleaseRQ(this);
    }

    void receivedReleaseRP() throws IOException {

        LOG.info("{} >> A-RELEASE-RP", name);
        state.receivedReleaseRP(this);
    }

    void receivedAbort(AAbort aa) {

        LOG.info("{}: >> {}", name, aa);
        exception = aa;
        setState(AssociationState.STA1);
        ae.removeFromPool(this);
    }

    void onDimseRQ(PresentationContext pc, DicomObject command, InputStream dataStream)
            throws IOException {

        // TODO incPerforming();
        ae.perform(this, pc.getPCID(), command, dataStream, pc.getTransferSyntax());
    }

    void sendPDataTF(ChannelBuffer pd) {

        try {
            state.sendPDataTF(this, pd);
        } catch (IOException e) {
            closeChannel();
        }
    }

    void writePDataTF(ChannelBuffer pd) {

        channel.write(pd);
    }

    void sendAssociateRQ(AAssociateRQ rq) {

        state.sendAssociateRQ(this, rq);
    }

    void sendReleaseRQ() {

        try {
            state.sendReleaseRQ(this);
        } catch (IOException e) {
            closeChannel();
        }
    }

    void abort(AAbort aa) {

        if (ae != null) {
            ae.removeFromPool(this);
        }
        state.abort(this, aa);
    }

    void writeAbort(AAbort aa) {

        exception = aa;
        setState(AssociationState.STA13);
        ChannelFuture writeFuture = channel.write(aa);
        writeFuture.addListener(new ChannelFutureListener() {

            @Override
            public void operationComplete(ChannelFuture future) throws Exception {

                closeChannel();
            }
        });
    }

    void unexpectedPDU(String name) throws AAbort {

        LOG.warn("received unexpected {} in state: {}", name, state);
        throw new AAbort(AAbort.UL_SERIVE_PROVIDER, AAbort.UNEXPECTED_PDU);
    }

    void illegalStateForSending(String name) throws IOException {

        LOG.warn("unable to send " + name + " in state: " + state);
        checkException();
        throw new AAbort();
    }

    void writeAssociateRQ(AAssociateRQ rq) {

        associateRQ = rq;
        name = rq.getCalledAET() + "(" + serialNo + ")";
        setState(AssociationState.STA5);
        channel.write(rq);
    }

    void onAssociateRQ(AAssociateRQ rq) {

        associateRQ = rq;
        name = rq.getCallingAET() + "(" + serialNo + ")";
        setState(AssociationState.STA3);
        try {
            if ((rq.getProtocolVersion() & 1) == 0) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_PROVIDER_ACSE,
                        AAssociateRJ.REASON_PROTOCOL_VERSION_NOT_SUPPORTED);
            }
            if (!rq.getApplicationContext().equals(UID.DICOMApplicationContextName)) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_USER,
                        AAssociateRJ.REASON_APP_CTX_NAME_NOT_SUPPORTED);
            }
            NetworkApplicationEntity ae = connector.getLocalApplicationEntity(rq.getCalledAET());
            if (ae == null) {
                throw new AAssociateRJ(AAssociateRJ.RESULT_REJECTED_PERMANENT,
                        AAssociateRJ.SOURCE_SERVICE_USER,
                        AAssociateRJ.REASON_CALLED_AET_NOT_RECOGNIZED);
            }
            // TODO checkConnectionCountWithinLimit()
            setApplicationEntity(ae);
            associateAC = ae.negotiate(this, rq);
            processAC();
            maxOpsInvoked = associateAC.getMaxOpsInvoked();
            maxPDULength = minZeroAsMax(rq.getMaxPDULength(), ae.getMaxPDULengthSend());
            setState(AssociationState.STA6);
            channel.write(associateAC);
            ae.addToPool(this);
            ae.associationAccepted(this);
        } catch (AAssociateRJ rj) {
            setState(AssociationState.STA13);
            channel.write(rj);
        }
    }

    void onAssociateAC(AAssociateAC ac) {

        associateAC = ac;
        processAC();
        maxOpsInvoked = associateAC.getMaxOpsInvoked();
        maxPDULength = minZeroAsMax(associateAC.getMaxPDULength(), ae.getMaxPDULengthSend());
        setState(AssociationState.STA6);
    }

    private int minZeroAsMax(int i1, int i2) {

        return i1 == 0 ? i2 : i2 == 0 ? i1 : Math.min(i1, i2);
    }

    void onAssociateRJ(AAssociateRJ rj) {

        exception = rj;
        setState(AssociationState.STA1);
    }

    void writeReleaseRQ() {

        setState(AssociationState.STA7);
        channel.write(new AReleaseRQ());
    }

    void onReleaseRP() {

        setState(AssociationState.STA1);
    }

    void onCollisionReleaseRP() {

        LOG.info("{} << A-RELEASE-RP", name);
        setState(AssociationState.STA13);
        channel.write(new AReleaseRP());
    }

    void onReleaseRQ() {

        setState(AssociationState.STA8);
        if (ae != null) {
            ae.removeFromPool(this);
        }
        // TODO waitForPerformingOps();
        setState(AssociationState.STA13);
        channel.write(new AReleaseRP());
    }

    void onCollisionReleaseRQ() {

        if (requestor) {
            setState(AssociationState.STA1);
            channel.write(new AReleaseRP());
        } else {
            setState(AssociationState.STA10);
        }
    }
}
