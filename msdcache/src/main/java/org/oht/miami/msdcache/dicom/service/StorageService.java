/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom.service;

import java.io.IOException;
import java.io.InputStream;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.UID;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.DicomServiceException;
import org.oht.miami.msdcache.dicom.Association;

public class StorageService extends DicomService implements CStoreSCP {

    protected StorageService(String sopClass) {

        super(sopClass);
    }

    protected StorageService(String[] sopClasses) {

        super(sopClasses, UID.StorageServiceClass);
    }

    @Override
    public void cstore(Association association, int pcid, DicomObject command,
            InputStream dataStream, String tsuid) throws DicomServiceException, IOException {

        DicomObject rsp = CommandUtils.mkRSP(command, CommandUtils.SUCCESS);
        onCStoreRQ(association, pcid, command, dataStream, tsuid, rsp);
        association.writeDimseRSP(pcid, rsp);
        onCStoreRSP(association, pcid, command, dataStream, tsuid, rsp);
    }

    protected void onCStoreRQ(Association association, int pcid, DicomObject rq,
            InputStream dataStream, String tsuid, DicomObject rsp) throws DicomServiceException,
            IOException {
    }

    protected void onCStoreRSP(Association association, int pcid, DicomObject rq,
            InputStream dataStream, String tsuid, DicomObject rsp) throws DicomServiceException,
            IOException {
    }
}
