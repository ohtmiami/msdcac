/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport;

import java.util.concurrent.ExecutorService;

import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdtk.store.Store;

/**
 * An abstract parent class for the different MSD transport modules (MSDSender
 * and MSDReceiver classes).
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public abstract class MSDTransport {

    /**
     * A boolean specifying whether or not this object has already been started.
     */
    protected boolean started = false;

    /**
     * The Store, used to store and retrieve files to/from disk.
     */
    protected Store store;

    /**
     * The thread pool provided to this object by the cache application.
     */
    protected ExecutorService executorService;

    /**
     * The HornetQ server instance which contains the queues required by this
     * component.
     */
    protected EmbeddedHornetQ messageServer;

    /**
     * This method specifies the start-up behavior of the component. This method
     * must be called after instantiation in order to "start" the component.
     */
    public abstract void start();

    /**
     * This method specifies the shut-down behavior of the component. This
     * method should be called when the overall application is being shut-down.
     */
    public abstract void stop();
}
