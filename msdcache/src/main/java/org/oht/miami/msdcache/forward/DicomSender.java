/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.forward;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.UID;
import org.dcm4che2.data.UIDDictionary;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.net.Status;
import org.oht.miami.msdcache.dicom.Association;
import org.oht.miami.msdcache.dicom.DataSetWriterAdapter;
import org.oht.miami.msdcache.dicom.DicomClient;
import org.oht.miami.msdcache.dicom.DimseRSPHandler;
import org.oht.miami.msdcache.dicom.NetworkApplicationEntity;
import org.oht.miami.msdcache.dicom.TransferCapability;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.Study2Dicom;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomSender extends DicomClient {

    private static Logger LOG = LoggerFactory.getLogger(DicomSender.class);

    private static final String[] IVLE_TS = { UID.ImplicitVRLittleEndian,
                    UID.ExplicitVRLittleEndian, UID.ExplicitVRBigEndian, };

    private static final String[] EVLE_TS = { UID.ExplicitVRLittleEndian,
                    UID.ImplicitVRLittleEndian, UID.ExplicitVRBigEndian, };

    private static final String[] EVBE_TS = { UID.ExplicitVRBigEndian, UID.ExplicitVRLittleEndian,
                    UID.ImplicitVRLittleEndian, };

    private final Store store;

    public DicomSender(ExecutorService executorService, Store store) {

        super(executorService);
        this.store = store;
    }

    public void addLocalApplicationEntity(String aeTitle, String hostname, int port) {

        NetworkApplicationEntity localAE = new NetworkApplicationEntity(aeTitle, hostname, port);
        localAE.setAssociationInitiator(true);
        NetworkApplicationEntity oldAE = this.addLocalApplicationEntity(localAE);
        if (oldAE != null) {
            LOG.warn("Replaced previously configured local AE {}", aeTitle);
        }
    }

    public void addLocalApplicationEntity(String aeTitle) {

        addLocalApplicationEntity(aeTitle, null, 0);
    }

    public void addRemoteApplicationEntity(String aeTitle, String hostname, int port) {

        NetworkApplicationEntity remoteAE = new NetworkApplicationEntity(aeTitle, hostname, port);
        NetworkApplicationEntity oldAE = this.addRemoteApplicationEntity(remoteAE);
        if (oldAE != null) {
            LOG.warn("Replaced previously configured remote AE {}", aeTitle);
        }
    }

    public void sendStudy(String remoteAET, String localAET, DicomUID studyInstanceUID)
            throws IOException, InterruptedException {

        // TODO Include version number as well?
        Study study = store.readVersion(studyInstanceUID);
        // TODO study.getSOPClassUIDs()
        Set<String> cuids = new HashSet<String>();
        cuids.add(study.getValueForAttributeAsString(Tag.SOPClassUID));
        // TODO study.getTransferSyntaxUIDs()
        Set<String> tsuids = new HashSet<String>();
        tsuids.add(study.getValueForAttributeAsString(Tag.TransferSyntaxUID));

        // TODO Redesign converter interface
        File convertedStudyDir = new File("target/test-out/converted/"
                + studyInstanceUID.toString());
        convertedStudyDir.mkdirs();
        Study2Dicom.study2SingleFrameDicom(study, convertedStudyDir, false);
        File[] sopInstanceFiles = convertedStudyDir.listFiles();

        Association association = initiateAssociation(remoteAET, localAET, cuids, tsuids);
        for (File sopInstanceFile : sopInstanceFiles) {
            DicomInputStream dicomIn = new DicomInputStream(sopInstanceFile);
            DicomObject sopInstance = null;
            try {
                sopInstance = dicomIn.readDicomObject();
            } finally {
                dicomIn.close();
            }
            sendSOPInstance(association, sopInstance);
            sopInstanceFile.delete();
        }
        association.waitForDimseRSP();
        association.release();
    }

    private Association initiateAssociation(String remoteAET, String localAET, Set<String> cuids,
            Set<String> tsuids) throws IOException, InterruptedException {

        // TODO Remove
        cuids.add(UID.SecondaryCaptureImageStorage);

        tsuids.add(UID.ImplicitVRLittleEndian);
        String[] tsuidArray = new String[tsuids.size()];
        tsuids.toArray(tsuidArray);

        TransferCapability[] tcs = new TransferCapability[cuids.size()];
        Iterator<String> cuidIter = cuids.iterator();
        for (int i = 0; i < tcs.length; i++) {
            tcs[i] = new TransferCapability(cuidIter.next(), tsuidArray, TransferCapability.SCU);
        }
        return this.connect(remoteAET, localAET, tcs);
    }

    private void sendSOPInstance(Association association, DicomObject sopInstance)
            throws IOException, InterruptedException {

        final String cuid = sopInstance.getString(Tag.MediaStorageSOPClassUID,
                sopInstance.getString(Tag.SOPClassUID));
        final String iuid = sopInstance.getString(Tag.MediaStorageSOPInstanceUID,
                sopInstance.getString(Tag.SOPInstanceUID));
        final String tsuid = sopInstance.getString(Tag.TransferSyntaxUID);

        TransferCapability tc = association.getTransferCapabilityAsSCU(cuid);
        if (tc == null) {
            // TODO Create a new exception type
            throw new IOException(UIDDictionary.getDictionary().prompt(cuid) + " not supported by "
                    + association.getCalledAETitle());
        }
        String selectedTSUID = selectTransferSyntax(tc.getTransferSyntax(), tsuid);
        if (selectedTSUID == null) {
            // TODO Create a new exception type
            throw new IOException(UIDDictionary.getDictionary().prompt(cuid) + " with "
                    + UIDDictionary.getDictionary().prompt(tsuid) + " not supported by "
                    + association.getCalledAETitle());
        }

        association.cstore(cuid, iuid, 0, new DataSetWriterAdapter(sopInstance), selectedTSUID,
                new DimseRSPHandler() {

                    @Override
                    public void onDimseRSP(Association association, DicomObject command,
                            DicomObject dataSet) {

                        int status = command.getInt(Tag.Status);
                        if (status != Status.Success) {
                            LOG.error("Instance {} was not sent successfully (status={})", iuid,
                                    status);
                        } else {
                            LOG.info("Instance {} was sent successfully", iuid);
                        }
                    }
                });
    }

    private String selectTransferSyntax(String[] available, String tsuid) {

        if (tsuid.equals(UID.ImplicitVRLittleEndian)) {
            return selectTransferSyntax(available, IVLE_TS);
        }
        if (tsuid.equals(UID.ExplicitVRLittleEndian)) {
            return selectTransferSyntax(available, EVLE_TS);
        }
        if (tsuid.equals(UID.ExplicitVRBigEndian)) {
            return selectTransferSyntax(available, EVBE_TS);
        }
        for (int j = 0; j < available.length; j++) {
            if (available[j].equals(tsuid)) {
                return tsuid;
            }
        }
        return null;
    }

    private String selectTransferSyntax(String[] available, String[] tsuids) {

        for (int i = 0; i < tsuids.length; i++) {
            for (int j = 0; j < available.length; j++) {
                if (available[j].equals(tsuids[i])) {
                    return available[j];
                }
            }
        }
        return null;
    }
}
