/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.net.CommandUtils;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelDownstreamHandler;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.oht.miami.msdcache.dicom.pdu.PDVType;
import org.oht.miami.msdcache.dicom.pdu.PDataTF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDataTFEncoder implements ChannelDownstreamHandler {

    private static Logger LOG = LoggerFactory.getLogger(PDataTFEncoder.class);

    @Override
    public void handleDownstream(ChannelHandlerContext ctx, ChannelEvent evt) throws Exception {

        if (!(evt instanceof MessageEvent)) {
            ctx.sendDownstream(evt);
            return;
        }

        Object message = ((MessageEvent) evt).getMessage();
        if (!(message instanceof PDataTF)) {
            ctx.sendDownstream(evt);
            return;
        }

        PDataTF pd = (PDataTF) message;
        Channel channel = evt.getChannel();
        Association association = AssociationHandler.associations.get(channel);
        int pcid = pd.getPCID();
        String tsuid = pd.getTransferSyntax();
        DicomObject command = pd.getCommand();
        LOG.info("{} << {}", association, CommandUtils.toString(command, pcid, tsuid));

        PDataTFOutputStream pdOut = new PDataTFOutputStream(association, pcid, PDVType.COMMAND);
        DicomOutputStream commandOut = new DicomOutputStream(pdOut);
        commandOut.writeCommand(command);
        commandOut.close();

        DataSetWriter dataSetWriter = pd.getDataSetWriter();
        if (dataSetWriter != null) {
            pdOut = new PDataTFOutputStream(association, pcid, PDVType.DATA);
            dataSetWriter.writeTo(pdOut, tsuid);
            pdOut.close();
        }
    }
}
