/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue;

import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.client.ClientMessage;
import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.transport.MSDTransport;

/**
 * A persistent queue containing StudyEntry messages, BulkData messages, and
 * Version messages that have been received from the MSDSender of another node.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class StudyReceiveQueue extends QueueManager {
    /**
     * Constructor
     * 
     * @param context
     *            The MSDSender or MSDReceiver that is instantiating this queue.
     * @param messageServer
     *            The HornetQ server instance on which this queue will be
     *            deployed.
     * @param serverID
     *            The ID of the HornetQ server.
     * @param queueName
     *            The unique name of this queue.
     * @param queueAddress
     *            The unique address of this queue.
     */
    public StudyReceiveQueue(MSDTransport context, EmbeddedHornetQ messageServer, int serverID,
            String queueName, String queueAddress) {
        super(context, messageServer, serverID, queueName, queueAddress);
    }

    /**
     * This method receives and returns a message from this queue, blocking the
     * thread while waiting for a message or for the specified timeout to pass.
     * 
     * @param timeout
     *            The amount of time in milliseconds to wait for a message.
     * @return The message received from the queue, or null if no message is
     *         received before the timeout is expired.
     */
    public ClientMessage getMessage(int timeout) {
        ClientMessage message = null;

        if (timeout < 0) {
            timeout = 0;
        }

        try {
            synchronized (consumerSession) {
                message = localConsumer.receive(timeout);
            }
        } catch (HornetQException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        return message;
    }
}
