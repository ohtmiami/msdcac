/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom;

import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomServer extends DicomConnector {

    private static Logger LOG = LoggerFactory.getLogger(DicomConnector.class);

    public DicomServer(ExecutorService executorService) {

        super(executorService);
    }

    @Override
    public void start() {

        if (this.bootstrap != null) {
            throw new IllegalStateException("Already started");
        }
        this.bootstrap = new ServerBootstrap(new NioServerSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        this.bootstrap.setPipelineFactory(new DicomServerPipelineFactory(this));
        this.bootstrap.setOption("child.tcpNoDelay", true);
        bindAll();
    }

    private void bindAll() {

        if (this.bootstrap == null) {
            throw new IllegalStateException("Not started");
        }
        ServerBootstrap serverBootstrap = (ServerBootstrap) this.bootstrap;
        Collection<NetworkApplicationEntity> localAEs = this.getLocalApplicationEntities();
        Set<InetSocketAddress> bindPoints = new HashSet<InetSocketAddress>(localAEs.size());
        for (NetworkApplicationEntity localAE : localAEs) {
            try {
                InetSocketAddress bindPoint = localAE.getAddress();
                if (bindPoints.add(bindPoint)) {
                    Channel serverChannel = serverBootstrap.bind(bindPoint);
                    this.getAllChannels().add(serverChannel);
                }
            } catch (Exception e) {
                LOG.error("Failed to start local AE " + localAE.getAETitle(), e);
                continue;
            }
        }
    }
}
