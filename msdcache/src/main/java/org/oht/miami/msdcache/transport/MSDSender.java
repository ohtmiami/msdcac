/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport;

import static org.oht.miami.msdcache.transport.util.QueueUtils.ACK_RECEIVE_QUEUE_ADDR;
import static org.oht.miami.msdcache.transport.util.QueueUtils.ACK_RECEIVE_QUEUE_NAME;
import static org.oht.miami.msdcache.transport.util.QueueUtils.MSD_SENDER_CONFIG;
import static org.oht.miami.msdcache.transport.util.QueueUtils.MSD_SENDER_HORNETQ_SERVER_ID;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_SEND_QUEUE_ADDR;
import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_SEND_QUEUE_NAME;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.core.config.impl.FileConfiguration;
import org.hornetq.core.remoting.impl.invm.InVMAcceptorFactory;
import org.hornetq.core.remoting.impl.netty.NettyAcceptorFactory;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.transport.queue.AckReceiveQueue;
import org.oht.miami.msdcache.transport.queue.StudySendQueue;
import org.oht.miami.msdcache.transport.queue.monitor.msdsender.MonitorAckReceiveQueue;
import org.oht.miami.msdcache.transport.queue.monitor.msdsender.MonitorStudySendQueue;
import org.oht.miami.msdcache.transport.study.StudySendTask;
import org.oht.miami.msdcache.transport.util.MSDReceiverAddress;
import org.oht.miami.msdcache.transport.util.QueueUtils;
import org.oht.miami.msdtk.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class which provides the public interface to the transport sending module.
 * Although this class does not enforce the Singleton pattern, it should never
 * be instantiated more than once per server.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class MSDSender extends MSDTransport {

    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(MSDSender.class);

    /**
     * The AckReceiveQueue, a persistent queue that receives acknowledgment
     * messages from a remote MSDReceiver.
     */
    private AckReceiveQueue ackReceiveQueue;

    /**
     * The StudySendQueue, a persistent queue containing the textual
     * representation of StudySendTask objects. The messages that are placed in
     * this queue represent the studies that have been successfully converted
     * into the MSD format and which then need to be forwarded to another Lite
     * Edge Cache.
     */
    private StudySendQueue studySendQueue;

    /**
     * The monitor responsible for taking action when a new message is received
     * by the AckReceiveQueue.
     */
    private MonitorAckReceiveQueue arqMonitor;

    /**
     * The Future<Void> object that is returned by submitting the
     * MonitorAckReceiveQueue object to the ExecutorService.
     */
    private Future<Void> arqMonitorThread;

    /**
     * The monitor responsible for taking action when a new message is received
     * by the StudySendQueue.
     */
    private MonitorStudySendQueue ssqMonitor;

    /**
     * The Future<Void> object that is returned by submitting the
     * MonitorStudySendQueue object to the ExecutorService.
     */
    private Future<Void> ssqMonitorThread;

    /**
     * A map containing the AE title and address of other known Lite Edge Caches
     * (specifically, the MSDReceiver of those caches).
     */
    private Map<String, MSDReceiverAddress> remoteNodes = new HashMap<String, MSDReceiverAddress>();

    /**
     * Constructor
     * 
     * @param store
     *            A reference to the cache's Store.
     * @param executorService
     *            The thread pool provided by the cache application.
     */
    public MSDSender(Store store, ExecutorService executorService) {
        if (store == null) {
            throw new IllegalArgumentException(
                    "The MSDSender cannot have a null reference to the Store.");
        } else if (executorService == null) {
            throw new IllegalArgumentException(
                    "The MSDSender cannot have a null reference to the ExecutorService (thread pool).");
        }

        this.store = store;
        this.executorService = executorService;
    }

    @Override
    public synchronized void start() {
        if (started) {
            LOG.info("The MSDSender is already started.");
            return;
        }

        try {
            // Initialize the FileConfiguration object so that it can be
            // populated with the necessary Acceptors and Connectors
            FileConfiguration config = new FileConfiguration();
            config.setConfigurationUrl(MSD_SENDER_CONFIG);

            // Setup Acceptors
            Set<TransportConfiguration> acceptorTransports = new HashSet<TransportConfiguration>();

            Map<String, Object> nettyAcceptorParams = new HashMap<String, Object>();
            nettyAcceptorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.PORT_PROP_NAME,
                    QueueUtils.MSD_SENDER_HORNETQ_PORT);
            nettyAcceptorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.HOST_PROP_NAME,
                    "0.0.0.0");
            acceptorTransports.add(new TransportConfiguration(NettyAcceptorFactory.class.getName(),
                    nettyAcceptorParams));

            Map<String, Object> invmAcceptorParams = new HashMap<String, Object>();
            invmAcceptorParams.put(
                    org.hornetq.core.remoting.impl.invm.TransportConstants.SERVER_ID_PROP_NAME,
                    QueueUtils.MSD_SENDER_HORNETQ_SERVER_ID);
            acceptorTransports.add(new TransportConfiguration(InVMAcceptorFactory.class.getName(),
                    invmAcceptorParams));

            config.setAcceptorConfigurations(acceptorTransports);

            // Setup Connectors
            Map<String, TransportConfiguration> connectorTransports = new HashMap<String, TransportConfiguration>();

            Map<String, Object> nettyConnectorParams = new HashMap<String, Object>();
            nettyConnectorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.PORT_PROP_NAME,
                    QueueUtils.MSD_SENDER_HORNETQ_PORT);
            nettyConnectorParams.put(
                    org.hornetq.core.remoting.impl.netty.TransportConstants.HOST_PROP_NAME,
                    "localhost");

            TransportConfiguration connectorConfig = new TransportConfiguration(
                    NettyConnectorFactory.class.getName(), nettyConnectorParams);
            connectorTransports.put(connectorConfig.getName(), connectorConfig);

            config.setConnectorConfigurations(connectorTransports);

            // Start the configuration
            config.start();

            // Instantiate the server, apply the configuration, and start it
            messageServer = new EmbeddedHornetQ();
            messageServer.setConfiguration(config);
            messageServer.start();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Setup the necessary queues
        ackReceiveQueue = new AckReceiveQueue(this, messageServer, MSD_SENDER_HORNETQ_SERVER_ID,
                ACK_RECEIVE_QUEUE_NAME, ACK_RECEIVE_QUEUE_ADDR);
        studySendQueue = new StudySendQueue(this, messageServer, MSD_SENDER_HORNETQ_SERVER_ID,
                STUDY_SEND_QUEUE_NAME, STUDY_SEND_QUEUE_ADDR);

        // Start the AckReceiveQueue monitor thread
        arqMonitor = new MonitorAckReceiveQueue(this, ackReceiveQueue, store, executorService);
        arqMonitorThread = executorService.submit(arqMonitor);

        // Start the StudySendQueue monitor thread
        ssqMonitor = new MonitorStudySendQueue(this, studySendQueue, store, executorService);
        ssqMonitorThread = executorService.submit(ssqMonitor);

        started = true;
    }

    @Override
    public synchronized void stop() {
        if (!started) {
            LOG.info("The MSDSender cannot be stopped because it has not been started yet.");
            return;
        }

        try {
            arqMonitor.stop();
            arqMonitorThread.cancel(false);

            ssqMonitor.stop();
            ssqMonitorThread.cancel(false);

            ackReceiveQueue.stop();
            studySendQueue.stop();
            messageServer.stop();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        started = false;
    }

    /**
     * This method receives a StudySendTask object and adds it to the internal
     * StudySendQueue, queuing it for processing by the MSDSender.
     * 
     * @param studySendTask
     *            The StudySendTask object which will be added to the queue.
     */
    public void sendStudy(StudySendTask studySendTask) {
        if (!started) {
            LOG.info("The MSDSender has not been started.");
            return;
        }

        studySendQueue.addTask(studySendTask);
    }

    /**
     * This method adds a new Lite Edge Cache to the list of known remote nodes.
     * 
     * @param aeTitle
     *            The AE Title of the remote node.
     * @param address
     *            The address of the remote node.
     */
    public void addRemoteNode(String aeTitle, MSDReceiverAddress address) {
        remoteNodes.put(aeTitle, address);
    }

    /**
     * This method retrieves the address of a remote node, looking it up by the
     * associated AE Title.
     * 
     * @param aeTitle
     *            The AE Title of the remote node to lookup.
     * @return The address of the remote node.
     */
    public MSDReceiverAddress lookupRemoteNode(String aeTitle) {
        return remoteNodes.get(aeTitle);
    }
}
