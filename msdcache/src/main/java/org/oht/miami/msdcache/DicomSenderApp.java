/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.oht.miami.msdcache.forward.DicomSender;
import org.oht.miami.msdtk.io.FileBasedStudyReader;
import org.oht.miami.msdtk.io.StudyReader;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomSenderApp {

    private static Logger LOG = LoggerFactory.getLogger(DicomSenderApp.class);

    public static void main(String[] args) throws IOException, InterruptedException {

        if (args.length < 2) {
            System.err.println("Usage: DicomSenderApp <aet>@<host>:<port> <studyIUID>");
        }

        String remoteAEArg = args[0];
        int atPos = remoteAEArg.indexOf('@');
        int colonPos = remoteAEArg.indexOf(':');
        String remoteAET = remoteAEArg.substring(0, atPos);
        String hostname = remoteAEArg.substring(atPos + 1, colonPos);
        int port = Integer.parseInt(remoteAEArg.substring(colonPos + 1));

        Store store = StoreFactory.createStore("msd-store.properties");
        File studyDir = new File(args[1]);
        LOG.info("Reading from {}...", studyDir);
        StudyReader studyReader = new FileBasedStudyReader(store, studyDir);
        Study study = studyReader.readStudy();
        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        LOG.info("Writing study {} to MSD-Store...", studyInstanceUID);
        store.writeVersion(study);

        final ExecutorService executorService = Executors.newCachedThreadPool();
        final DicomSender dicomSender = new DicomSender(executorService, store);
        String localAET = "DCMSND_NETTY";
        dicomSender.addLocalApplicationEntity(localAET);
        dicomSender.addRemoteApplicationEntity(remoteAET, hostname, port);

        dicomSender.start();
        LOG.info("Sending study {} to {}...", studyInstanceUID, remoteAET);
        try {
            dicomSender.sendStudy(remoteAET, localAET, studyInstanceUID);
        } finally {
            dicomSender.stop();
        }
    }
}
