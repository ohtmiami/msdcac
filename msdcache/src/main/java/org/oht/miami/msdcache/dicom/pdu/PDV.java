/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.dicom.pdu;

import org.jboss.netty.buffer.ChannelBuffer;

public class PDV {

    private final int pcid;

    private final int messageControlHeader;

    private final ChannelBuffer data;

    public PDV(int pcid, int messageControlHeader, ChannelBuffer data) {

        this.pcid = pcid;
        this.messageControlHeader = messageControlHeader;
        this.data = data;
    }

    public int getPCID() {

        return pcid;
    }

    public int getType() {

        return messageControlHeader & PDVType.COMMAND;
    }

    public boolean isCommand() {

        return getType() == PDVType.COMMAND;
    }

    public boolean isDataSet() {

        return getType() == PDVType.DATA;
    }

    public boolean isLast() {

        return (messageControlHeader & PDVType.LAST) != 0;
    }

    public ChannelBuffer getData() {

        return data;
    }
}
