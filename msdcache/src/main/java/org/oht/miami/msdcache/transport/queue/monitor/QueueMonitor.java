/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue.monitor;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.queue.QueueManager;
import org.oht.miami.msdtk.store.Store;

/**
 * An abstract parent class used to encapsulate any one of the various queue
 * monitors that are used by the transportation module. A queue monitor is used
 * to monitor a queue and take action when a message is received into that
 * queue. This class extracts away the code that is common to the various queue
 * monitors.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public abstract class QueueMonitor implements Callable<Void> {

    /**
     * A reference to the parent component (MSDSender or MSDReceiver).
     */
    protected final MSDTransport context;

    /**
     * The queue which this QueueMonitor will monitor.
     */
    protected final QueueManager queue;

    /**
     * A reference to the Store.
     */
    protected final Store store;

    /**
     * A reference to the application-wide thread pool.
     */
    protected final ExecutorService executorService;

    /**
     * The amount of time to wait for a message to be received from the queue
     * which is being monitored, before otherwise returning null.
     */
    protected int messageReceiveTimeout = 10000;

    /**
     * A boolean specifying whether or not this object has already been started
     * (true upon instantiation).
     */
    private boolean running = true;

    /**
     * Constructor
     * 
     * @param context
     *            The parent component (MSDSender or MSDReceiver).
     * @param queue
     *            The queue which this QueueMonitor will monitor.
     * @param store
     *            The Store used by the application.
     * @param executorService
     *            The thread pool used by the application.
     */
    protected QueueMonitor(MSDTransport context, QueueManager queue, Store store,
            ExecutorService executorService) {
        this.context = context;
        this.queue = queue;
        this.store = store;
        this.executorService = executorService;
    }

    /**
     * Returns the "running" state of this QueueMonitor.
     * 
     * @return The "running" state of this QueueMonitor.
     */
    public synchronized boolean isRunning() {
        return running;
    }

    /**
     * Sets the "running" state of this QueueMonitor to true.
     */
    public synchronized void start() {
        running = true;
    }

    /**
     * Sets the "running" state of this QueueMonitor to false.
     */
    public synchronized void stop() {
        running = false;
    }
}
