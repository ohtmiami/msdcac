/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue;

import java.util.HashMap;

import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.SimpleString;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.core.client.ClientConsumer;
import org.hornetq.api.core.client.ClientProducer;
import org.hornetq.api.core.client.ClientSession;
import org.hornetq.api.core.client.ClientSessionFactory;
import org.hornetq.api.core.client.HornetQClient;
import org.hornetq.api.core.client.ServerLocator;
import org.hornetq.core.remoting.impl.invm.InVMConnectorFactory;
import org.hornetq.core.remoting.impl.invm.TransportConstants;
import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.util.QueueUtils;

/**
 * An abstract parent class used to encapsulate any one of the various HornetQ
 * queues that are used by the transportation module. This class extracts away
 * the tedious creation and message management code common to and required by
 * every queue.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public abstract class QueueManager {

    /**
     * A reference to the parent component (MSDSender or MSDReceiver).
     */
    protected final MSDTransport context;

    /**
     * A reference to the HornetQ server instance on which this queue will be
     * deployed. This is provided by the parent component.
     */
    protected final EmbeddedHornetQ messageServer;

    /**
     * A reference to the server ID, used to distinguish the two instances of
     * HornetQ (MSDSender and MSDReceiver), and defined in the QueueUtils class.
     */
    protected final int serverID;

    /**
     * The unique name of this queue, defined in the QueueUtils class.
     */
    protected final String queueName;

    /**
     * The unique address of this queue, defined in the QueueUtils class.
     */
    protected final String queueAddress;

    /**
     * The ServerLocator object, used to create a connection to an instances of
     * HornetQ and to instantiate a ClientSessionFactory.
     */
    private final ServerLocator serverLocator;

    /**
     * The ClientSessionFactory object, used to instantiate a ClientSession.
     */
    private final ClientSessionFactory sessionFactory;

    /**
     * A ClientSession object, used to instantiate new messages, as well as a
     * ClientProducer.
     */
    protected final ClientSession producerSession;

    /**
     * A ClientSession object, used to instantiate a ClientConsumer.
     */
    protected final ClientSession consumerSession;

    /**
     * The ClientProducer object, used to produce messages and add them to this
     * queue.
     */
    protected final ClientProducer localProducer;

    /**
     * The ClientConsumer object, used to consume messages from this queue.
     */
    protected final ClientConsumer localConsumer;

    /**
     * Constructor
     * 
     * @param context
     *            The MSDSender or MSDReceiver that is instantiating this queue.
     * @param messageServer
     *            The HornetQ server instance on which this queue will be
     *            deployed.
     * @param serverID
     *            The ID of the HornetQ server.
     * @param queueName
     *            The unique name of this queue.
     * @param queueAddress
     *            The unique address of this queue.
     */
    public QueueManager(MSDTransport context, EmbeddedHornetQ messageServer, int serverID,
            String queueName, String queueAddress) {
        if (context != null) {
            this.context = context;
        } else {
            throw new IllegalArgumentException(
                    "A non-null reference to an MSDTransport (MSDSender or MSDReceiver) is required.");
        }

        if (messageServer != null) {
            this.messageServer = messageServer;
        } else {
            throw new IllegalArgumentException(
                    "A non-null reference to an EmbeddedHornetQ server is required.");
        }

        this.serverID = serverID;

        if (queueName != null && !queueName.trim().isEmpty()) {
            this.queueName = queueName;
        } else {
            throw new IllegalArgumentException(
                    "The argument for 'queueName' cannot be null or empty.");
        }

        if (queueAddress != null && !queueAddress.trim().isEmpty()) {
            this.queueAddress = queueAddress;
        } else {
            throw new IllegalArgumentException(
                    "The argument for 'queueAddress' cannot be null or empty.");
        }

        // Deploy this queue on the HornetQ server instance
        try {
            SimpleString ssQueueAddress = new SimpleString(queueAddress);
            SimpleString ssQueueName = new SimpleString(queueName);
            SimpleString ssFilter = null;

            messageServer.getHornetQServer().deployQueue(ssQueueAddress, ssQueueName, ssFilter,
                    true, false);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Instantiate a ServerLocator object (effectively creating a connection
        // to this queue).
        HashMap<String, Object> connectionParams = new HashMap<String, Object>();
        connectionParams.put(TransportConstants.SERVER_ID_PROP_NAME, serverID);
        TransportConfiguration transportConfig = new TransportConfiguration(
                InVMConnectorFactory.class.getName(), connectionParams);
        serverLocator = HornetQClient.createServerLocatorWithoutHA(transportConfig);

        // Create the local ClientConsumer and ClientProducer objects, used to
        // manipulate the items in this queue.
        try {
            // First, create a session factory
            sessionFactory = serverLocator.createSessionFactory();

            // Create (and start) the ClientConsumer
            consumerSession = sessionFactory.createSession(true, true, QueueUtils.ACK_BATCH_SIZE);
            localConsumer = consumerSession.createConsumer(queueName);
            /*
             * The session must be started before ClientConsumer objects created
             * by the session can consume messages from the queue. This is not
             * necessary for the session used to create ClientProducer objects.
             */
            consumerSession.start();

            // Create the ClientProducer
            producerSession = sessionFactory.createSession(true, true, QueueUtils.ACK_BATCH_SIZE);
            localProducer = producerSession.createProducer(queueAddress);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
            throw new RuntimeException(); // This is a temporary exception.
                                          // Replace it once explicit exceptions
                                          // have been created.
        }
    }

    /**
     * Retrieve the name of this queue.
     * 
     * @return The name of this queue.
     */
    public String getQueueName() {
        return queueName;
    }

    /**
     * Retrieve the address of this queue.
     * 
     * @return The address of this queue.
     */
    public String getQueueAddress() {
        return queueAddress;
    }

    /**
     * This method specifies the shut-down behavior of a queue. This method
     * should be called when the parent component (MSDSender or MSDReceiver) is
     * being shut-down.
     */
    public void stop() {
        try {
            localProducer.close();
            localConsumer.close();
            producerSession.close();
            consumerSession.close();
        } catch (HornetQException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        sessionFactory.close();
        serverLocator.close();
    }
}
