/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue;

import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.Message;
import org.hornetq.api.core.client.ClientMessage;
import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.study.StudySendTask;

/**
 * A persistent queue containing the textual representation of StudySendTask
 * objects.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class StudySendQueue extends QueueManager {
    /**
     * Constructor
     * 
     * @param context
     *            The MSDSender or MSDReceiver that is instantiating this queue.
     * @param messageServer
     *            The HornetQ server instance on which this queue will be
     *            deployed.
     * @param serverID
     *            The ID of the HornetQ server.
     * @param queueName
     *            The unique name of this queue.
     * @param queueAddress
     *            The unique address of this queue.
     */
    public StudySendQueue(MSDTransport context, EmbeddedHornetQ messageServer, int serverID,
            String queueName, String queueAddress) {
        super(context, messageServer, serverID, queueName, queueAddress);
    }

    /**
     * This method creates a TextMessage from the provided StudySendTask object
     * and adds it to the embedded queue.
     * 
     * @param studySendTask
     *            The StudySendTask which will be added to the queue.
     */
    public void addTask(StudySendTask studySendTask) {
        // This method must create a lock on the producerSession object, since
        // that object (and messages created from it) cannot be manipulated
        // concurrently by different threads. This is a HornetQ limitation.
        synchronized (producerSession) {
            ClientMessage message = producerSession.createMessage(Message.TEXT_TYPE, true);
            message.getBodyBuffer().writeString(studySendTask.serialize());

            try {
                localProducer.send(message);
            } catch (HornetQException e) {
                // TODO handle exception cases, call GWL
                e.printStackTrace();
            }
        }
    }

    /**
     * This method receives a message from this queue, blocking the thread while
     * waiting for a message or for the specified timeout to pass. If a message
     * is received, the contents of that message are de-serialized and the
     * resulting StudySendTask is returned.
     * 
     * @param timeout
     *            The amount of time in milliseconds to wait for a message.
     * @return The StudySendTask created from the message received from the
     *         queue, or null if no message is received before the timeout is
     *         expired.
     */
    public StudySendTask receiveTask(int timeout) {
        StudySendTask task = null;

        if (timeout < 0) {
            timeout = 0;
        }

        try {
            ClientMessage message;

            synchronized (consumerSession) {
                message = localConsumer.receive(timeout);

                if (message == null) {
                    return null;
                }

                String studySendTaskString = message.getBodyBuffer().readString();
                task = StudySendTask.deserialize(studySendTaskString);
            }
        } catch (HornetQException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        return task;
    }
}
