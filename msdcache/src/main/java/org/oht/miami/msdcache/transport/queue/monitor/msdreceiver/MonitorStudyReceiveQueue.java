/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue.monitor.msdreceiver;

import java.util.concurrent.ExecutorService;

import org.hornetq.api.core.client.ClientMessage;
import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.queue.StudyReceiveQueue;
import org.oht.miami.msdcache.transport.queue.monitor.QueueMonitor;
import org.oht.miami.msdcache.transport.study.StudyReceiveManager;
import org.oht.miami.msdcache.transport.util.MessagePropertyType;
import org.oht.miami.msdtk.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class defines the job performed by the StudyReceiveQueue monitor thread.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class MonitorStudyReceiveQueue extends QueueMonitor {

    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(MonitorStudyReceiveQueue.class);

    /**
     * Constructor
     * 
     * @param context
     *            The parent component (MSDSender or MSDReceiver).
     * @param queue
     *            The queue which this QueueMonitor will monitor.
     * @param store
     *            The Store used by the application.
     * @param executorService
     *            The thread pool used by the application.
     */
    public MonitorStudyReceiveQueue(MSDTransport context, StudyReceiveQueue queue, Store store,
            ExecutorService executorService) {
        super(context, queue, store, executorService);
    }

    @Override
    public Void call() {
        while (isRunning()) {
            StudyReceiveQueue studyRecieveQueue = (StudyReceiveQueue) queue;
            final ClientMessage message = studyRecieveQueue.getMessage(messageReceiveTimeout);

            // If a message was found in the queue, process it
            if (message != null) {
                processMessage(message);
            }
        }

        LOG.info("This QueueMonitor is no longer running.");
        return null;
    }

    /**
     * Processes the received message by looking up the associated
     * StudyReceiveManager object and handing the message off to it.
     * 
     * @param message
     *            The received message which will be processed by the
     *            StudyReceiveManager.
     */
    private void processMessage(ClientMessage message) {
        // Get the group ID from the message header
        String groupID = message.getStringProperty(MessagePropertyType.GROUP_ID.getValue());

        // Use the group ID to lookup the appropriate StudyReceiveManager for
        // this message group
        LOG.info("Looking up the StudyReceiveManager for the group ID: " + groupID);
        StudyReceiveManager manager = StudyReceiveManager.getInstance(context, groupID, store);

        // Process the message
        manager.processMessage(message);
    }
}
