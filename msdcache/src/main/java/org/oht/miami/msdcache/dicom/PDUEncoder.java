/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

import org.dcm4che2.net.pdu.AAbort;
import org.dcm4che2.net.pdu.AAssociateAC;
import org.dcm4che2.net.pdu.AAssociateRJ;
import org.dcm4che2.net.pdu.AAssociateRQ;
import org.dcm4che2.net.pdu.AAssociateRQAC;
import org.dcm4che2.net.pdu.CommonExtendedNegotiation;
import org.dcm4che2.net.pdu.ExtendedNegotiation;
import org.dcm4che2.net.pdu.PresentationContext;
import org.dcm4che2.net.pdu.RoleSelection;
import org.dcm4che2.net.pdu.UserIdentityAC;
import org.dcm4che2.net.pdu.UserIdentityRQ;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRP;
import org.oht.miami.msdcache.dicom.pdu.AReleaseRQ;
import org.oht.miami.msdcache.dicom.pdu.ItemType;
import org.oht.miami.msdcache.dicom.pdu.PDUType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDUEncoder extends OneToOneEncoder {

    private static Logger LOG = LoggerFactory.getLogger(PDUEncoder.class);

    private Association association = null;

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg)
            throws Exception {

        association = AssociationHandler.associations.get(channel);
        if (msg instanceof AAssociateRJ) {
            AAssociateRJ rj = (AAssociateRJ) msg;
            LOG.info("{} << {}", association, rj.getMessage());
            return encodeFixedLengthPDU(PDUType.A_ASSOCIATE_RJ, rj.getResult(), rj.getSource(),
                    rj.getReason());
        } else if (msg instanceof AReleaseRQ) {
            LOG.info("{} << A-RELEASE-RQ", association);
            return encodeFixedLengthPDU(PDUType.A_RELEASE_RQ, 0, 0, 0);
        } else if (msg instanceof AReleaseRP) {
            LOG.info("{} << A-RELEASE-RP", association);
            return encodeFixedLengthPDU(PDUType.A_RELEASE_RP, 0, 0, 0);
        } else if (msg instanceof AAbort) {
            AAbort aa = (AAbort) msg;
            LOG.info("{} << {}", association, aa.getMessage());
            return encodeFixedLengthPDU(PDUType.A_ABORT, 0, aa.getSource(), aa.getReason());
        } else if (msg instanceof AAssociateRQ) {
            AAssociateRQ rq = (AAssociateRQ) msg;
            LOG.info("{}: A-ASSOCIATE-RQ {} << {}", new Object[] { association, rq.getCalledAET(),
                            rq.getCallingAET() });
            return encodeAssociateRQAC(rq, PDUType.A_ASSOCIATE_RQ, ItemType.RQ_PRES_CONTEXT);
        } else if (msg instanceof AAssociateAC) {
            AAssociateAC ac = (AAssociateAC) msg;
            LOG.info("{}: A-ASSOCIATE-AC {} << {}", new Object[] { association, ac.getCalledAET(),
                            ac.getCallingAET() });
            return encodeAssociateRQAC(ac, PDUType.A_ASSOCIATE_AC, ItemType.AC_PRES_CONTEXT);
        } else {
            // Return the original message, so that it will be processed by the
            // next downstream handler
            return msg;
        }
    }

    private ChannelBuffer encodeFixedLengthPDU(int pduType, int result, int source, int reason) {

        ChannelBuffer buffer = ChannelBuffers.buffer(10);
        // PDU Type (1B)
        buffer.writeByte(pduType);
        // Reserved (1B)
        buffer.writeByte(0);
        // PDU Length (4B)
        buffer.writeInt(4);
        // Reserved (1B)
        buffer.writeByte(0);
        // Result (1B)
        buffer.writeByte(result);
        // Source (1B)
        buffer.writeByte(source);
        // Reason (1B)
        buffer.writeByte(reason);
        return buffer;
    }

    private ChannelBuffer encodeAssociateRQAC(AAssociateRQAC rqac, int pduType, int pcItemType) {

        int pduLength = rqac.length();
        ChannelBuffer buffer = ChannelBuffers.buffer(6 + pduLength);
        // PDU Type (1B)
        buffer.writeByte(pduType);
        // Reserved (1B)
        buffer.writeByte(0);
        // PDU Length (4B)
        buffer.writeInt(pduLength);
        // Protocol Version (2B)
        buffer.writeShort(rqac.getProtocolVersion());
        // Reserved (2B)
        buffer.writeShort(0);
        // Called Entity Title (16B)
        encodeAET(buffer, rqac.getCalledAET());
        // Calling Entity Title (16B)
        encodeAET(buffer, rqac.getCallingAET());
        // Reserved (32B)
        buffer.writeZero(32);
        // Application Context Item
        encodeStringItem(buffer, ItemType.APP_CONTEXT, rqac.getApplicationContext());
        // Presentation Context Items
        encodePCs(buffer, pcItemType, rqac.getPresentationContexts());
        encodeUserInfo(buffer, rqac);
        return buffer;
    }

    private void writeASCIIString(ChannelBuffer buffer, String s) {

        byte[] bytes = null;
        try {
            bytes = s.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("US-ASCII charset is not available", e);
        }
        buffer.writeBytes(bytes);
    }

    private void encodeAET(ChannelBuffer buffer, String aet) {

        int aetLength = aet.length();
        if (aetLength > 16) {
            throw new IllegalArgumentException("Length of AE Title \"" + aet
                    + "\" exceeds 16 characters");
        }
        writeASCIIString(buffer, aet);
        for (int i = aetLength; i < 16; i++) {
            buffer.writeByte(0x20);
        }
    }

    private void encodeASCIIString(ChannelBuffer buffer, String s) {

        buffer.writeShort(s.length());
        writeASCIIString(buffer, s);
    }

    private void encodeItemHeader(ChannelBuffer buffer, int type, int length) {

        // Item Type (1B)
        buffer.writeByte(type);
        // Reserved (1B)
        buffer.writeByte(0);
        // Item Length (2B)
        buffer.writeShort(length);
    }

    private void encodeStringItem(ChannelBuffer buffer, int type, String s) {

        if (s == null) {
            return;
        }
        encodeItemHeader(buffer, type, s.length());
        writeASCIIString(buffer, s);
    }

    private void encodePCs(ChannelBuffer buffer, int pcItemType, Collection<PresentationContext> pcs) {

        for (PresentationContext pc : pcs) {
            encodeItemHeader(buffer, pcItemType, pc.length());
            // Presentation Context ID (1B)
            buffer.writeByte(pc.getPCID());
            // Reserved (1B)
            buffer.writeByte(0);
            // Result/Reason (1B)
            buffer.writeByte(pc.getResult());
            // Reserved (1B)
            buffer.writeByte(0);
            encodeStringItem(buffer, ItemType.ABSTRACT_SYNTAX, pc.getAbstractSyntax());
            for (String tsuid : pc.getTransferSyntaxes()) {
                encodeStringItem(buffer, ItemType.TRANSFER_SYNTAX, tsuid);
            }
        }
    }

    private void encodeUserInfo(ChannelBuffer buffer, AAssociateRQAC rqac) {

        encodeItemHeader(buffer, ItemType.USER_INFO, rqac.userInfoLength());
        encodeMaxPDULength(buffer, rqac.getMaxPDULength());
        encodeStringItem(buffer, ItemType.IMPL_CLASS_UID, rqac.getImplClassUID());
        if (rqac.isAsyncOps()) {
            encodeAsyncOpsWindow(buffer, rqac);
        }
        for (RoleSelection rs : rqac.getRoleSelections()) {
            encodeRoleSelection(buffer, rs);
        }
        encodeStringItem(buffer, ItemType.IMPL_VERSION_NAME, rqac.getImplVersionName());
        for (ExtendedNegotiation en : rqac.getExtendedNegotiations()) {
            encodeExtendedNegotiation(buffer, en);
        }
        for (CommonExtendedNegotiation cen : rqac.getCommonExtendedNegotiations()) {
            encodeCommonExtendedNegotiation(buffer, cen);
        }
        if (rqac instanceof AAssociateRQ) {
            encodeUserIdentityRQ(buffer, ((AAssociateRQ) rqac).getUserIdentity());
        } else {
            encodeUserIdentityAC(buffer, ((AAssociateAC) rqac).getUserIdentity());
        }
    }

    private void encodeRoleSelection(ChannelBuffer buffer, RoleSelection selection) {

        encodeItemHeader(buffer, ItemType.ROLE_SELECTION, selection.length());
        encodeASCIIString(buffer, selection.getSOPClassUID());
        buffer.writeByte(selection.isSCU() ? 1 : 0);
        buffer.writeByte(selection.isSCP() ? 1 : 0);
    }

    private void encodeExtendedNegotiation(ChannelBuffer buffer, ExtendedNegotiation extNeg) {

        encodeItemHeader(buffer, ItemType.EXT_NEG, extNeg.length());
        encodeASCIIString(buffer, extNeg.getSOPClassUID());
        buffer.writeBytes(extNeg.getInformation());
    }

    private void encodeCommonExtendedNegotiation(ChannelBuffer buffer,
            CommonExtendedNegotiation extNeg) {
        encodeItemHeader(buffer, ItemType.COMMON_EXT_NEG, extNeg.length());
        encodeASCIIString(buffer, extNeg.getSOPClassUID());
        encodeASCIIString(buffer, extNeg.getServiceClassUID());
        for (String cuid : extNeg.getRelatedGeneralSOPClassUIDs()) {
            encodeASCIIString(buffer, cuid);
        }
    }

    private void encodeAsyncOpsWindow(ChannelBuffer buffer, AAssociateRQAC rqac) {

        encodeItemHeader(buffer, ItemType.ASYNC_OPS_WINDOW, 4);
        buffer.writeShort(rqac.getMaxOpsInvoked());
        buffer.writeShort(rqac.getMaxOpsPerformed());
    }

    private void encodeMaxPDULength(ChannelBuffer buffer, int maxPDULength) {

        encodeItemHeader(buffer, ItemType.MAX_PDU_LENGTH, 4);
        // Maximum Length Received (4B)
        buffer.writeInt(maxPDULength);
    }

    private void encodeUserIdentityRQ(ChannelBuffer buffer, UserIdentityRQ userIdentity) {

        if (userIdentity == null) {
            return;
        }
        encodeItemHeader(buffer, ItemType.RQ_USER_IDENTITY, userIdentity.length());
        buffer.writeByte(userIdentity.getUserIdentityType());
        buffer.writeByte(userIdentity.isPositiveResponseRequested() ? 1 : 0);
        encodeBytes(buffer, userIdentity.getPrimaryField());
        encodeBytes(buffer, userIdentity.getSecondaryField());
    }

    private void encodeUserIdentityAC(ChannelBuffer buffer, UserIdentityAC userIdentity) {

        if (userIdentity == null) {
            return;
        }
        encodeItemHeader(buffer, ItemType.RQ_USER_IDENTITY, userIdentity.length());
        encodeBytes(buffer, userIdentity.getServerResponse());
    }

    private void encodeBytes(ChannelBuffer buffer, byte[] bytes) {

        buffer.writeShort(bytes.length);
        buffer.writeBytes(bytes);
    }
}
