/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue.sender;

import static org.oht.miami.msdcache.transport.util.QueueUtils.STUDY_RECEIVE_QUEUE_ADDR;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.hornetq.api.core.HornetQException;
import org.hornetq.api.core.Message;
import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.api.core.client.ClientMessage;
import org.hornetq.api.core.client.ClientProducer;
import org.hornetq.api.core.client.ClientSession;
import org.hornetq.api.core.client.ClientSessionFactory;
import org.hornetq.api.core.client.HornetQClient;
import org.hornetq.api.core.client.ServerLocator;
import org.hornetq.core.remoting.impl.netty.NettyConnectorFactory;
import org.hornetq.core.remoting.impl.netty.TransportConstants;
import org.oht.miami.msdcache.transport.MSDSender;
import org.oht.miami.msdcache.transport.MSDTransport;
import org.oht.miami.msdcache.transport.study.StudySendTask;
import org.oht.miami.msdcache.transport.util.MSDReceiverAddress;
import org.oht.miami.msdcache.transport.util.MessagePropertyType;
import org.oht.miami.msdcache.transport.util.MessageType;
import org.oht.miami.msdcache.transport.util.QueueUtils;
import org.oht.miami.msdtk.store.BulkDataInfo;
import org.oht.miami.msdtk.store.BulkDataSet;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.TransportableStudyEntry;
import org.oht.miami.msdtk.store.VersionInfo;
import org.oht.miami.msdtk.util.DicomUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains the functionality needed to convert a StudySendTask into
 * the various required messages, to connect to the receiving cache, and to send
 * those messages to the receiving cache.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class StudySender {
    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(StudySender.class);

    /**
     * Converts the provided StudySendTask into the various required messages,
     * connects to the receiving cache, and sends those messages to the
     * receiving cache.
     * 
     * @param task
     *            The StudySendTask which will be processed.
     * @param context
     *            The parent component (MSDSender or MSDReceiver).
     * @param store
     *            The Store used by the application.
     */
    public static void sendStudy(StudySendTask task, MSDTransport context, Store store) {
        // Get the StudyInstanceUID, source, and destination AE Title from the
        // StudySendTask
        DicomUID studyInstanceUID = task.getStudyInstanceUID();
        String sourceAET = task.getSourceAETitle();
        String destinationAET = task.getDesintationAETitle();

        // Generate a unique group ID for this group of messages (all of the
        // messages required to transmit this particular study)
        UUID groupID = UUID.randomUUID();

        // Get the TransportableStudyEntry from the Store
        TransportableStudyEntry transportableStudyEntry = store
                .getTransportableStudyEntry(studyInstanceUID);

        if (transportableStudyEntry == null) {
            // TODO handle exception cases, call GWL
            LOG.error("Error: received a null TransportableStudyEntry");
            throw new RuntimeException(); // This is a temporary exception.
                                          // Replace it once explicit exceptions
                                          // have been created.
        }

        // Create the lists which will hold the UUIDs of the files that will
        // need to be sent
        List<UUID> bulkDataUUIDs = new ArrayList<UUID>();
        List<UUID> versionUUIDs = new ArrayList<UUID>();

        // Get the required bulk data file UUIDs and add them to the list
        for (BulkDataSet bulkDataSet : transportableStudyEntry.getBulkDataSetHistory()) {
            for (BulkDataInfo bulkDataInfo : bulkDataSet) {
                bulkDataUUIDs.add(bulkDataInfo.getUUID());
            }
        }

        // Get the required version file UUIDs and add them to the list
        for (VersionInfo versionInfo : transportableStudyEntry.getVersionHistory()) {
            versionUUIDs.add(versionInfo.getUUID());
        }

        // Lookup the destination address using the destination AE title
        MSDReceiverAddress destinationAddress = ((MSDSender) context)
                .lookupRemoteNode(destinationAET);

        if (destinationAddress == null) {
            // TODO handle exception cases, call GWL
            LOG.error("Destination AE Title not found: " + destinationAET);
            throw new RuntimeException(); // This is a temporary exception.
                                          // Replace it once explicit exceptions
                                          // have been created.
        }

        // Instantiate a ServerLocator object (effectively creating a connection
        // to the remote queue specified by the MSDReceiverAddress object).
        HashMap<String, Object> connectionParams = new HashMap<String, Object>();
        connectionParams.put(TransportConstants.HOST_PROP_NAME, destinationAddress.getHostname());
        connectionParams.put(TransportConstants.PORT_PROP_NAME, destinationAddress.getPort());
        TransportConfiguration transportConfig = new TransportConfiguration(
                NettyConnectorFactory.class.getName(), connectionParams);
        ServerLocator serverLocator = HornetQClient.createServerLocatorWithoutHA(transportConfig);

        // Create the ClientSession and ClientProducer objects, used to send
        // messages to the remote queue.
        ClientSessionFactory sessionFactory = null;
        ClientSession producerSession = null;
        ClientProducer producer = null;
        try {
            sessionFactory = serverLocator.createSessionFactory();
            producerSession = sessionFactory.createSession(true, true, QueueUtils.ACK_BATCH_SIZE);
            producer = producerSession.createProducer(STUDY_RECEIVE_QUEUE_ADDR);
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // Each group of messages will contain a single "study entry message"
        // which contains the serialized TransportableStudyEntry object. The
        // following code creates that message and populates the header with the
        // required properties.
        ClientMessage studyEntryMessage = createStudyEntryMessage(producerSession,
                transportableStudyEntry);
        studyEntryMessage.putStringProperty(MessagePropertyType.MESSAGE_TYPE.getValue(),
                MessageType.STUDY_ENTRY.getValue());
        studyEntryMessage.putStringProperty(MessagePropertyType.GROUP_ID.getValue(),
                groupID.toString());
        studyEntryMessage.putStringProperty(MessagePropertyType.STUDY_INSTANCE_UID.getValue(),
                studyInstanceUID.toString());
        studyEntryMessage.putStringProperty(MessagePropertyType.SOURCE_AETITLE.getValue(),
                sourceAET);
        studyEntryMessage.putStringProperty(MessagePropertyType.DESTINATION_AETITLE.getValue(),
                destinationAET);

        // Send the study entry message.
        try {
            LOG.info("Sending StudyEntry message... ");
            producer.send(studyEntryMessage);
            LOG.info("sent!");
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        // If there are any Bulk Data files that need to be transmitted, get the
        // UUID from the list, retrieve the FileInputStream from the store using
        // the UUID, and create and send a message that includes the appropriate
        // file.
        if (!bulkDataUUIDs.isEmpty()) {
            for (UUID uuid : bulkDataUUIDs) {
                ClientMessage message = producerSession.createMessage(Message.STREAM_TYPE, true);

                try {
                    // Get the file size of the Bulk Data file from the store.
                    long fileSize = store.getBulkDataFileSize(uuid, studyInstanceUID);

                    // Populate the Bulk Data file message header with the
                    // required properties.
                    message.setBodyInputStream(store.getBulkDataInputStream(uuid, studyInstanceUID));
                    message.putStringProperty(MessagePropertyType.MESSAGE_TYPE.getValue(),
                            MessageType.BULK_DATA.getValue());
                    message.putStringProperty(MessagePropertyType.GROUP_ID.getValue(),
                            groupID.toString());
                    message.putStringProperty(MessagePropertyType.STUDY_INSTANCE_UID.getValue(),
                            studyInstanceUID.toString());
                    message.putStringProperty(MessagePropertyType.UUID.getValue(), uuid.toString());
                    message.putLongProperty(MessagePropertyType.FILE_SIZE.getValue(), fileSize);

                    // Send the Bulk Data file message.
                    LOG.info("Sending BD message... ");
                    producer.send(message);
                    LOG.info("sent!");
                } catch (IOException e) {
                    // TODO handle exception cases, call GWL
                    e.printStackTrace();
                } catch (HornetQException e) {
                    // TODO handle exception cases, call GWL
                    e.printStackTrace();
                }
            }
        }

        // If there are any Version files that need to be transmitted, get the
        // UUID from the list, retrieve the FileInputStream from the store using
        // the UUID, and create and send a message that includes the appropriate
        // file.
        if (!versionUUIDs.isEmpty()) {
            for (UUID uuid : versionUUIDs) {
                ClientMessage message = producerSession.createMessage(Message.STREAM_TYPE, true);

                try {
                    // Get the file size of the Version file from the store.
                    long fileSize = store.getVersionFileSize(uuid, studyInstanceUID);

                    // Populate the Version file message header with the
                    // required properties.
                    message.setBodyInputStream(store.getVersionInputStream(uuid, studyInstanceUID));
                    message.putStringProperty(MessagePropertyType.MESSAGE_TYPE.getValue(),
                            MessageType.VERSION.getValue());
                    message.putStringProperty(MessagePropertyType.GROUP_ID.getValue(),
                            groupID.toString());
                    message.putStringProperty(MessagePropertyType.STUDY_INSTANCE_UID.getValue(),
                            studyInstanceUID.toString());
                    message.putStringProperty(MessagePropertyType.UUID.getValue(), uuid.toString());
                    message.putLongProperty(MessagePropertyType.FILE_SIZE.getValue(), fileSize);

                    // Send the Version file message.
                    LOG.info("Sending Version message... ");
                    producer.send(message);
                    LOG.info("sent!");
                } catch (IOException e) {
                    // TODO handle exception cases, call GWL
                    e.printStackTrace();
                } catch (HornetQException e) {
                    // TODO handle exception cases, call GWL
                    e.printStackTrace();
                }
            }
        }

        try {
            producer.close();
            producerSession.close();
            sessionFactory.close();
        } catch (Exception e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            serverLocator.close();
        }
    }

    /**
     * Serializes the provided TransportableStudyEntry, creates a new
     * ClientMessage, and writes the serialized study entry to the body of the
     * new message.
     * 
     * @param producerSession
     *            The ClientSession used to create messages to the remote queue.
     * @param transportableStudyEntry
     *            The TransportableStudyEntry which will be serialized and added
     *            to the body of a new ClientMessage.
     * @return The newly created ClientMessage, containing the serialized study
     *         entry.
     */
    private static ClientMessage createStudyEntryMessage(ClientSession producerSession,
            TransportableStudyEntry transportableStudyEntry) {
        ClientMessage message;

        message = producerSession.createMessage(Message.OBJECT_TYPE, true);

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(bos);) {
            oos.writeObject(transportableStudyEntry);
            message.getBodyBuffer().writeBytes(bos.toByteArray());
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }

        return message;
    }
}
