/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.dicom;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.net.CommandUtils;
import org.dcm4che2.net.DicomServiceException;
import org.dcm4che2.net.Status;
import org.dcm4che2.util.CloseUtils;
import org.oht.miami.msdcache.dicom.service.CEchoSCP;
import org.oht.miami.msdcache.dicom.service.CStoreSCP;
import org.oht.miami.msdcache.dicom.service.DicomService;

class DicomServiceRegistry {

    private final HashSet<String> sopCUIDs = new HashSet<String>();

    private final HashMap<String, DicomService> cstoreSCPs = new HashMap<String, DicomService>();

    private final HashMap<String, DicomService> cechoSCPs = new HashMap<String, DicomService>(1);

    private void registerWith(HashMap<String, DicomService> registry, DicomService service) {

        final String[] sopClasses = service.getSOPClasses();
        for (int i = 0; i < sopClasses.length; i++) {
            registry.put(sopClasses[i], service);
            sopCUIDs.add(sopClasses[i]);
        }
        final String serviceClass = service.getServiceClass();
        if (serviceClass != null) {
            registry.put(serviceClass, service);
            sopCUIDs.add(serviceClass);
        }
    }

    private void deregisterFrom(HashMap<String, DicomService> registry, DicomService service) {

        for (Iterator<Map.Entry<String, DicomService>> iter = registry.entrySet().iterator(); iter
                .hasNext();) {
            Map.Entry<String, DicomService> element = iter.next();
            if (element.getValue() == service) {
                iter.remove();
            }
        }
    }

    public void register(DicomService service) {

        if (service instanceof CStoreSCP) {
            registerWith(cstoreSCPs, service);
        }
        if (service instanceof CEchoSCP) {
            registerWith(cechoSCPs, service);
        }
    }

    public void deregister(DicomService service) {

        if (service instanceof CStoreSCP) {
            deregisterFrom(cstoreSCPs, service);
        }
        if (service instanceof CEchoSCP) {
            deregisterFrom(cechoSCPs, service);
        }
    }

    private Object getFrom(HashMap<String, DicomService> registry, DicomObject command, int tag)
            throws DicomServiceException {

        String cuid = command.getString(tag);
        Object scp = registry.get(cuid);
        if (scp == null) {
            throw new DicomServiceException(command,
                    sopCUIDs.contains(cuid) ? Status.UnrecognizedOperation : Status.NoSuchSOPclass);
        }
        return scp;
    }

    private CStoreSCP getCStoreSCP(DicomObject command) throws DicomServiceException {

        return (CStoreSCP) getFrom(cstoreSCPs, command, Tag.AffectedSOPClassUID);
    }

    private CEchoSCP getCEchoSCP(DicomObject command) throws DicomServiceException {

        return (CEchoSCP) getFrom(cechoSCPs, command, Tag.AffectedSOPClassUID);
    }

    public void process(Association association, int pcid, DicomObject command,
            InputStream dataStream, String tsuid) throws IOException {

        try {
            int commandField = command.getInt(Tag.CommandField);
            if (commandField == CommandUtils.C_STORE_RQ) {
                getCStoreSCP(command).cstore(association, pcid, command, dataStream, tsuid);
            } else {
                DicomObject dataSet = null;
                if (dataStream != null) {
                    DicomInputStream dicomIn = new DicomInputStream(dataStream,
                            TransferSyntax.valueOf(tsuid));
                    try {
                        dataSet = dicomIn.readDicomObject();
                    } finally {
                        CloseUtils.safeClose(dicomIn);
                    }
                }
                switch (commandField) {
                case CommandUtils.C_ECHO_RQ:
                    getCEchoSCP(command).cecho(association, pcid, command);
                    break;

                default:
                    throw new DicomServiceException(command, Status.UnrecognizedOperation);
                }
            }
        } catch (DicomServiceException e) {
            association.writeDimseRSP(pcid, e.getCommand(), e.getDataset());
        }
    }
}
