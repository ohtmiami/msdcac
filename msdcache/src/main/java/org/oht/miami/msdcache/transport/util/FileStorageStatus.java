/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.util;

import java.util.Date;

/**
 * A class which encapsulates the information required to describe the status of
 * storing a file to the Store.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class FileStorageStatus {
    private FileStorageStatusType currentStatus;
    private int retryCount;
    private Date lastAccessDate;

    public FileStorageStatus(FileStorageStatusType status) {
        currentStatus = status;
        retryCount = 0;
        lastAccessDate = new Date();
    }

    /**
     * Gets the current status.
     * 
     * @return the currentStatus
     */
    public FileStorageStatusType getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Sets the current status.
     */
    public void setCurrentStatus(FileStorageStatusType newStatus) {
        currentStatus = newStatus;
        lastAccessDate = new Date();
    }

    /**
     * Gets the retry count.
     * 
     * @return the retryCount
     */
    public int getRetryCount() {
        return retryCount;
    }

    /**
     * Increments the retry count.
     */
    public void incrementRetryCount() {
        retryCount++;
        lastAccessDate = new Date();
    }

    /**
     * Gets the last access date.
     * 
     * @return the lastAccessDate
     */
    public Date getLastAccessDate() {
        return lastAccessDate;
    }

    /**
     * Manually sets the last access date.
     */
    public void setLastAccessDate(Date lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }
}
