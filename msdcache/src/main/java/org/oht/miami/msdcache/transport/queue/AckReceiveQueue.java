/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.queue;

import org.hornetq.core.server.embedded.EmbeddedHornetQ;
import org.oht.miami.msdcache.transport.MSDTransport;

/**
 * A persistent queue that receives acknowledgment messages from the
 * MSDReceiver.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class AckReceiveQueue extends QueueManager {
    /**
     * Constructor
     * 
     * @param context
     *            The MSDSender or MSDReceiver that is instantiating this queue.
     * @param messageServer
     *            The HornetQ server instance on which this queue will be
     *            deployed.
     * @param serverID
     *            The ID of the HornetQ server.
     * @param queueName
     *            The unique name of this queue.
     * @param queueAddress
     *            The unique address of this queue.
     */
    public AckReceiveQueue(MSDTransport context, EmbeddedHornetQ messageServer, int serverID,
            String queueName, String queueAddress) {
        super(context, messageServer, serverID, queueName, queueAddress);
    }
}
