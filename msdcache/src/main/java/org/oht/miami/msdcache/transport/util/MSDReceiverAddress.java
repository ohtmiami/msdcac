/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.transport.util;

/**
 * A class which encapsulates an MSD Receiver address (hostname and port).
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class MSDReceiverAddress {
    private String hostname;
    private int port;

    /**
     * Constructor
     * 
     * @param hostname
     *            The host name of the destination cache (MSDReceiver).
     * @param port
     *            The port of the destination cache (MSDReceiver).
     */
    public MSDReceiverAddress(String hostname, int port) {
        if (hostname == null || hostname.trim().isEmpty()) {
            throw new IllegalArgumentException("A valid hostname cannot be null or empty.");
        }

        this.hostname = hostname;
        this.port = port;
    }

    /**
     * Gets the host name of the destination cache (MSDReceiver).
     * 
     * @return The host name of the destination cache (MSDReceiver).
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Gets the port of the destination cache (MSDReceiver).
     * 
     * @return The port of the destination cache (MSDReceiver).
     */
    public int getPort() {
        return port;
    }
}
