/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import org.oht.miami.msdcache.forward.DicomSender;
import org.oht.miami.msdcache.ingest.DicomReceiver;
import org.oht.miami.msdcache.transport.MSDSender;
import org.oht.miami.msdcache.transport.util.MSDReceiverAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Properties file utilities.
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class PropertiesFileUtils {
    /**
     * The logger.
     */
    private static Logger LOG = LoggerFactory.getLogger(PropertiesFileUtils.class);

    /**
     * Adds local AE entries to the DicomSender.
     * 
     * @param dicomSender
     *            The DicomSender to which these AE entries are being added.
     * @param file
     *            The config file that contains these AE entries.
     */
    public static void addLocalApplicationEntitiesFromFile(DicomSender dicomSender, String file) {
        try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);) {
            String line;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                if (line.startsWith("#")) {
                    continue;
                }

                if (st.countTokens() == 1) {
                    String aeTitle = st.nextToken().trim();
                    dicomSender.addLocalApplicationEntity(aeTitle);
                    LOG.info("Adding Local AE to DicomSender: " + aeTitle);
                    continue;
                }

                if (st.countTokens() == 3) {
                    String aeTitle = st.nextToken().trim();
                    String hostname = st.nextToken().trim();
                    int port = Integer.parseInt(st.nextToken().trim());
                    LOG.info("Adding Local AE to DicomSender: " + aeTitle + "@" + hostname + ":"
                            + port);
                    dicomSender.addLocalApplicationEntity(aeTitle, hostname, port);
                    continue;
                }

                continue;
            }
        } catch (FileNotFoundException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }
    }

    /**
     * Adds local AE Entries to the DicomReceiver.
     * 
     * @param dicomReceiver
     *            The DicomReceiver to which these AE entries are being added.
     * @param file
     *            The config file that contains these AE entries.
     */
    public static void addLocalApplicationEntitiesFromFile(DicomReceiver dicomReceiver, String file) {
        try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);) {
            String line;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                if (line.startsWith("#")) {
                    continue;
                }

                if (st.countTokens() == 2) {
                    String aeTitle = st.nextToken().trim();
                    int port = Integer.parseInt(st.nextToken().trim());
                    LOG.info("Adding Local AE to DicomReceiver: " + aeTitle + ":" + port);
                    dicomReceiver.addLocalApplicationEntity(aeTitle, port);
                    continue;
                }

                if (st.countTokens() == 3) {
                    String aeTitle = st.nextToken().trim();
                    String hostname = st.nextToken().trim();
                    int port = Integer.parseInt(st.nextToken().trim());
                    LOG.info("Adding Local AE to DicomReceiver: " + aeTitle + "@" + hostname + ":"
                            + port);
                    dicomReceiver.addLocalApplicationEntity(aeTitle, hostname, port);
                    continue;
                }

                continue;
            }
        } catch (FileNotFoundException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }
    }

    /**
     * Adds remote AE entries to the DicomSender.
     * 
     * @param dicomSender
     *            The DicomSender to which these AE entries are being added.
     * @param file
     *            The config file that contains these AE entries.
     */
    public static void addRemoteApplicationEntitiesFromFile(DicomSender dicomSender, String file) {
        try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);) {
            String line;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                if (line.startsWith("#")) {
                    continue;
                }

                if (st.countTokens() == 3) {
                    String aeTitle = st.nextToken().trim();
                    String hostname = st.nextToken().trim();
                    int port = Integer.parseInt(st.nextToken().trim());
                    LOG.info("Adding Remote AE to DicomSender: " + aeTitle + "@" + hostname + ":"
                            + port);
                    dicomSender.addRemoteApplicationEntity(aeTitle, hostname, port);
                    continue;
                }

                continue;
            }
        } catch (FileNotFoundException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }
    }

    /**
     * Adds remote cache node entries to the MSDSender.
     * 
     * @param studySender
     *            The MSDSender to which these cache node entries are being
     *            added.
     * @param file
     *            The config file that contains these cache node entries.
     */
    public static void addRemoteNodesFromFile(MSDSender studySender, String file) {
        try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr);) {
            String line;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ",");

                if (line.startsWith("#")) {
                    continue;
                }

                if (st.countTokens() == 3) {
                    String aeTitle = st.nextToken().trim();
                    String hostname = st.nextToken().trim();
                    int port = Integer.parseInt(st.nextToken().trim());
                    LOG.info("Adding Remote Node to MSDSender: " + aeTitle + "@" + hostname + ":"
                            + port);
                    studySender.addRemoteNode(aeTitle, new MSDReceiverAddress(hostname, port));
                    continue;
                }

                continue;
            }
        } catch (FileNotFoundException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        } catch (IOException e) {
            // TODO handle exception cases, call GWL
            e.printStackTrace();
        }
    }
}
