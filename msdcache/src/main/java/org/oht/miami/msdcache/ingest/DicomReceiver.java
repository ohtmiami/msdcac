/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version 2.0 (the "License");
 * you may not use this file except in compliance with the License. You may obtain a copy of the
 * License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
 * ANY KIND, either express or implied. See the License for the specific language governing rights
 * and limitations under the License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in Java(TM), hosted at
 * http://sourceforge.net/projects/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Gunter Zeilinger, Huetteldorferstr. 24/10, 1150 Vienna/Austria/Europe.
 * Portions created by the Initial Developer are Copyright (C) 2002-2005 the Initial Developer.
 * All Rights Reserved.
 *
 * Contributor(s):
 * Gunter Zeilinger <gunterze@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

package org.oht.miami.msdcache.ingest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.dcm4che2.data.UID;
import org.oht.miami.msdcache.dicom.DicomServer;
import org.oht.miami.msdcache.dicom.NetworkApplicationEntity;
import org.oht.miami.msdcache.dicom.TransferCapability;
import org.oht.miami.msdcache.dicom.service.VerificationService;
import org.oht.miami.msdcache.transport.MSDSender;
import org.oht.miami.msdtk.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DicomReceiver extends DicomServer {

    private static Logger LOG = LoggerFactory.getLogger(DicomReceiver.class);

    private static final String[] TSUIDS = { UID.JPEGLSLossless, UID.JPEGLossless,
                    UID.JPEGLosslessNonHierarchical14, UID.JPEG2000LosslessOnly,
                    UID.DeflatedExplicitVRLittleEndian, UID.RLELossless,
                    UID.ExplicitVRLittleEndian, UID.ExplicitVRBigEndian,
                    UID.ImplicitVRLittleEndian, UID.JPEGBaseline1, UID.JPEGExtended24,
                    UID.JPEGLSLossyNearLossless, UID.JPEG2000, UID.MPEG2 };

    private static final TransferCapability[] TCS = initTransferCapabilities();

    private final VerificationService cechoSCP;

    private final StudyStorageService cstoreSCP;

    private final ScheduledExecutorService schedulerService;

    public DicomReceiver(ExecutorService executorService, Store store, long studyTimeoutDuration,
            TimeUnit studyTimeoutUnit) {

        super(executorService);
        cechoSCP = new VerificationService();
        cstoreSCP = new StudyStorageService(store, studyTimeoutDuration, studyTimeoutUnit,
                executorService);
        schedulerService = Executors.newScheduledThreadPool(1);
    }

    public void setStudySender(MSDSender studySender) {

        cstoreSCP.setStudySender(studySender);
    }

    public void addLocalApplicationEntity(String aeTitle, String hostname, int port) {

        NetworkApplicationEntity localAE = new NetworkApplicationEntity(aeTitle, hostname, port);
        localAE.setAssociationAcceptor(true);
        localAE.register(cechoSCP);
        localAE.register(cstoreSCP);
        localAE.setTransferCapabilities(TCS);
        // TODO localAE.addAssociationListener() - what to do when an
        // association is closed?
        NetworkApplicationEntity oldAE = this.addLocalApplicationEntity(localAE);
        if (oldAE != null) {
            LOG.warn("Replaced previously configured local AE {}", aeTitle);
        }
    }

    public void addLocalApplicationEntity(String aeTitle, int port) {

        addLocalApplicationEntity(aeTitle, null, port);
    }

    private static TransferCapability[] initTransferCapabilities() {

        TransferCapability[] tcs = new TransferCapability[StudyStorageService.CUIDS.length + 1];
        tcs[0] = new TransferCapability(UID.VerificationSOPClass,
                new String[] { UID.ImplicitVRLittleEndian }, TransferCapability.SCP);
        for (int i = 0; i < StudyStorageService.CUIDS.length; i++) {
            tcs[i + 1] = new TransferCapability(StudyStorageService.CUIDS[i], TSUIDS,
                    TransferCapability.SCP);
        }
        return tcs;
    }

    @Override
    public void start() {

        super.start();

        // TODO initialDelay and period should be customizable
        schedulerService.scheduleAtFixedRate(new Runnable() {

            @Override
            public void run() {

                cstoreSCP.getActiveStudyTable().cleanUp();
            }
        }, 10, 10, TimeUnit.SECONDS);
    }

    @Override
    public void stop() {

        super.stop();

        // Manually time out all active studies
        cstoreSCP.getActiveStudyTable().clear();

        // Terminate active study table maintenance thread
        schedulerService.shutdownNow();
        try {
            if (!schedulerService.awaitTermination(500, TimeUnit.MILLISECONDS)) {
                LOG.warn("Active study table maintenance thread did not terminate");
            }
        } catch (InterruptedException e) {
            LOG.warn("Interrupted while terminating active study table maintenance thread", e);
        }
    }
}
