/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdcache.util;

/**
 * A class which encapsulates a pair of AETitles (source and destination).
 * 
 * @author Derek Bougan (dbougan@harris.com)
 */
public class AETitlePair {
    private String source;
    private String destination;

    public AETitlePair(String source, String destination) {
        this.source = source;
        this.destination = destination;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return "(Source: " + source + ", Destination: " + destination + ")";
    }

    @Override
    public int hashCode() {
        String hashString = source + '\\' + destination;
        return hashString.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AETitlePair)) {
            return false;
        }

        AETitlePair other = (AETitlePair) obj;
        return source.equals(other.source) && destination.equals(other.destination);
    }
}
