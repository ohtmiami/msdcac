/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * Server: <br>
 * 
 * <pre>server
 * 
 * <pre>
 * <br>
 * <br>
 * Client operation:
 * <br>
 * 
 * <pre>
 * client [operation] [absolute_path_to_file/directory] [dest_ip]
 * </pre>
 * 
 * <br>
 * <br>
 * Client standby: <br>
 * 
 * <pre>
 * client [operation] [absolute_path_to_file/directory] [dest_ip] standby
 * [starter_server_ip]
 * </pre>
 * 
 * <br>
 * <br>
 * 
 * <pre>
 * [operation] = upload/download/upload_multipart
 * </pre>
 * 
 * <br>
 * <br>
 * standby means client connects to a starter server first, and start operating
 * after receiving a signal from that server. This makes running multiple
 * clients at exactly the same time possible Main class for starter server is
 * Starter.java; <br>
 * Operation detial: <br>
 * <br>
 * 1) upload <br>
 * 
 * <pre>
 * client upload ~/dicom/SMALLCT [server_host]
 * </pre>
 * 
 * <br>
 * client will send a series of http POST requests. Each request contain one
 * file. Chunked encoded. <br>
 * <br>
 * 2) upload_multipart <br>
 * 
 * <pre>
 * client upload_multipart ~/dicom/SMALLCT [server_host]
 * </pre>
 * 
 * <br>
 * client will send one http POST requests containing all the files using MIME
 * multipart protocol. Chunked encoded. <br>
 * <br>
 * 3) download <br>
 * 
 * <pre>
 * client download /root/test/MSDTK/MSD/SMALLCT [server_host]
 * </pre>
 * 
 * <br>
 * client will send a http GET request and server will send back a series of
 * http responses. Each response contains one file <br>
 * <br>
 * Running multiple clients simultaneously: <br>
 * First start the server and the starter server. Then start all clients (they
 * can be in different machine) by doing: <br>
 * 
 * <pre>
 * client [operation] [absolute_path_to_file/directory] [server_host] standby
 * [starter_server_host];
 * </pre>
 * 
 * <br>
 * Then start all clients simultaneously by pressing return on starter server
 * program
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class Main {

    private static StudyClient studyClient;
    private static Channel channel;

    public static void main(String[] args) throws IOException, InterruptedException {
        String standByHost = "localhost";
        String host = "localhost";
        String input = "server";
        String operation = null;
        String filePath = null;
        boolean standBy = false;

        if (args.length == 3) {
            input = args[0];
            operation = args[1];
            filePath = args[2];
        } else if (args.length == 4) {
            input = args[0];
            operation = args[1];
            filePath = args[2];
            host = args[3];
        } else if (args.length == 1) {
            input = args[0];
            if (input.equalsIgnoreCase("client")) {
                printUsage();
                return;
            }
        } else if (args.length == 5) {
            input = args[0];
            operation = args[1];
            filePath = args[2];
            host = args[3];
            String flag = args[4];
            if (flag.equalsIgnoreCase("standby")) {
                standBy = true;
            }
        } else if (args.length == 6) {
            input = args[0];
            operation = args[1];
            filePath = args[2];
            host = args[3];
            String flag = args[4];
            standByHost = args[5];
            if (flag.equalsIgnoreCase("standby")) {
                standBy = true;
            }
        } else {
            printUsage();
            return;
        }

        if (input.equalsIgnoreCase("server")) {
            new StudyServer().run();
        } else if (input.equalsIgnoreCase("client")) {
            if (standBy) {
                standBy(standByHost);
                studyClient = new StudyClient(host, 8444, filePath, operation);
                studyClient.connect();
            } else {
                studyClient = new StudyClient(host, 8444, filePath, operation);
                studyClient.connect();
                studyClient.run();
            }
        } else {
            printUsage();
        }
    }

    public static void standBy(String standByHost) {
        System.out.println("client standing by ...");
        ClientBootstrap bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        bootstrap.setPipelineFactory(new StudyClientStandByPipelineFactory());
        bootstrap.setOption("tcpNoDelay", true);
        bootstrap.setOption("keepAlive", true);

        ChannelFuture future = bootstrap.connect(new InetSocketAddress(standByHost, 8447));

        // check if connection succeeded
        channel = future.awaitUninterruptibly().getChannel();
        if (!future.isSuccess()) {
            future.getCause().printStackTrace();
            bootstrap.releaseExternalResources();
            System.err.println("standBy starter not connected ...");
            System.exit(2);
        }
    }

    /**
     * called when start signal is received
     */
    public static void start() {
        // close the connection to starter server
        channel.close();
        System.err.println("client start running ...");
        studyClient.run();
    }

    private static void printUsage() {
        System.out.println("Usage:\n 1) server\n 2) client [upload/download/upload_multipart]"
                + " [absolute_file_path] [server_ip] [if_stand_by] [starter_ip]");
    }
}
