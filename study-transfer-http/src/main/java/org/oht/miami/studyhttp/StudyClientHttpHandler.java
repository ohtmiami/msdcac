/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.handler.codec.http.HttpChunk;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.util.CharsetUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client handler that receive incoming network packet following http protocol.
 * Supporting chunked-transfer-encoding
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyClientHttpHandler extends SimpleChannelHandler {
    private boolean readingChunks;
    private static final Logger logger = Logger.getLogger(StudyClientHttpHandler.class.getName());
    private static FileChannel fc = null;

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
        super.handleUpstream(ctx, e);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        if (!readingChunks) {
            HttpResponse response = (HttpResponse) e.getMessage();
            if (response.isChunked()) {
                readingChunks = true;
                String fileName = "download";
                if (response.containsHeader(HttpHeaders.Names.CONTENT_LOCATION)) {
                    fileName = response.getHeader(HttpHeaders.Names.CONTENT_LOCATION);
                    try {
                        fileName = URLDecoder.decode(fileName, "UTF-8");
                    } catch (UnsupportedEncodingException e4) {
                        try {
                            fileName = URLDecoder.decode(fileName, "ISO-8859-1");
                        } catch (UnsupportedEncodingException e8) {
                            throw new Error();
                        }
                    }
                }
                /*
                 * System.err.println(
                 * "==============================================================="
                 * ); System.err.println(e.getMessage()); System.err.println(
                 * "==============================================================="
                 * );
                 */
                File file = new File("downloads/" + fileName);
                try {
                    FileOutputStream outputStream = new FileOutputStream(file);
                    fc = outputStream.getChannel();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                    return;
                }
            } else {
                ChannelBuffer content = response.getContent();
                if (content.readable()) {
                    System.out.println("CONTENT (NO CHUNKED) {");
                    System.out.println(content.toString(CharsetUtil.UTF_8));
                    System.out.println("} END OF CONTENT");
                }
            }
        } else {
            HttpChunk chunk = (HttpChunk) e.getMessage();
            if (chunk.isLast()) {
                readingChunks = false;
                fc = null;
            } else {
                try {
                    fc.write(chunk.getContent().toByteBuffer());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        ctx.sendUpstream(e);

        long elapse = System.currentTimeMillis() - StudyClient.startTimer;
        long sec = elapse / 1000;
        long min = (long) Math.floor(sec / 60);
        sec = sec - min * 60;
        elapse = elapse - min * 60000 - sec * 1000;
        System.err.println("\n===================> " + min + " min; " + sec + " sec; " + elapse
                + " milisec <===================");

        System.exit(0);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        logger.log(Level.WARNING, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }
}
