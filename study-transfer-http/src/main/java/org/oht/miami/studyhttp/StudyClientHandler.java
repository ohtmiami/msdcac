/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Client handler that handle byte stream input from the network
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyClientHandler extends SimpleChannelUpstreamHandler {

    private static final Logger logger = Logger.getLogger(StudyClientHandler.class.getName());
    private static String fileName = null;

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
        if (e instanceof ChannelStateEvent) {
            logger.info(e.toString());
        }
        super.handleUpstream(ctx, e);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        System.err.println("------- Connected -------");
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        ChannelBuffer buf = (ChannelBuffer) e.getMessage();
        if (fileName == null) {
            fileName = new String(buf.array(), Charset.forName("UTF-16LE"));
            if (fileName.length() > 100) {
                System.err.println("ERROR: fileName too long");
            } else {
                // System.err.println("---- File Name Received ---- [" +
                // fileName + "]");
            }

            return;
        }

        int size = buf.readableBytes();
        System.err.println("------- Message Received ------- [" + size + "]");
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(new File("downloads/" + fileName), true);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
            fileName = null;
            return;
        }
        FileChannel fc = outputStream.getChannel();
        try {
            fc.write(buf.toByteBuffer());
        } catch (IOException e1) {
            e1.printStackTrace();
            fileName = null;
            return;
        }

        fileName = null;
        
        // print out the time elapsed
        long elapse = System.currentTimeMillis() - StudyClient.startTimer;
        long sec = elapse / 1000;
        long min = (long) Math.floor(sec / 60);
        sec = sec - min * 60;
        elapse = elapse - min * 60000 - sec * 1000;
        System.err.println("===================> " + min + " min; " + sec + " sec; " + elapse
                + " milisec");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        logger.log(Level.WARNING, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }
}
