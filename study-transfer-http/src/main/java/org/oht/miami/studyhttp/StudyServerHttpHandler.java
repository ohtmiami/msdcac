/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import static org.jboss.netty.handler.codec.http.HttpHeaders.isKeepAlive;
import static org.jboss.netty.handler.codec.http.HttpHeaders.setContentLength;
import static org.jboss.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static org.jboss.netty.handler.codec.http.HttpMethod.GET;
import static org.jboss.netty.handler.codec.http.HttpMethod.POST;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.BAD_REQUEST;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.FORBIDDEN;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.INTERNAL_SERVER_ERROR;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.NOT_FOUND;
import static org.jboss.netty.handler.codec.http.HttpResponseStatus.OK;
import static org.jboss.netty.handler.codec.http.HttpVersion.HTTP_1_1;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelFutureProgressListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.DefaultFileRegion;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.FileRegion;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.frame.TooLongFrameException;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpChunk;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import org.jboss.netty.handler.codec.http.multipart.DiskAttribute;
import org.jboss.netty.handler.codec.http.multipart.DiskFileUpload;
import org.jboss.netty.handler.codec.http.multipart.FileUpload;
import org.jboss.netty.handler.codec.http.multipart.HttpDataFactory;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.EndOfDataDecoderException;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.ErrorDataDecoderException;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.IncompatibleDataDecoderException;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestDecoder.NotEnoughDataDecoderException;
import org.jboss.netty.handler.codec.http.multipart.InterfaceHttpData;
import org.jboss.netty.handler.codec.http.multipart.InterfaceHttpData.HttpDataType;
import org.jboss.netty.handler.ssl.SslHandler;
import org.jboss.netty.handler.stream.ChunkedFile;
import org.jboss.netty.util.CharsetUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.activation.MimetypesFileTypeMap;

/**
 * Server handler that receiving http POST and GET request and sending http
 * response. Implement http protocl including chunk-transfer-encoding and MIMT
 * multipart
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyServerHttpHandler extends SimpleChannelUpstreamHandler {

    private List<File> fileList = new ArrayList<File>();
    private Iterator<File> iter = null;
    private MessageEvent e = null;
    private boolean readingChunks = false;
    private boolean isMultipart = false;
    private FileChannel fc = null;
    private ChannelHandlerContext ctx = null;
    private static final HttpDataFactory factory = new DefaultHttpDataFactory(
            DefaultHttpDataFactory.MINSIZE);
    private HttpPostRequestDecoder decoder;
    static {
        DiskFileUpload.deleteOnExitTemporaryFile = true;
        DiskFileUpload.baseDirectory = null;
        DiskAttribute.deleteOnExitTemporaryFile = true;
        DiskAttribute.baseDirectory = null;
    }
    public static final String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    public static final String HTTP_DATE_GMT_TIMEZONE = "GMT";
    public static final int HTTP_CACHE_SECONDS = 60;

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        System.err.println("connection closed");
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        // check if message is http header or chunked http message body
        if (!readingChunks) {
            HttpRequest request = (HttpRequest) e.getMessage();
            // check for http request method, get or post
            if (request.getMethod() == GET) {

                // System.err.println("=======================================================");
                // System.err.println(e.getMessage());
                // System.err.println("=======================================================");

                final String path = sanitizeUri(request.getUri());
                if (path == null) {
                    sendError(ctx, FORBIDDEN);
                    return;
                }
                File file = new File(path);
                getAllFiles(file);
                iter = fileList.iterator();
                this.e = e;
                this.ctx = ctx;
                sendAllFiles();
            } else if (request.getMethod() == POST) {
                // clean previous FileUpload if Any
                if (decoder != null) {
                    decoder.cleanFiles();
                    decoder = null;
                }

                // System.err.println("=======================================================");
                // System.err.println(e.getMessage());
                // System.err.println("=======================================================");

                // initialize http post request decoder
                try {
                    decoder = new HttpPostRequestDecoder(factory, request);
                } catch (ErrorDataDecoderException e1) {
                    e1.printStackTrace();
                    Channels.close(e.getChannel());
                    System.err.println("1");
                    return;
                } catch (IncompatibleDataDecoderException e1) {
                    e1.printStackTrace();
                    System.err.println("2");
                    return;
                }

                isMultipart = decoder.isMultipart();
                if (!isMultipart) {
                    // System.out.println("==========> NON-MULTIPART <==========");
                    String fileName = request.getUri();
                    try {
                        fileName = URLDecoder.decode(fileName, "UTF-8");
                    } catch (UnsupportedEncodingException e3) {
                        try {
                            fileName = URLDecoder.decode(fileName, "ISO-8859-1");
                        } catch (UnsupportedEncodingException e1) {
                            throw new Error();
                        }
                    }
                    File file = new File("downloads/" + fileName);
                    FileOutputStream outputStream = new FileOutputStream(file);
                    fc = outputStream.getChannel();
                    if (request.isChunked()) {
                        // Chunk version
                        readingChunks = true;
                    } else {
                    }
                } else {
                    // System.out.println("==========> MULTIPART <==========");
                    if (request.isChunked()) {
                        // Chunk version
                        readingChunks = true;
                    } else {
                        // Not chunk version
                        readHttpDataAllReceive(e.getChannel());
                    }
                }
            }
        } else {
            // New chunk is received
            HttpChunk chunk = (HttpChunk) e.getMessage();
            if (isMultipart) {
                try {
                    decoder.offer(chunk);
                } catch (ErrorDataDecoderException e1) {
                    e1.printStackTrace();
                    Channels.close(e.getChannel());
                    return;
                }
                readHttpDataChunkByChunk(e.getChannel());
            } else {
                fc.write(chunk.getContent().toByteBuffer());
            }

            if (chunk.isLast()) {
                readingChunks = false;
            }
        }
    }

    /**
     * check if http post request decoder generate any result
     * 
     * @param channel
     */
    private void readHttpDataChunkByChunk(Channel channel) {
        try {
            while (decoder.hasNext()) {
                InterfaceHttpData data = decoder.next();
                if (data != null) {
                    // new value
                    writeHttpData(data);
                }
            }
        } catch (EndOfDataDecoderException e1) {
        }
    }

    /**
     * send all files by repeatedly calling itself. files are requested by
     * client using http GET request
     */
    public void sendAllFiles() {
        HttpRequest request = (HttpRequest) e.getMessage();

        if (iter.hasNext()) {
            File file = iter.next();
            if (file.isHidden() || !file.exists()) {
                sendError(ctx, NOT_FOUND);
                return;
            }
            if (!file.isFile()) {
                sendError(ctx, FORBIDDEN);
                return;
            }

            // Cache Validation
            String ifModifiedSince = request.getHeader(HttpHeaders.Names.IF_MODIFIED_SINCE);
            if (ifModifiedSince != null && !ifModifiedSince.equals("")) {
                SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
                Date ifModifiedSinceDate = null;
                try {
                    ifModifiedSinceDate = dateFormatter.parse(ifModifiedSince);
                } catch (ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

                // Only compare up to the second because the datetime format we
                // send to the client does
                // not have milliseconds
                long ifModifiedSinceDateSeconds = ifModifiedSinceDate.getTime() / 1000;
                long fileLastModifiedSeconds = file.lastModified() / 1000;
                if (ifModifiedSinceDateSeconds == fileLastModifiedSeconds) {
                    sendNotModified(ctx);
                    return;
                }
            }

            RandomAccessFile raf = null;
            long fileLength = 0;
            try {
                raf = new RandomAccessFile(file, "r");
                fileLength = raf.length();
            } catch (FileNotFoundException fnfe) {
                sendError(ctx, NOT_FOUND);
                System.exit(1);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(2);
            }

            HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);
            setContentLength(response, fileLength);
            setContentTypeHeader(response, file);
            setDateAndCacheHeaders(response, file);
            response.setHeader(HttpHeaders.Names.CONTENT_LOCATION, file.getName());

            Channel ch = e.getChannel();

            // Write the initial line and the header.
            ch.write(response);

            // Write the content.
            ChannelFuture writeFuture;
            final String path = file.getPath();
            if (ch.getPipeline().get(SslHandler.class) != null) {
                // Cannot use zero-copy with HTTPS.
                try {
                    writeFuture = ch.write(new ChunkedFile(raf, 0, fileLength, 8192));
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    System.exit(4);
                }
            } else {
                // No encryption - use zero-copy.
                final FileRegion region = new DefaultFileRegion(raf.getChannel(), 0, fileLength);
                writeFuture = ch.write(region);
                writeFuture.addListener(new ChannelFutureProgressListener() {
                    public void operationComplete(ChannelFuture future) {
                        region.releaseExternalResources();
                        sendAllFiles();
                    }

                    public void operationProgressed(ChannelFuture future, long amount,
                            long current, long total) {
                        // System.out.printf("%s: %d / %d (+%d)%n", path,
                        // current, total, amount);
                    }
                });
            }
        } else {
            // Decide whether to close the connection or not.
            if (!isKeepAlive(request)) {
                e.getChannel().close();
            }
        }
    }

    /**
     * get all files that are requested by client
     * 
     * @param file
     */
    public void getAllFiles(File file) {
        if (!file.isDirectory()) {
            fileList.add(file);
            return;
        }

        File[] files = file.listFiles();
        for (File f : files) {
            getAllFiles(f);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        Channel ch = e.getChannel();
        Throwable cause = e.getCause();
        if (cause instanceof TooLongFrameException) {
            sendError(ctx, BAD_REQUEST);
            return;
        }

        cause.printStackTrace();
        if (ch.isConnected()) {
            sendError(ctx, INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * trim the uri
     * 
     * @param uri
     * @return
     */
    private static String sanitizeUri(String uri) {
        // Decode the path.
        try {
            uri = URLDecoder.decode(uri, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            try {
                uri = URLDecoder.decode(uri, "ISO-8859-1");
            } catch (UnsupportedEncodingException e1) {
                throw new Error();
            }
        }

        // Convert file separators.
        uri = uri.replace('/', File.separatorChar);

        // Simplistic dumb security check.
        // You will have to do something serious in the production environment.
        if (uri.contains(File.separator + ".") || uri.contains("." + File.separator)
                || uri.startsWith(".") || uri.endsWith(".")) {
            return null;
        }

        // Convert to absolute path.
        return uri;
    }

    /**
     * Send error message back to the sander
     * 
     * @param ctx
     *            channel handler context
     * @param status
     *            http response status
     */
    private static void sendError(ChannelHandlerContext ctx, HttpResponseStatus status) {
        System.err.println("===> SendError");
        HttpResponse response = new DefaultHttpResponse(HTTP_1_1, status);
        response.setHeader(CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.setContent(ChannelBuffers.copiedBuffer("Failure: " + status.toString() + "\r\n",
                CharsetUtil.UTF_8));

        // Close the connection as soon as the error message is sent.
        ctx.getChannel().write(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * When file timestamp is the same as what the browser is sending up, send a
     * "304 Not Modified"
     * 
     * @param ctx
     *            Context
     */
    private static void sendNotModified(ChannelHandlerContext ctx) {
        System.err.println("===> SendNotModified");
        HttpResponse response = new DefaultHttpResponse(HTTP_1_1, HttpResponseStatus.NOT_MODIFIED);
        setDateHeader(response);

        // Close the connection as soon as the error message is sent.
        ctx.getChannel().write(response).addListener(ChannelFutureListener.CLOSE);
    }

    /**
     * Sets the Date header for the HTTP response
     * 
     * @param response
     *            HTTP response
     */
    private static void setDateHeader(HttpResponse response) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_GMT_TIMEZONE));

        Calendar time = new GregorianCalendar();
        response.setHeader(HttpHeaders.Names.DATE, dateFormatter.format(time.getTime()));
    }

    /**
     * Sets the Date and Cache headers for the HTTP Response
     * 
     * @param response
     *            HTTP response
     * @param fileToCache
     *            file to extract content type
     */
    private static void setDateAndCacheHeaders(HttpResponse response, File fileToCache) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_GMT_TIMEZONE));

        // Date header
        Calendar time = new GregorianCalendar();
        response.setHeader(HttpHeaders.Names.DATE, dateFormatter.format(time.getTime()));

        // Add cache headers
        time.add(Calendar.SECOND, HTTP_CACHE_SECONDS);
        response.setHeader(HttpHeaders.Names.EXPIRES, dateFormatter.format(time.getTime()));
        response.setHeader(HttpHeaders.Names.CACHE_CONTROL, "private, max-age="
                + HTTP_CACHE_SECONDS);
        response.setHeader(HttpHeaders.Names.LAST_MODIFIED,
                dateFormatter.format(new Date(fileToCache.lastModified())));
    }

    /**
     * Set the content type header for this http response
     * 
     * @param response
     *            http response
     * @param file
     *            content file
     */
    private static void setContentTypeHeader(HttpResponse response, File file) {
        MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
        response.setHeader(HttpHeaders.Names.CONTENT_TYPE,
                mimeTypesMap.getContentType(file.getPath()));
    }

    /**
     * write incoming data from POST request into file system
     * 
     * @param data
     */
    private void writeHttpData(InterfaceHttpData data) {
        if (data.getHttpDataType() == HttpDataType.Attribute) {

        } else if (data.getHttpDataType() == HttpDataType.FileUpload) {
            FileUpload fileUpload = (FileUpload) data;
            File file = new File("downloads/" + fileUpload.getName());
            try {
                file.createNewFile();
                fileUpload.renameTo(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Write all http data from a http message
     * @param channel
     */
    private void readHttpDataAllReceive(Channel channel) {
        List<InterfaceHttpData> datas = null;
        try {
            datas = decoder.getBodyHttpDatas();
        } catch (NotEnoughDataDecoderException e1) {
            e1.printStackTrace();
            Channels.close(channel);
            return;
        }
        for (InterfaceHttpData data : datas) {
            writeHttpData(data);
        }
    }
}