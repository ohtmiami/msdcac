/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import static org.jboss.netty.channel.Channels.pipeline;

import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.handler.codec.frame.DelimiterBasedFrameDecoder;
import org.jboss.netty.handler.codec.http.HttpRequestEncoder;

/**
 * Client pipeline factory that implementing byte array input/output stream
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyClientPipelineFactory implements ChannelPipelineFactory {

    public static final String DELIMITER = "jiefeng";
    
    public ChannelPipeline getPipeline() throws Exception {
        ChannelPipeline pipeline = pipeline();
        pipeline.addLast("DelimiterBasedFrameDecoder", new DelimiterBasedFrameDecoder(
                Integer.MAX_VALUE, true, true, ChannelBuffers.wrappedBuffer(DELIMITER.getBytes())));
        pipeline.addLast("encoder", new HttpRequestEncoder());
        pipeline.addLast("handler", new StudyClientHandler());
        return pipeline;
    }
}
