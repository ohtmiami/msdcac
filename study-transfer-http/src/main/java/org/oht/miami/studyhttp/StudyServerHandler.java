/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelEvent;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.DefaultFileRegion;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.FileRegion;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.channel.group.ChannelGroup;
import org.jboss.netty.channel.group.DefaultChannelGroup;
import org.jboss.netty.handler.codec.http.HttpRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Server handler that handler receiving http GET request and sending byte
 * stream data
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyServerHandler extends SimpleChannelUpstreamHandler {

    List<File> fileList = new ArrayList<File>();
    private Iterator<File> iter = null;
    private MessageEvent e = null;
    private File f = null;
    private boolean readingChunks = false;
    private static final Logger logger = Logger.getLogger(StudyServerHandler.class.getName());
    public static final ChannelGroup channels = new DefaultChannelGroup();

    @Override
    public void handleUpstream(ChannelHandlerContext ctx, ChannelEvent e) throws Exception {
        if (e instanceof ChannelStateEvent) {
            logger.info(e.toString());
        }
        super.handleUpstream(ctx, e);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        super.channelConnected(ctx, e);
        channels.add(e.getChannel());
        System.err.println("------- Connected -------");
        
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        super.channelDisconnected(ctx, e);
        System.err.println("------- Disconnected -------");
       channels.remove(e.getChannel());
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        System.out.println(">>>>>>>>>>>>>>>>>>> MESSAGE RECEIVED >>>>>>>>>>>>>>>>>>>");
        System.err.println(e.getMessage());
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

        if (!readingChunks) {
            HttpRequest request = (HttpRequest) e.getMessage();

            String uri = request.getUri();
            File file = new File(uri);
            getAllFiles(file);

            System.err.println(fileList.size());

            iter = fileList.iterator();
            this.e = e;
            sendFileName();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
        logger.log(Level.WARNING, "Unexpected exception from downstream.", e.getCause());
        e.getChannel().close();
    }

    public void getAllFiles(File file) {
        if (!file.isDirectory()) {
            fileList.add(file);
            return;
        }

        File[] files = file.listFiles();
        for (File f : files) {
            getAllFiles(f);
        }
    }

    public void sendFileName() {
        if (iter.hasNext()) {
            f = iter.next();
            byte[] b = f.getName().getBytes(Charset.forName("UTF-16LE"));
            ByteBuffer buf = ByteBuffer.wrap(b);
            ChannelBuffer a = ChannelBuffers.buffer(StudyClientPipelineFactory.DELIMITER.length()
                    + buf.capacity());
            ChannelBuffer delimiterBuffer = ChannelBuffers
                    .wrappedBuffer(StudyClientPipelineFactory.DELIMITER.getBytes());
            a.writeBytes(buf);
            a.writeBytes(delimiterBuffer);
            // Write the response.
            ChannelFuture cf = e.getChannel().write(a);
            cf.addListener(new ChannelFutureListener() {
                public void operationComplete(ChannelFuture future) {
                    sendNextFile();
                }
            });
        } else {
            e.getChannel().close();
        }
    }

    public void sendNextFile() {
        System.err.println(">>>>>>>>>>>>>>>>>> SENDING - " + f.getPath() + ">>>>>>>>>>>>>>>>>> ["
                + f.length() + "]");

        RandomAccessFile raf = null;
        long fileLength = 0;
        try {
            raf = new RandomAccessFile(f, "r");
            fileLength = raf.length();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        }
        final FileRegion region = new DefaultFileRegion(raf.getChannel(), 0, fileLength);
        ChannelFuture writeFuture = e.getChannel().write(region);

        writeFuture.addListener(new ChannelFutureListener() {
            public void operationComplete(ChannelFuture future) {
                region.releaseExternalResources();
                ChannelBuffer delimiterBuffer = ChannelBuffers
                        .wrappedBuffer(StudyClientPipelineFactory.DELIMITER.getBytes());
                ChannelFuture cf = future.getChannel().write(delimiterBuffer);
                cf.addListener(new ChannelFutureListener() {
                    public void operationComplete(ChannelFuture future) {
                        sendFileName();
                    }
                });
            }
        });
    }
}