/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.studyhttp;

import static org.jboss.netty.handler.codec.http.HttpHeaders.setContentLength;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelFutureProgressListener;
import org.jboss.netty.channel.DefaultFileRegion;
import org.jboss.netty.channel.FileRegion;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.jboss.netty.handler.codec.http.DefaultHttpRequest;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpMethod;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import org.jboss.netty.handler.codec.http.multipart.DiskAttribute;
import org.jboss.netty.handler.codec.http.multipart.DiskFileUpload;
import org.jboss.netty.handler.codec.http.multipart.HttpDataFactory;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestEncoder;
import org.jboss.netty.handler.codec.http.multipart.HttpPostRequestEncoder.ErrorDataEncoderException;
import org.jboss.netty.handler.ssl.SslHandler;
import org.jboss.netty.handler.stream.ChunkedFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;

import javax.activation.MimetypesFileTypeMap;

/**
 * Client program that can send GET request asking for a study or send one or
 * series of POST requests to upload a study
 * 
 * @author Jiefeng Zhai jzhai2@gmail.com
 */
public class StudyClient {
    private String host;
    private int port;
    private Channel channel;
    private String filePath;
    private String operation;
    public static long startTimer = 0;
    private ClientBootstrap bootstrap;
    private List<File> fileList = new ArrayList<File>();
    private Iterator<File> iter = null;
    public static final String HTTP_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    public static final String HTTP_DATE_GMT_TIMEZONE = "GMT";
    public static final int HTTP_CACHE_SECONDS = 60;
    private static MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();

    public StudyClient() {
        host = "localhost";
        port = 8444;
        filePath = "/";
        operation = "download";
    }

    public StudyClient(String host, int port, String filePath, String operation) {
        this.host = host;
        this.port = port;
        this.filePath = filePath;
        this.operation = operation;
    }

    public void connect() throws IOException {
        // set up connection
        bootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        bootstrap.setPipelineFactory(new StudyClientHttpPipelineFactory());
        bootstrap.setOption("tcpNoDelay", true);
        bootstrap.setOption("keepAlive", true);

        ChannelFuture future = bootstrap.connect(new InetSocketAddress(host, port));

        // check if connection succeeded
        channel = future.awaitUninterruptibly().getChannel();
        if (!future.isSuccess()) {
            future.getCause().printStackTrace();
            bootstrap.releaseExternalResources();
            System.exit(8);
        }
    }

    public void run() {
        if (operation.equalsIgnoreCase("download")) {
            formGet(bootstrap, host, port, filePath);
        } else if (operation.equalsIgnoreCase("upload")) {
            formPost(bootstrap, host, port, filePath);
        } else if (operation.equalsIgnoreCase("upload_multipart")) {
            formPostMultipart(bootstrap, host, port, filePath);
        }
    }

    private void formGet(ClientBootstrap bootstrap, String host, int port, String filePath) {
        // Prepare the HTTP request.
        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, filePath);
        // /root/test/MSDTK/picture
        // /root/test/deidentifiedstudies/SMALLCT/68D6DFC9.dcm
        request.setHeader(HttpHeaders.Names.HOST, host);
        request.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);

        startTimer = System.currentTimeMillis();

        // Send the HTTP request.
        channel.write(request);
    }

    private void formPost(ClientBootstrap bootstrap, String host, int port, String filePath) {
        File file = new File(filePath);
        getAllFiles(file);
        iter = fileList.iterator();

        startTimer = System.currentTimeMillis();

        sendAllFiles();
    }

    private void formPostMultipart(ClientBootstrap bootstrap, String host, int port, String filePath) {
        File file = new File(filePath);
        getAllFiles(file);

        startTimer = System.currentTimeMillis();

        sendAllFilesbodyRequestEncoder(file.getName());
    }

    public void sendAllFilesbodyRequestEncoder(String fileName) {
        HttpDataFactory factory = new DefaultHttpDataFactory(DefaultHttpDataFactory.MINSIZE);
        DiskFileUpload.deleteOnExitTemporaryFile = true;
        DiskFileUpload.baseDirectory = null;
        DiskAttribute.deleteOnExitTemporaryFile = true;
        DiskAttribute.baseDirectory = null;
        HttpPostRequestEncoder bodyRequestEncoder = null;
        HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST,
                fileName);
        request.setChunked(true);
        try {
            bodyRequestEncoder = new HttpPostRequestEncoder(factory, request, true);
        } catch (NullPointerException e) {
            // should not be since args are not null
            e.printStackTrace();
        } catch (ErrorDataEncoderException e) {
            // test if method is a POST method
            e.printStackTrace();
        }

        try {
            for (File f : fileList) {
                bodyRequestEncoder.addBodyFileUpload(f.getName(), f,
                        mimeTypesMap.getContentType(f), false);
            }
        } catch (NullPointerException e) {
            // should not be since not null args
            e.printStackTrace();
        } catch (ErrorDataEncoderException e) {
            // if an encoding error occurs
            e.printStackTrace();
        }

        // finalize request
        try {
            request = bodyRequestEncoder.finalizeRequest();
        } catch (ErrorDataEncoderException e) {
            // if an encoding error occurs
            e.printStackTrace();
        }

        // send request
        channel.write(request);

        // test if request was chunked and if so, finish the write
        if (request.isChunked()) {
            ChannelFuture cf = channel.write(bodyRequestEncoder);
            cf.addListener(new ChannelFutureListener() {
                public void operationComplete(ChannelFuture future) {
                    future.getChannel().close();
                }
            });
        }
    }

    public void getAllFiles(File file) {
        if (!file.isDirectory()) {
            fileList.add(file);
            return;
        }

        File[] files = file.listFiles();
        for (File f : files) {
            getAllFiles(f);
        }
    }

    public void sendAllFiles() {
        if (iter.hasNext()) {
            File file = iter.next();
            if (!file.exists() || !file.isFile()) {
                System.err.println("ERROR: file not exist or file is directory");
                return;
            }

            RandomAccessFile raf = null;
            long fileLength = 0;
            try {
                raf = new RandomAccessFile(file, "r");
                fileLength = raf.length();
            } catch (FileNotFoundException fnfe) {
                System.exit(1);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(2);
            }

            // Prepare the HTTP request.
            HttpRequest request = new DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.POST,
                    file.getName());
            // /root/test/MSDTK/picture
            // /root/test/deidentifiedstudies/SMALLCT/68D6DFC9.dcm
            request.setHeader(HttpHeaders.Names.HOST, host);
            setContentLength(request, fileLength);
            setContentTypeHeader(request, file);
            setDateAndCacheHeaders(request, file);

            // Send the HTTP request.
            channel.write(request);

            // Write the content.
            ChannelFuture writeFuture;
            if (channel.getPipeline().get(SslHandler.class) != null) {
                // Cannot use zero-copy with HTTPS.
                try {
                    writeFuture = channel.write(new ChunkedFile(raf, 0, fileLength, 8192));
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                    System.exit(4);
                }
            } else {
                // No encryption - use zero-copy.
                final FileRegion region = new DefaultFileRegion(raf.getChannel(), 0, fileLength);
                writeFuture = channel.write(region);
                writeFuture.addListener(new ChannelFutureProgressListener() {
                    public void operationComplete(ChannelFuture future) {
                        region.releaseExternalResources();
                        sendAllFiles();
                    }

                    public void operationProgressed(ChannelFuture future, long amount,
                            long current, long total) {
                        // System.out.printf("%s: %d / %d (+%d)%n", path,
                        // current, total, amount);
                    }
                });
            }
        } else {
            channel.close();
        }
    }

    private static void setDateAndCacheHeaders(HttpRequest request, File fileToCache) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(HTTP_DATE_FORMAT, Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone(HTTP_DATE_GMT_TIMEZONE));

        // Date header
        Calendar time = new GregorianCalendar();
        request.setHeader(HttpHeaders.Names.DATE, dateFormatter.format(time.getTime()));

        // Add cache headers
        time.add(Calendar.SECOND, HTTP_CACHE_SECONDS);
        request.setHeader(HttpHeaders.Names.EXPIRES, dateFormatter.format(time.getTime()));
        request.setHeader(HttpHeaders.Names.CACHE_CONTROL, "private, max-age=" + HTTP_CACHE_SECONDS);
        request.setHeader(HttpHeaders.Names.LAST_MODIFIED,
                dateFormatter.format(new Date(fileToCache.lastModified())));
    }

    private static void setContentTypeHeader(HttpRequest request, File file) {
        mimeTypesMap = new MimetypesFileTypeMap();
        request.setHeader(HttpHeaders.Names.CONTENT_TYPE,
                mimeTypesMap.getContentType(file.getPath()));
    }
}
